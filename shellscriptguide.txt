$ echo '#!/bin/sh' > my-script.sh
$ echo 'echo Hello World' >> my-script.sh
$ chmod 755 my-script.sh
$ ./my-script.sh
Hello World




my-script.sh

#!/bin/sh
# This is a comment!
echo Hello World	# This is a comment, too!


to make it executable 
$ chmod a+rx my-script.sh
$ ./my-script.sh


----------------

$ echo '#!/bin/sh' > ms.sh
$ echo 'cd /home/user/jdk-1-8/bin' >> ms.sh
echo 'java -jar restserverv2.jar' >> ms.sh
$ chmod 755 ms.sh
$ ./ms.sh
 

 
 
$ echo '#!/bin/sh' > mc.sh
$ echo 'cd /home/user/jdk-1-8/bin' >> mc.sh
echo 'java -jar mapleclient.jar' >> mc.sh
$ chmod 755 mc.sh
$ ./mc.sh



$ echo '#!/bin/sh' > mu.sh
$ echo 'cd /home/user/jdk-1-8/bin' >> mu.sh
echo 'java -jar launcherv2.jar' >> mu.sh
$ chmod 755 mu.sh
$ ./mu.sh
