package com.maple.mapleclient.restService;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import com.maple.weighbridge.entity.BranchMst;
import com.maple.weighbridge.entity.CompanyMst;
import com.maple.weighbridge.entity.TyreWeightSavingMst;
import com.maple.weighbridge.entity.WeighBridgeMaterialMst;
import com.maple.weighbridge.entity.WeighBridgeTareWeight;
import com.maple.weighbridge.entity.WeighBridgeVehicleMst;
import com.maple.weighbridge.entity.WeighBridgeVehicleTypeMst;
import com.maple.weighbridge.entity.WeighBridgeWeights;
import com.maple.weighbridge.voucherNumberService.VoucherNumber;

import javafx.scene.control.DatePicker;

@Component

public class RestCaller {

	public static String HOST;
	public static String HOST2;
	static boolean hostUpdated = false;

	static RestTemplate restTemplate = new RestTemplate();
	static RestTemplate restTemplatePdf = new RestTemplate();
	
	
	
	public static ResponseEntity<List<WeighBridgeWeights>> getWeighBridgeReport(String uDate1,String tDate) {

			String path = HOST + "weighbridegereportresource/weighbridegereport?fromdate=" + uDate1+"&&todate="+tDate;
			System.out.println("========path==========" + path);

			ResponseEntity<List<WeighBridgeWeights>> response = restTemplate.exchange(path, HttpMethod.GET, null,
					new ParameterizedTypeReference<List<WeighBridgeWeights>>() {
					});

			return response;

	}

	
	public static ResponseEntity<List<WeighBridgeWeights>> getWeighBridgeDetails(String uDate1) {

		String path = HOST + "weighbridgeweightsresource/getweighbridgedetails?selecteddate=" + uDate1;
		System.out.println("========path==========" + path);

		ResponseEntity<List<WeighBridgeWeights>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<WeighBridgeWeights>>() {
				});

		return response;

}


	public static ResponseEntity<List<WeighBridgeTareWeight>> WeighBridgeTareWeightSearchByVehicleNo(
			String searchData) {
		
		String path = HOST + "getalltareweight";
		System.out.println("========path==========" + path);

		ResponseEntity<List<WeighBridgeTareWeight>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<WeighBridgeTareWeight>>() {
				});

		return response;
	}



	public static ResponseEntity<List<WeighBridgeMaterialMst>> getAllWeighBridgeMaterialMst() {
		String path = HOST + "findallweighbridgematerialmst";
		System.out.println("========path==========" + path);

		ResponseEntity<List<WeighBridgeMaterialMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<WeighBridgeMaterialMst>>() {
				});

		return response;
	}



	public static ResponseEntity<List<WeighBridgeVehicleMst>> getAllWeighBridgeVehicleMst() {
		
		
		String path = HOST + "findallweighbridgevehiclemst";
		System.out.println("========path==========" + path);

		ResponseEntity<List<WeighBridgeVehicleMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<WeighBridgeVehicleMst>>() {
				});

		return response;
	}



	static public BranchMst getBranchDtls(String branchCode) {

		String path = HOST + "branchmstbybranchcode/" + branchCode;

		System.out.println(path);
		ResponseEntity<BranchMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<BranchMst>() {
				});

		return response.getBody();

	}



	public static ResponseEntity<WeighBridgeVehicleMst> getWeighBridgeVehicleMstByVehicleType(String vehicleType) {
		String path = HOST + "findweighbridgevehiclemstbyvehicletype/" + vehicleType;
		System.out.println(path);
		ResponseEntity<WeighBridgeVehicleMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<WeighBridgeVehicleMst>() {
				});

		return response;
	}
	
	static public ResponseEntity<CompanyMst> getCompanyMst(String mycompany) {
		String path = HOST2 + "companymst/" + mycompany + "/companymst";
		System.out.println(path);
		ResponseEntity<CompanyMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<CompanyMst>() {
				});

		return response;
	}



	public static ResponseEntity<WeighBridgeVehicleMst> SaveWeighBridgeVehicleMst(
			WeighBridgeVehicleMst weighBridgeVehicleMst) {
		return restTemplate.postForEntity(HOST + "saveweighbridgevehiclemst", weighBridgeVehicleMst,
				WeighBridgeVehicleMst.class);

	}



	public static ResponseEntity<WeighBridgeMaterialMst> getWeighBridgeMaterialMstByMaterial(String material) {
		String path = HOST + "findweighbridgematerialmstbymaterial/" + material;
		System.out.println(path);
		ResponseEntity<WeighBridgeMaterialMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<WeighBridgeMaterialMst>() {
				});

		return response;
	}



	public static ResponseEntity<WeighBridgeMaterialMst> SaveWeighBridgeMaterialMst(
			WeighBridgeMaterialMst weighBridgeMaterialMstSaved) {
		return restTemplate.postForEntity(HOST + "weighbridgematerialmst", weighBridgeMaterialMstSaved,
				WeighBridgeMaterialMst.class);

	}



	static public String getVoucherNumber(String uID) {

		System.out.println("==path==" + HOST + "vouchernumber?id=" + uID);

		return restTemplate.getForObject(HOST + "vouchernumber?id=" + uID, String.class);

	}



	public static ResponseEntity<WeighBridgeWeights> SaveWeighBridgeWeights(
			WeighBridgeWeights weighBridgeWeightsSaved) {
		return restTemplate.postForEntity(HOST + "saveweighbridgeweights", weighBridgeWeightsSaved,
				WeighBridgeWeights.class);

	}
	
	public static ResponseEntity<WeighBridgeTareWeight> getWeighBridgeTareWeightByVehicleNo(String vehicleNo) {
		String path = HOST + "weighbridgetareweightbyvehicleno/" + vehicleNo;
		System.out.println(path);
		ResponseEntity<WeighBridgeTareWeight> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<WeighBridgeTareWeight>() {
				});

		return response;
	}
	
	
	public static ResponseEntity<WeighBridgeWeights> getFirstWeightByVehicleNo(String vehicleNo, String VehicleloadStatus) {
		
		String path = HOST + "getfistweight/" + vehicleNo+"/"+VehicleloadStatus;
		System.out.println(path);
		ResponseEntity<WeighBridgeWeights> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<WeighBridgeWeights>() {
				});

		return response;
	}



	public static ResponseEntity<WeighBridgeTareWeight> SaveWeighBridgeTareWeight(
			WeighBridgeTareWeight weighBridgeTareWeightSaved) {
		return restTemplate.postForEntity(HOST + "saveweighbridgetareweight", weighBridgeTareWeightSaved,
				WeighBridgeTareWeight.class);

	}



	public static ResponseEntity<List<WeighBridgeWeights>> searchWeightByVehicleNo(String vehicleno) {

		String path = HOST + "getallweight?data=" + vehicleno;
		System.out.println(path);
		ResponseEntity<List<WeighBridgeWeights>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<WeighBridgeWeights>>() {
				});

		return response;

	}



	public static ResponseEntity<WeighBridgeWeights> getWeighBridgeWeightById(String id) {
		String path = HOST + "weighbridgeweights/" + id;
		System.out.println(path);
		ResponseEntity<WeighBridgeWeights> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<WeighBridgeWeights>() {
				});

		return response;
	}



	public static ResponseEntity<WeighBridgeVehicleTypeMst> saveWeighBridgeVehicleTypeMst(
			WeighBridgeVehicleTypeMst weighBridgeVehicleTypeMst) {
		
		return restTemplate.postForEntity(HOST + "saveweighbridgevehicletypemst", weighBridgeVehicleTypeMst,
				WeighBridgeVehicleTypeMst.class);
	}



	public static ResponseEntity<WeighBridgeVehicleTypeMst> getWeighBridgeVehicleTypeMstVehicleNumber(
			String vehicleNo) {
		String path = HOST + "findweighbridgevehicletypemstbyvehiclenumber/" + vehicleNo;
		System.out.println(path);
		ResponseEntity<WeighBridgeVehicleTypeMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<WeighBridgeVehicleTypeMst>() {
				});

		return response;
	}
	
	static public ResponseEntity<CompanyMst> saveCompanyMst(CompanyMst companyMst) {

		return restTemplate.postForEntity(HOST2 + "companymst", companyMst, CompanyMst.class);

	}
	
	static public ResponseEntity<BranchMst> SavebranchCreation(BranchMst branchcreation) {

		if (null == branchcreation) {
			System.out.println("Branch creation is null");
		}
		ResponseEntity<BranchMst> response = null;
		String path = HOST + "branch?companymstid=";
		System.out.println(" Caling " + path);
		response = restTemplate.postForEntity(HOST + "branch", branchcreation, BranchMst.class);
		System.out.println(path);

		return response;

	}



	public static void updateWeighBridgeWeights(WeighBridgeWeights weighBridgeWeights) {
		
		restTemplate.put(
				HOST + "weighbridgereportresurce/upateweighbridgeweightbyid",
				weighBridgeWeights);
		return;
		
	}





	public static ResponseEntity<WeighBridgeMaterialMst> getWeighBridgeMaterialMstByMaterialName(String selectedItem) {
		String path = HOST + "findweighbridgematerialmstbymaterial/" + selectedItem;
		System.out.println(path);
		ResponseEntity<WeighBridgeMaterialMst> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<WeighBridgeMaterialMst>() {
				});

		return response;
	}



	public static ResponseEntity<WeighBridgeMaterialMst> saveWeighBridgeMaterialMst(
			WeighBridgeMaterialMst weighBridgeMaterialMst) {
		return restTemplate.postForEntity(HOST + "weighbridgematerialmst", weighBridgeMaterialMst,
				WeighBridgeMaterialMst.class);
	}


//-------------------------------------weighbridge tyre saving window-------------------
	public static ResponseEntity<TyreWeightSavingMst> saveVehicleTyreWeight(TyreWeightSavingMst tyreWeightSavingMst) {
		return restTemplate.postForEntity(HOST + "tyreweightsavingresource/savetyreweight", tyreWeightSavingMst,
				TyreWeightSavingMst.class);
	}



	public static ResponseEntity<List<TyreWeightSavingMst>> getVehicleTyreWeight() {
		String path = HOST + "tyreweightsavingresource/showalltyreweightsaving";
		System.out.println(path);

		ResponseEntity<List<TyreWeightSavingMst>> response = restTemplate.exchange(path, HttpMethod.GET, null,
				new ParameterizedTypeReference<List<TyreWeightSavingMst>>() {
				});
		return response;
	}



	public static void deleteTyreWeightSaving(String id) {
		restTemplate.delete(HOST + "tyreweightsavingresource/deletetyreweightsaving/" + id);	
		
	}

	
	
}
