package com.maple.jasper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.maple.mapleclient.restService.RestCaller;
import com.maple.weighbridge.WeighbridgeApplication;
import com.maple.weighbridge.entity.BranchMst;
import com.maple.weighbridge.entity.WeighBridgeWeights;
import com.maple.weighbridge.utils.SystemSetting;

import javafx.application.HostServices;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Service
public class JasperPdfReportService {

	public static void WeighBridgeReport(String fdate, String tdate) throws JRException {

		String pdfToGenerate = SystemSetting.reportpath + "/WeighBridgeReport.pdf";

		// Call url
		ResponseEntity<List<WeighBridgeWeights>> weighBridgeWeightsReporttt = RestCaller.getWeighBridgeReport(fdate,
				tdate);

		List<WeighBridgeWeights> weighBridgeWeightsReportttList = weighBridgeWeightsReporttt.getBody();

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(
				weighBridgeWeightsReportttList);

		String jrxmlPath = "jrxml/WeighBridgeReport.jrxml";

		// Compile the Jasper report from .jrxml to .japser
		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		parameters.put("branchName", branchMst.getBranchName());
		parameters.put("Address1", branchMst.getBranchAddress1());
		parameters.put("fromdate", fdate);
		parameters.put("todate", tdate);
		Double totalpaidamount = 0.0;

		for (WeighBridgeWeights WeighBridgeReport : weighBridgeWeightsReportttList) {
			if (null != WeighBridgeReport.getFirstPaidAmount()) {

				totalpaidamount = totalpaidamount + (double) (WeighBridgeReport.getFirstPaidAmount());
						
			}
			if(null != WeighBridgeReport.getSecondPaidAmount())
			{
				totalpaidamount = totalpaidamount + (double) (WeighBridgeReport.getSecondPaidAmount());

			}
		}

		parameters.put("totalpaidAmount", totalpaidamount);

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = WeighbridgeApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);
	}

	public static void WeighBridgeInvoice(WeighBridgeWeights weighBridgeWeights) throws JRException {

		List<WeighBridgeWeights> weighBridgeWeightsList = new ArrayList<WeighBridgeWeights>();
		weighBridgeWeightsList.add(weighBridgeWeights);

		System.out.println("888888" + weighBridgeWeightsList);

		JRBeanCollectionDataSource jrBeanCollectionDataSource = new JRBeanCollectionDataSource(weighBridgeWeightsList);

		String jrxmlPath = "jrxml/wbinvoice.jrxml";

		// Compile the Jasper report from .jrxml to .japser

		HashMap parameters = new HashMap();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		/*
		 * parameters.put("BranchName", branchMst.getBranchName());
		 * parameters.put("Address1", branchMst.getBranchAddress1());
		 * parameters.put("phno", branchMst.getBranchTelNo());
		 * 
		 * 
		 * if(null==weighBridgeWeights.getSecondVoucherNumber()) {
		 * parameters.put("voucherNumber", weighBridgeWeights.getFirstVoucherNumber());
		 * parameters.put("voucherDate", weighBridgeWeights.getVoucherDate());
		 * parameters.put("PaidAmount", weighBridgeWeights.getFirstPaidAmount());
		 * 
		 * }else { parameters.put("voucherNumber",
		 * weighBridgeWeights.getSecondVoucherNumber()); parameters.put("voucherDate",
		 * weighBridgeWeights.getSndVoucherDate()); parameters.put("PaidAmount",
		 * weighBridgeWeights.getSecondPaidAmount()); }
		 */

		// New set of parameters
		parameters.put("company_name", SystemSetting.WEIGHCOMPANYTITLE);
		parameters.put("voucher_date", weighBridgeWeights.getVoucherDate().toString());
		
		
		if(null==weighBridgeWeights.getSecondVoucherNumber()) {
			 parameters.put("voucher_no", weighBridgeWeights.getFirstVoucherNumber().toString());
			 parameters.put("voucher_date", weighBridgeWeights.getVoucherDate().toString());
			 parameters.put("paid_amount", weighBridgeWeights.getFirstPaidAmount()+"/-");
			 
			 }else { parameters.put("voucher_no", weighBridgeWeights.getSecondVoucherNumber().toString());
			 parameters.put("voucher_date",weighBridgeWeights.getSndVoucherDate().toString());
			 parameters.put("paid_amount", weighBridgeWeights.getSecondPaidAmount()+"/-");
			 }
			 
		
		
		
		
//		parameters.put("voucher_no", weighBridgeWeights.getFirstVoucherNumber());
		parameters.put("first_weight", weighBridgeWeights.getPreviousweight().toString()+"Kg");

		if (null != weighBridgeWeights.getNextweight()) {
			parameters.put("second_weight", weighBridgeWeights.getNextweight().toString()+"Kg");
		}
		if (null != weighBridgeWeights.getFirstweightdate()) {
			parameters.put("first_weight_date", weighBridgeWeights.getFirstweightdate());
		}
		if (null != weighBridgeWeights.getSecondWeightdate()) {
			parameters.put("second_weight_date", weighBridgeWeights.getSecondWeightdate());
		}

		Integer netWeight;
		if (null != weighBridgeWeights.getPreviousweight() && null != weighBridgeWeights.getNextweight()) {

			netWeight = weighBridgeWeights.getPreviousweight() - weighBridgeWeights.getNextweight();
			if(netWeight<0) {
				netWeight = -1 * netWeight;
			}
			parameters.put("net_weight", netWeight.toString()+"Kg");

		}else if(null != weighBridgeWeights.getPreviousweight() && null == weighBridgeWeights.getNextweight()){
			parameters.put("net_weight", weighBridgeWeights.getPreviousweight().toString()+"Kg");
		}

		parameters.put("paid_amount", weighBridgeWeights.getFirstPaidAmount().toString()+"/-");

		parameters.put("address_line1",SystemSetting.WEIGHCOMPANYADDRESS1);
		parameters.put("address_line2",SystemSetting.WEIGHCOMPANYADDRESS2);
		parameters.put("tel_no",SystemSetting.WEIGHCOMPANYADDRESS3 );
		
		String voucherNumber = "";
		
		if(null== weighBridgeWeights.getSecondVoucherNumber()) {
			voucherNumber = weighBridgeWeights.getFirstVoucherNumber();
		}else {
			voucherNumber = weighBridgeWeights.getSecondVoucherNumber();
		}

		String pdfToGenerate = SystemSetting.reportpath + "/" + voucherNumber + ".pdf";

		JasperReport jasperReport = JasperCompileManager.compileReport(jrxmlPath);

		// Fill the report
		JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, jrBeanCollectionDataSource);
		JasperExportManager.exportReportToPdfFile(jasperPrint, pdfToGenerate);

		HostServices hs = WeighbridgeApplication.mapleclientApplication.getHostServerFromMain();

		hs.showDocument(pdfToGenerate);
	}

}
