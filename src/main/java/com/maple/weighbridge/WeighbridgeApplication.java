package com.maple.weighbridge;

import java.sql.Connection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.EnvironmentAware;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.core.env.Environment;
import org.springframework.core.io.FileSystemResource;

import com.maple.mapleclient.restService.RestCaller;
import com.maple.weighbridge.controller.WBMainFrameController;
import com.maple.weighbridge.dataenrich.FinalAmount;
import com.maple.weighbridge.dataenrich.FirstWeight;
import com.maple.weighbridge.dataenrich.MakeInvoice;
import com.maple.weighbridge.dataenrich.PaidAmountForEmptyVehicle;
import com.maple.weighbridge.dataenrich.PaidAmountForLoadVehicle;
import com.maple.weighbridge.dataenrich.RateByWheel;
import com.maple.weighbridge.dataenrich.SavedTareWeight;
import com.maple.weighbridge.service.SavedVehicleDataFetch;
import com.maple.weighbridge.service.WeighBridgeEventInitiator;

import javafx.application.Application;
import javafx.application.HostServices;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Screen;
import javafx.stage.Stage;

@SpringBootApplication
@Configuration
@PropertySource("classpath:application.properties")

@EnableAutoConfiguration

public class WeighbridgeApplication extends Application implements EnvironmentAware {

	public static Parent rootNode;
	public static FXMLLoader loader;
	public ConfigurableApplicationContext context;
	private static ApplicationContext applicationContext;

	public static Stage primarySate;

	public static WBMainFrameController mainFrameController;
	public static Connection conn;

	public static AnchorPane mainWorkArea;
	public static Node payments;

	public static WeighbridgeApplication mapleclientApplication;

	@Bean
	public PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {

		PropertySourcesPlaceholderConfigurer properties = new PropertySourcesPlaceholderConfigurer();
		properties.setLocation(new FileSystemResource("application.properties"));
		properties.setIgnoreResourceNotFound(false);

		return properties;
	}

	public static void main(String[] args) {
		launch();
	}

	@Override
	public void setEnvironment(Environment environment) {

		final String myhost = environment.getProperty("myhost");
		final String myhost2 = environment.getProperty("myhost2");

		if (null != myhost) {
			RestCaller.HOST = myhost;
		}
		if (null != myhost2) {
			RestCaller.HOST2 = myhost2;

		}

	}

	@Override
	public void init() throws Exception {

		mapleclientApplication = this;
		// DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

		SpringApplicationBuilder builder = new SpringApplicationBuilder(WeighbridgeApplication.class);
		context = builder.run(getParameters().getRaw().toArray(new String[0]));

		loader = new FXMLLoader(getClass().getResource("/fxml/login.fxml"));

		loader.setControllerFactory(context::getBean);
		rootNode = loader.load();
//        insertItemmst(getDbConn("memdb"));

		applicationContext = new AnnotationConfigApplicationContext(WeighbridgeApplication.class);

		for (String beanName : applicationContext.getBeanDefinitionNames()) {
			System.out.println(beanName);
		}

	}

	@Override
	public void start(Stage primaryStage) throws Exception {

		primarySate = primaryStage;

		Rectangle2D visualBounds = Screen.getPrimary().getVisualBounds();
		double width = visualBounds.getWidth() / 2;
		double height = visualBounds.getHeight() / 1.5;

		primaryStage.setScene(new Scene(rootNode, width, height));
		primaryStage.centerOnScreen();
		primaryStage.show();

		WeighBridgeEventInitiator.getInstance();

		SavedTareWeight.getInstance();
		RateByWheel.getInstance();
		PaidAmountForEmptyVehicle.getInstance();
		MakeInvoice.getInstance();
		FirstWeight.getInstance();
		FinalAmount.getInstance();
		
		PaidAmountForLoadVehicle.getInstance();

		SavedVehicleDataFetch.getInstance();

	}

	@Override
	public void stop() throws Exception {
		context.close();
	}

	public HostServices getHostServerFromMain() {
		return getHostServices();

	}

}
