package   com.maple.weighbridge.voucherNumberService;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import com.sun.istack.NotNull;

/**
 * Represents an invoice for a transaction of goods or services.
 */
@Entity
 
@Table(name = "voucher_number" , uniqueConstraints={
	    @UniqueConstraint(columnNames = {"code", "company"})
}) 
public class VoucherNumber extends Model
{
  @Column(name = "code",  updatable = false)
  @NotNull
  private final String code;
 
  @Column(name = "company",   updatable = false)
  @NotNull
  private String company;
  
  /**
   * Deliberately hidden to prevent direct instantiation.
   */
  public VoucherNumber()
  {
    this(null,null);
  }

  /**
   * Creates an invoice with a specified unique code.
   *
   * @param code The unique invoice code.
   */
  public VoucherNumber(final String code, String companyMstId )
  {
    this.code = code;
    this.company = companyMstId;
    
    
  }

  /**
   * Gets the unique invoice code.
   *
   * @return The unique invoice code.
   */
 /* public String getCode()
  {
    return code;
  }
*/
  
public String getCompany() {
	return company;
}

public void setCompany(String company) {
	this.company = company;
}

public String getCode() {
	return code;
}
 
}
