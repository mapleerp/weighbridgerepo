package  com.maple.weighbridge.voucherNumberService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Generates invoice codes as sequential numbers.
 */
@Component
public class VoucherCodeGenerator extends SequenceGenerator
{
	  


/**
   * Gets the name of the sequence to use for generating
   * invoice codes.
   *
   * @return The name of the sequence to use for generating
   * invoice codes.
   */
  String getName(String uCode)
  {
    return uCode;
  }
}
