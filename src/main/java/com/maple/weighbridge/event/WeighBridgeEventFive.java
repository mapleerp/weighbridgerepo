package com.maple.weighbridge.event;

import com.maple.weighbridge.entity.WeighBridgeVehicleMst;
import com.maple.weighbridge.entity.WeighBridgeVehicleTypeMst;
import com.maple.weighbridge.entity.WeighBridgeWeights;

public class WeighBridgeEventFive {
	
	
	
	//=================================================
	WeighBridgeWeights weighBridgeWeights;
	WeighBridgeEventOne weighBridgeEventOne;
	WeighBridgeVehicleTypeMst weighBridgeVehicleTypeMst;
	WeighBridgeVehicleMst weighBridgeVehicleMst;
	
	Integer previousAmount;
	Integer emptyAmount;
	Integer cashToPay;
	Integer loadAmount;
	Integer balanceAmount;
	
	Integer previosWeight;


	public WeighBridgeEventOne getWeighBridgeEventOne() {
		return weighBridgeEventOne;
	}
	public void setWeighBridgeEventOne(WeighBridgeEventOne weighBridgeEventOne) {
		this.weighBridgeEventOne = weighBridgeEventOne;
	}
	public WeighBridgeVehicleTypeMst getWeighBridgeVehicleTypeMst() {
		return weighBridgeVehicleTypeMst;
	}
	public void setWeighBridgeVehicleTypeMst(WeighBridgeVehicleTypeMst weighBridgeVehicleTypeMst) {
		this.weighBridgeVehicleTypeMst = weighBridgeVehicleTypeMst;
	}
	public WeighBridgeWeights getWeighBridgeWeights() {
		return weighBridgeWeights;
	}
	public void setWeighBridgeWeights(WeighBridgeWeights weighBridgeWeights) {
		this.weighBridgeWeights = weighBridgeWeights;
	}
	public WeighBridgeVehicleMst getWeighBridgeVehicleMst() {
		return weighBridgeVehicleMst;
	}
	public void setWeighBridgeVehicleMst(WeighBridgeVehicleMst weighBridgeVehicleMst) {
		this.weighBridgeVehicleMst = weighBridgeVehicleMst;
	}
	public Integer getPreviousAmount() {
		return previousAmount;
	}
	public void setPreviousAmount(Integer previousAmount) {
		this.previousAmount = previousAmount;
	}
	
	public Integer getCashToPay() {
		return cashToPay;
	}
	public void setCashToPay(Integer cashToPay) {
		this.cashToPay = cashToPay;
	}
	public Integer getEmptyAmount() {
		return emptyAmount;
	}
	public void setEmptyAmount(Integer emptyAmount) {
		this.emptyAmount = emptyAmount;
	}
	public Integer getLoadAmount() {
		return loadAmount;
	}
	public void setLoadAmount(Integer loadAmount) {
		this.loadAmount = loadAmount;
	}
	public Integer getBalanceAmount() {
		return balanceAmount;
	}
	public void setBalanceAmount(Integer balanceAmount) {
		this.balanceAmount = balanceAmount;
	}
	public Integer getPreviosWeight() {
		return previosWeight;
	}
	public void setPreviosWeight(Integer previosWeight) {
		this.previosWeight = previosWeight;
	}
	
	

}
