package com.maple.weighbridge.event;

import java.util.HashMap;
import java.util.Map;

public class WeighingMachineDataEvent {
	
	String weightData;
	String vehicleNo;
	
	Map<String, String> observersList = new HashMap<String, String>();

	@Override
	public String toString() {
		return "WeighingMachineDataEvent [weightData=" + weightData + "]";
	}

	public String getWeightData() {
		return weightData;
	}

	public void setWeightData(String weightData) {
		this.weightData = weightData;
	}

	public String getVehicleNo() {
		return vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	public Map<String, String> getObserversList() {
		return observersList;
	}

	public void setObserversList(Map<String, String> observersList) {
		this.observersList = observersList;
	}
	 
	 
	

}
