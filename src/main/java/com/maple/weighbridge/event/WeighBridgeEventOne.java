package com.maple.weighbridge.event;

import java.util.HashMap;
import java.util.Map;

import org.springframework.context.annotation.ComponentScan;
@ComponentScan
public class WeighBridgeEventOne {
	
	String vehicleNo;
	String vehicleLoadStatus;
	String vehicleType;

	
	Map<String, String> observersList = new HashMap<String, String>();

	public String getVehicleNo() {
		return vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	public Map<String, String> getObserversList() {
		return observersList;
	}

	public String getVehicleLoadStatus() {
		return vehicleLoadStatus;
	}

	public void setVehicleLoadStatus(String vehicleLoadStatus) {
		this.vehicleLoadStatus = vehicleLoadStatus;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	
	


}
