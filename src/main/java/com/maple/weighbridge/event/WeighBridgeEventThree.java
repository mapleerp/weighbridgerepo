package com.maple.weighbridge.event;

import com.maple.weighbridge.entity.WeighBridgeVehicleTypeMst;
import com.maple.weighbridge.entity.WeighBridgeWeights;

public class WeighBridgeEventThree {
	
	
	WeighBridgeWeights weighBridgeWeights;
	WeighBridgeEventOne weighBridgeEventOne;
	WeighBridgeVehicleTypeMst weighBridgeVehicleTypeMst;
	
	public WeighBridgeEventOne getWeighBridgeEventOne() {
		return weighBridgeEventOne;
	}
	public void setWeighBridgeEventOne(WeighBridgeEventOne weighBridgeEventOne) {
		this.weighBridgeEventOne = weighBridgeEventOne;
	}
	public WeighBridgeVehicleTypeMst getWeighBridgeVehicleTypeMst() {
		return weighBridgeVehicleTypeMst;
	}
	public void setWeighBridgeVehicleTypeMst(WeighBridgeVehicleTypeMst weighBridgeVehicleTypeMst) {
		this.weighBridgeVehicleTypeMst = weighBridgeVehicleTypeMst;
	}
	public WeighBridgeWeights getWeighBridgeWeights() {
		return weighBridgeWeights;
	}
	public void setWeighBridgeWeights(WeighBridgeWeights weighBridgeWeights) {
		this.weighBridgeWeights = weighBridgeWeights;
	}
	
	

}
