package com.maple.weighbridge.event;

import com.maple.weighbridge.entity.WeighBridgeVehicleTypeMst;

public class WeighBridgeEventTwo {
	
	
	
	//=================================================
	
	WeighBridgeEventOne weighBridgeEventOne;
	WeighBridgeVehicleTypeMst weighBridgeVehicleTypeMst;
	public WeighBridgeEventOne getWeighBridgeEventOne() {
		return weighBridgeEventOne;
	}
	public void setWeighBridgeEventOne(WeighBridgeEventOne weighBridgeEventOne) {
		this.weighBridgeEventOne = weighBridgeEventOne;
	}
	public WeighBridgeVehicleTypeMst getWeighBridgeVehicleTypeMst() {
		return weighBridgeVehicleTypeMst;
	}
	public void setWeighBridgeVehicleTypeMst(WeighBridgeVehicleTypeMst weighBridgeVehicleTypeMst) {
		this.weighBridgeVehicleTypeMst = weighBridgeVehicleTypeMst;
	}
	
	

}
