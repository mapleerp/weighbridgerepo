package com.maple.weighbridge.event;

public class WeighBridgeTareWeightEvent {
	String vehicleno;
	Integer tareweight;
	public String getVehicleno() {
		return vehicleno;
	}
	public void setVehicleno(String vehicleno) {
		this.vehicleno = vehicleno;
	}
	public Integer getTareweight() {
		return tareweight;
	}
	public void setTareweight(Integer tareweight) {
		this.tareweight = tareweight;
	}
	@Override
	public String toString() {
		return "WeighBridgeTareWeightEvent [vehicleno=" + vehicleno + ", tareweight=" + tareweight + "]";
	}
	

}
