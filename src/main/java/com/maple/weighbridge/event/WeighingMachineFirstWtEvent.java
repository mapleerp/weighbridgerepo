package com.maple.weighbridge.event;


import java.time.LocalDateTime;
import java.util.Date;

public class WeighingMachineFirstWtEvent {

	LocalDateTime voucherDate;
	String vehicleNo;
	Integer firstWeight;
	
	public LocalDateTime getVoucherDate() {
		return voucherDate;
	}
	public void setVoucherDate(LocalDateTime voucherDate) {
		this.voucherDate = voucherDate;
	}
	public String getVehicleNo() {
		return vehicleNo;
	}
	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}
	public Integer getFirstWeight() {
		return firstWeight;
	}
	public void setFirstWeight(Integer firstWeight) {
		this.firstWeight = firstWeight;
	}
	@Override
	public String toString() {
		return "WeighingMachineFirstWtEvent [voucherDate=" + voucherDate + ", vehicleNo=" + vehicleNo + ", firstWeight="
				+ firstWeight + "]";
	}
	
	
}
