package com.maple.weighbridge.service;

import java.util.HashMap;
import java.util.Map;

 
import org.springframework.stereotype.Component;

import com.google.common.eventbus.EventBus;
import com.maple.weighbridge.EventBusFactory;
import com.maple.weighbridge.event.WeighBridgeEventOne;


 
 public class WeighBridgeEventInitiator {
	Map<String, String> observersList = new HashMap<String, String>();
	
	 
	EventBus eventBus = EventBusFactory.getEventBus();
	
	
	 private static WeighBridgeEventInitiator instance = new WeighBridgeEventInitiator();

	 private WeighBridgeEventInitiator(){}

	 public static WeighBridgeEventInitiator getInstance(){
	      return instance;
	   }
	
	public void EventInitiator(String vehicleNo,String vehicleLoadStatus,String vehicleType) {
		
		System.out.println("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
		System.out.println("==============EventInitiator===========");
		WeighBridgeEventOne weighBridgeEvent = new WeighBridgeEventOne();
		weighBridgeEvent.setVehicleNo(vehicleNo);
		weighBridgeEvent.setVehicleLoadStatus(vehicleLoadStatus);
		
		if(null != vehicleType && vehicleType.length() > 0) {
			weighBridgeEvent.setVehicleType(vehicleType);
		}
		
		weighBridgeEvent.getObserversList().put("VehicleNo", vehicleNo);
		weighBridgeEvent.getObserversList().put("VehicleLoadStatus",vehicleLoadStatus);
		 
		eventBus.post(weighBridgeEvent);

	}

	public Map<String, String> getObserversList() {
		return observersList;
	}

	public void setObserversList(Map<String, String> observersList) {
		this.observersList = observersList;
	}
 

}
