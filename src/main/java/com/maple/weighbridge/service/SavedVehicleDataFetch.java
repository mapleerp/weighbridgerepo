package com.maple.weighbridge.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.weighbridge.EventBusFactory;
import com.maple.weighbridge.dataenrich.FinalAmount;
import com.maple.weighbridge.entity.BranchMst;
import com.maple.weighbridge.entity.CompanyMst;
import com.maple.weighbridge.entity.WeighBridgeVehicleTypeMst;
import com.maple.weighbridge.event.WeighBridgeEventOne;
import com.maple.weighbridge.event.WeighBridgeEventTwo;
import com.maple.weighbridge.utils.SystemSetting;

public class SavedVehicleDataFetch {

	EventBus eventBus = EventBusFactory.getEventBus();

	WeighBridgeVehicleTypeMst weighBridgeVehicleTypeMst = null;

	WeighBridgeEventOne weighBridgeEvent = null;
	
	WeighBridgeEventTwo weighBridgeEventTwo = null;

//	@Autowired
//	WeighBridgeVehicleTypeRepository weighBridgeVehicleTypeRepository;

	private SavedVehicleDataFetch() {
		eventBus.register(this);
	}

	private static SavedVehicleDataFetch instance = new SavedVehicleDataFetch();

	public static SavedVehicleDataFetch getInstance() {

		return instance;
	}

	@Subscribe
	public void SavedVehicleDataFetch(WeighBridgeEventOne weighBridgeEvent) {
		this.weighBridgeEvent = weighBridgeEvent;

		if (null != this.weighBridgeEvent.getVehicleNo()) {

			this.weighBridgeVehicleTypeMst = getSavedVehicleDataByVehicleNumber(this.weighBridgeEvent.getVehicleNo());

			if (null == this.weighBridgeVehicleTypeMst) {

				if (null != this.weighBridgeEvent.getVehicleType()
						&& this.weighBridgeEvent.getVehicleType().length() > 0) {

					this.weighBridgeVehicleTypeMst = saveBridgeVehicleType(this.weighBridgeEvent.getVehicleNo(),
							this.weighBridgeEvent.getVehicleType());

				}
			}
			
			WeighBridgeEventTwo weighBridgeEventTwo = new WeighBridgeEventTwo();
			
			weighBridgeEventTwo.setWeighBridgeEventOne(this.weighBridgeEvent);
			weighBridgeEventTwo.setWeighBridgeVehicleTypeMst(this.weighBridgeVehicleTypeMst);
			
			this.weighBridgeEventTwo = weighBridgeEventTwo;
			eventBus.post(this.weighBridgeEventTwo);

			putbackEnrichedData();
		}
	}

	public void putbackEnrichedData() {

		eventBus.post(this);

	}

	public WeighBridgeVehicleTypeMst saveBridgeVehicleType(String vehicleNo, String vehicleType) {

		WeighBridgeVehicleTypeMst weighBridgeVehicleTypeMst = new WeighBridgeVehicleTypeMst();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.getSystemBranch());

//		Optional<BranchMst> branchMstOpt = branchMstRepository
//				.findByBranchCodeAndCompanyMstId(SystemSetting.getSystemBranch(), SystemSetting.myCompany);

		weighBridgeVehicleTypeMst.setBranchMst(branchMst);

		ResponseEntity<CompanyMst> companyOpt = RestCaller.getCompanyMst(SystemSetting.myCompany);

		weighBridgeVehicleTypeMst.setCompanyMst(companyOpt.getBody());
		weighBridgeVehicleTypeMst.setVehicleno(vehicleNo);
		weighBridgeVehicleTypeMst.setVehicletype(vehicleType);

		ResponseEntity<WeighBridgeVehicleTypeMst> weighBridgeVehicleTypeMstSaved = RestCaller
				.saveWeighBridgeVehicleTypeMst(weighBridgeVehicleTypeMst);

//		weighBridgeVehicleTypeMst = weighBridgeVehicleTypeRepository.saveAndFlush(weighBridgeVehicleTypeMst);

		return weighBridgeVehicleTypeMstSaved.getBody();
	}

	public WeighBridgeVehicleTypeMst getSavedVehicleDataByVehicleNumber(String vehicleNo) {

		ResponseEntity<WeighBridgeVehicleTypeMst> weighBridgeVehicleTypeMstResp = RestCaller
				.getWeighBridgeVehicleTypeMstVehicleNumber(vehicleNo);


		return (weighBridgeVehicleTypeMstResp.getBody());
	}

}
