package com.maple.weighbridge.dataenrich;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.jfree.util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.weighbridge.EventBusFactory;
import com.maple.weighbridge.service.WeighBridgeEventInitiator;



 
public class MakeInvoice {
	EventBus eventBus = EventBusFactory.getEventBus();
	FinalAmount finalAmount = null;
	
	Map<String, String> observersList = new HashMap<String, String>();
	private static MakeInvoice instance = new MakeInvoice();
	private MakeInvoice() {
		eventBus.register(this);
		 
	}

	public static MakeInvoice getInstance() {

		return instance;
	}

	@Subscribe
	public void weighBridgeEventListner(FinalAmount finalAmount) {
		this.finalAmount = finalAmount;

	 
 for (Entry<String, String> entry : finalAmount.getObserversList().entrySet()) {
			 
			 
			 observersList.put(entry.getKey() , entry.getValue());
			 
				System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
			 

				 
			 }
		/*
		 * Call API to fetch saved data and set it to the event
		 */
 observersList.put( "MakeInvoice", "DONE");

		putbackEnrichedData();

	}

	public void putbackEnrichedData() {

	 	 
		eventBus.post(this);
		
		 
	 
	}

	public Map<String, String> getObserversList() {
		return observersList;
	}

	public void setObserversList(Map<String, String> observersList) {
		this.observersList = observersList;
	}

}
