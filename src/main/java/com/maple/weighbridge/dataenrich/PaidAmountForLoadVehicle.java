package com.maple.weighbridge.dataenrich;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.jfree.util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.weighbridge.EventBusFactory;
import com.maple.weighbridge.entity.WeighBridgeVehicleMst;
import com.maple.weighbridge.event.WeighBridgeEventFive;
import com.maple.weighbridge.event.WeighBridgeEventFour;
import com.maple.weighbridge.event.WeighBridgeEventThree;
import com.maple.weighbridge.service.WeighBridgeEventInitiator;

public class PaidAmountForLoadVehicle {
	EventBus eventBus = EventBusFactory.getEventBus();

	Map<String, String> observersList = new HashMap<String, String>();

	FirstWeight firstWeight = null;
	WeighBridgeEventFour weighBridgeEventFour = null;

	WeighBridgeVehicleMst weighBridgeVehicleMst = null;
	
	

	private static PaidAmountForLoadVehicle instance = new PaidAmountForLoadVehicle();

	private PaidAmountForLoadVehicle() {
		eventBus.register(this);

	}

	public static PaidAmountForLoadVehicle getInstance() {

		return instance;
	}

	@Subscribe
	public void weighBridgeEventListner(WeighBridgeEventFour weighBridgeEventFour) {
		this.weighBridgeEventFour = weighBridgeEventFour;

		if (null != this.weighBridgeEventFour.getWeighBridgeEventOne()
				&& null != this.weighBridgeEventFour.getWeighBridgeVehicleTypeMst()) {
			
			if (null == this.weighBridgeEventFour.getWeighBridgeWeights()) {
				getVehicleRate();

				
				WeighBridgeEventFive weighBridgeEventFive = new WeighBridgeEventFive();
				
				int cashToPay = this.weighBridgeVehicleMst.getLoadRate() + weighBridgeVehicleMst.getEmptyRate();


				weighBridgeEventFive.setWeighBridgeEventOne(this.weighBridgeEventFour.getWeighBridgeEventOne());
				weighBridgeEventFive.setWeighBridgeVehicleMst(this.weighBridgeVehicleMst);
				weighBridgeEventFive
						.setWeighBridgeVehicleTypeMst(this.weighBridgeEventFour.getWeighBridgeVehicleTypeMst());
				weighBridgeEventFive.setWeighBridgeWeights(this.weighBridgeEventFour.getWeighBridgeWeights());

				weighBridgeEventFive.setCashToPay(cashToPay);
				weighBridgeEventFive.setPreviousAmount(0);
				weighBridgeEventFive.setEmptyAmount(weighBridgeVehicleMst.getEmptyRate());
				weighBridgeEventFive.setLoadAmount(weighBridgeVehicleMst.getLoadRate());
//				weighBridgeEventFour.setBalanceAmount(balanceAmount);

				eventBus.post(weighBridgeEventFive);

			} else {

				getVehicleRate(); 
				
				
				
				WeighBridgeEventFive weighBridgeEventFive = new WeighBridgeEventFive();
				
				int cashToPay = this.weighBridgeVehicleMst.getLoadRate() + weighBridgeVehicleMst.getEmptyRate();

				weighBridgeEventFive.setWeighBridgeEventOne(this.weighBridgeEventFour.getWeighBridgeEventOne());
				weighBridgeEventFive.setWeighBridgeVehicleMst(this.weighBridgeVehicleMst);
				weighBridgeEventFive
						.setWeighBridgeVehicleTypeMst(this.weighBridgeEventFour.getWeighBridgeVehicleTypeMst());
				weighBridgeEventFive.setWeighBridgeWeights(this.weighBridgeEventFour.getWeighBridgeWeights());

				weighBridgeEventFive.setCashToPay(cashToPay);
				weighBridgeEventFive.setPreviousAmount(this.weighBridgeEventFour.getWeighBridgeWeights().getFirstPaidAmount());
				weighBridgeEventFive.setEmptyAmount(weighBridgeVehicleMst.getEmptyRate());
				weighBridgeEventFive.setLoadAmount(weighBridgeVehicleMst.getLoadRate());
				weighBridgeEventFive.setPreviosWeight(this.weighBridgeEventFour.getWeighBridgeWeights().getPreviousweight());
//				weighBridgeEventFour.setBalanceAmount(balanceAmount);
				
				Integer previousRate = this.weighBridgeEventFour.getWeighBridgeWeights().getRate();
				Integer prevoisAmount = this.weighBridgeEventFour.getWeighBridgeWeights().getFirstPaidAmount();
				if (prevoisAmount > previousRate)
				{
					if(this.weighBridgeEventFour.getWeighBridgeEventOne().getVehicleLoadStatus().equalsIgnoreCase("LOAD"))
					{
						Integer balanceAmount = prevoisAmount - previousRate;
						Integer cashToPayFn =   0;
						
						if(balanceAmount == 0)
						{
							cashToPayFn = this.weighBridgeVehicleMst.getLoadRate() + weighBridgeVehicleMst.getEmptyRate();

						}
						
						weighBridgeEventFive.setBalanceAmount(balanceAmount);
						weighBridgeEventFive.setCashToPay(cashToPayFn);
					}
				} 
				
				eventBus.post(weighBridgeEventFive);


			}
		}

		putbackEnrichedData();

	}

	private void getVehicleRate() {

		ResponseEntity<WeighBridgeVehicleMst> weighBridgeVehicleMst = RestCaller.getWeighBridgeVehicleMstByVehicleType(
				this.weighBridgeEventFour.getWeighBridgeVehicleTypeMst().getVehicletype());

		this.weighBridgeVehicleMst = weighBridgeVehicleMst.getBody();

	}

	public void putbackEnrichedData() {

		eventBus.post(this);

	}

	public Map<String, String> getObserversList() {
		return observersList;
	}

	public void setObserversList(Map<String, String> observersList) {
		this.observersList = observersList;
	}

}
