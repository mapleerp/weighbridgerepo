package com.maple.weighbridge.dataenrich;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.jfree.util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.weighbridge.EventBusFactory;
import com.maple.weighbridge.service.WeighBridgeEventInitiator;

public class FinalAmount {
	EventBus eventBus = EventBusFactory.getEventBus();
	Map<String, String> observersList = new HashMap<String, String>();
	PaidAmountForEmptyVehicle paidAmount = null;

	private static FinalAmount instance = new FinalAmount();

	private FinalAmount() {
		eventBus.register(this);

	}

	public static FinalAmount getInstance() {

		return instance;
	}

	@Subscribe
	public void weighBridgeDestinationEventListner(PaidAmountForEmptyVehicle paidAmount) {
		this.paidAmount = paidAmount;

		for (Entry<String, String> entry : paidAmount.getObserversList().entrySet()) {

			observersList.put(entry.getKey(), entry.getValue());
			System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());

		}

		observersList.put("FinalAmount", "DONE");

		/*
		 * Call API to fetch saved data and set it to the event
		 */
	}

	public void putbackEnrichedData() {

		eventBus.post(this);

	}

	public Map<String, String> getObserversList() {
		return observersList;
	}

	public void setObserversList(Map<String, String> observersList) {
		this.observersList = observersList;
	}

}
