package com.maple.weighbridge.dataenrich;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.jfree.util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.weighbridge.EventBusFactory;
import com.maple.weighbridge.entity.WeighBridgeVehicleTypeMst;
import com.maple.weighbridge.entity.WeighBridgeWeights;
import com.maple.weighbridge.event.WeighBridgeEventFour;
import com.maple.weighbridge.event.WeighBridgeEventOne;
import com.maple.weighbridge.event.WeighBridgeEventThree;
import com.maple.weighbridge.event.WeighBridgeEventTwo;
import com.maple.weighbridge.service.SavedVehicleDataFetch;
import com.maple.weighbridge.service.WeighBridgeEventInitiator;

public class FirstWeight {

	EventBus eventBus = EventBusFactory.getEventBus();
	Map<String, String> observersList = new HashMap<String, String>();
	WeighBridgeEventTwo weighBridgeEventTwo = null;

	SavedVehicleDataFetch savedVehicleDataFetch = null;

	WeighBridgeEventThree weighBridgeEventThree = null;
	
	WeighBridgeEventFour weighBridgeEventFour = null;


	WeighBridgeVehicleTypeMst WeighBridgeVehicleTypeMst = null;
	WeighBridgeEventOne weighBridgeEventOne = null;

	WeighBridgeWeights weighBridgeWeights = null;
	private static FirstWeight instance = new FirstWeight();

	private FirstWeight() {
		eventBus.register(this);

	}

	public static FirstWeight getInstance() {

		return instance;
	}

	@Subscribe
	public void weighBridgeEventListner(WeighBridgeEventTwo weighBridgeEventTwo) {
		this.weighBridgeEventTwo = weighBridgeEventTwo;

		if (null != this.weighBridgeEventTwo) {
			this.weighBridgeEventOne = this.weighBridgeEventTwo.getWeighBridgeEventOne();
			this.WeighBridgeVehicleTypeMst = this.weighBridgeEventTwo.getWeighBridgeVehicleTypeMst();

			if (null != this.weighBridgeEventOne) {
				if (this.weighBridgeEventOne.getVehicleLoadStatus().equalsIgnoreCase("EMPTY")) {
					
					getPrevoisData("EMPTY");
					
					WeighBridgeEventThree weighBridgeEventThree = new WeighBridgeEventThree();
					weighBridgeEventThree.setWeighBridgeEventOne(this.weighBridgeEventTwo.getWeighBridgeEventOne());
					weighBridgeEventThree.setWeighBridgeVehicleTypeMst(this.weighBridgeEventTwo.getWeighBridgeVehicleTypeMst());
					weighBridgeEventThree.setWeighBridgeWeights(this.weighBridgeWeights);

					this.weighBridgeEventThree = weighBridgeEventThree;

					eventBus.post(this.weighBridgeEventThree);
					
				} else {
					
					getPrevoisData("LOAD");

					WeighBridgeEventFour WeighBridgeEventFour1 = new WeighBridgeEventFour();
					
					WeighBridgeEventFour1.setWeighBridgeEventOne(this.weighBridgeEventTwo.getWeighBridgeEventOne());
					WeighBridgeEventFour1.setWeighBridgeVehicleTypeMst(this.weighBridgeEventTwo.getWeighBridgeVehicleTypeMst());
					WeighBridgeEventFour1.setWeighBridgeWeights(this.weighBridgeWeights);
					
					this.weighBridgeEventFour = WeighBridgeEventFour1;
					
					eventBus.post(this.weighBridgeEventFour);


				}
			}
		}

		putbackEnrichedData();

	}

	private void getPreviousEmpty() {

	}

	private void getPrevoisData(String lordStatus) {

		ResponseEntity<WeighBridgeWeights> weighBridgeWeightsResp = RestCaller.getFirstWeightByVehicleNo(
				this.WeighBridgeVehicleTypeMst.getVehicleno(), lordStatus);

		this.weighBridgeWeights = weighBridgeWeightsResp.getBody();
	}

	public void putbackEnrichedData() {

		eventBus.post(this);

	}

}
