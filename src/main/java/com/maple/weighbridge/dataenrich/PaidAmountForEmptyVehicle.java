package com.maple.weighbridge.dataenrich;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.jfree.util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.weighbridge.EventBusFactory;
import com.maple.weighbridge.entity.WeighBridgeVehicleMst;
import com.maple.weighbridge.event.WeighBridgeEventFive;
import com.maple.weighbridge.event.WeighBridgeEventThree;
import com.maple.weighbridge.service.WeighBridgeEventInitiator;

public class PaidAmountForEmptyVehicle {
	EventBus eventBus = EventBusFactory.getEventBus();

	Map<String, String> observersList = new HashMap<String, String>();

	FirstWeight firstWeight = null;
	WeighBridgeEventThree weighBridgeEventThree = null;

	WeighBridgeVehicleMst weighBridgeVehicleMst = null;
	
	

	private static PaidAmountForEmptyVehicle instance = new PaidAmountForEmptyVehicle();

	private PaidAmountForEmptyVehicle() {
		eventBus.register(this);

	}

	public static PaidAmountForEmptyVehicle getInstance() {

		return instance;
	}

	@Subscribe
	public void weighBridgeEventListner(WeighBridgeEventThree weighBridgeEventThree) {
		this.weighBridgeEventThree = weighBridgeEventThree;

		if (null != this.weighBridgeEventThree.getWeighBridgeEventOne()
				&& null != this.weighBridgeEventThree.getWeighBridgeVehicleTypeMst()) {
			
			if (null == this.weighBridgeEventThree.getWeighBridgeWeights()) {
				getVehicleRate();

				WeighBridgeEventFive weighBridgeEventFive = new WeighBridgeEventFive();


				weighBridgeEventFive.setWeighBridgeEventOne(this.weighBridgeEventThree.getWeighBridgeEventOne());
				weighBridgeEventFive.setWeighBridgeVehicleMst(this.weighBridgeVehicleMst);
				weighBridgeEventFive
						.setWeighBridgeVehicleTypeMst(this.weighBridgeEventThree.getWeighBridgeVehicleTypeMst());
				weighBridgeEventFive.setWeighBridgeWeights(this.weighBridgeEventThree.getWeighBridgeWeights());

				weighBridgeEventFive.setCashToPay(this.weighBridgeVehicleMst.getEmptyRate());
				weighBridgeEventFive.setPreviousAmount(0);
				weighBridgeEventFive.setEmptyAmount(weighBridgeVehicleMst.getEmptyRate());
				weighBridgeEventFive.setLoadAmount(weighBridgeVehicleMst.getLoadRate());
//				weighBridgeEventFour.setBalanceAmount(balanceAmount);

				eventBus.post(weighBridgeEventFive);

			} else {

				getVehicleRate(); 
				
				
				
				WeighBridgeEventFive weighBridgeEventFive = new WeighBridgeEventFive();
				
				

				weighBridgeEventFive.setWeighBridgeEventOne(this.weighBridgeEventThree.getWeighBridgeEventOne());
				weighBridgeEventFive.setWeighBridgeVehicleMst(this.weighBridgeVehicleMst);
				weighBridgeEventFive
						.setWeighBridgeVehicleTypeMst(this.weighBridgeEventThree.getWeighBridgeVehicleTypeMst());
				weighBridgeEventFive.setWeighBridgeWeights(this.weighBridgeEventThree.getWeighBridgeWeights());

				weighBridgeEventFive.setCashToPay(this.weighBridgeVehicleMst.getEmptyRate());
				weighBridgeEventFive.setPreviousAmount(this.weighBridgeEventThree.getWeighBridgeWeights().getFirstPaidAmount());
				weighBridgeEventFive.setEmptyAmount(weighBridgeVehicleMst.getEmptyRate());
				weighBridgeEventFive.setLoadAmount(weighBridgeVehicleMst.getLoadRate());
				weighBridgeEventFive.setPreviosWeight(this.weighBridgeEventThree.getWeighBridgeWeights().getPreviousweight());
//				weighBridgeEventFour.setBalanceAmount(balanceAmount);
				
				Integer previousRate = this.weighBridgeEventThree.getWeighBridgeWeights().getRate();
				Integer prevoisAmount = this.weighBridgeEventThree.getWeighBridgeWeights().getFirstPaidAmount();
				if (prevoisAmount > previousRate)
				{
					if(this.weighBridgeEventThree.getWeighBridgeEventOne().getVehicleLoadStatus().equalsIgnoreCase("EMPTY"))
					{
						Integer balanceAmount = prevoisAmount - previousRate;
						Integer cashToPay = weighBridgeVehicleMst.getEmptyRate()-balanceAmount;
						
						weighBridgeEventFive.setBalanceAmount(balanceAmount);
						weighBridgeEventFive.setCashToPay(cashToPay);
					}
				} 
				
				eventBus.post(weighBridgeEventFive);


			}
		}

		putbackEnrichedData();

	}

	private void getVehicleRate() {

		ResponseEntity<WeighBridgeVehicleMst> weighBridgeVehicleMst = RestCaller.getWeighBridgeVehicleMstByVehicleType(
				this.weighBridgeEventThree.getWeighBridgeVehicleTypeMst().getVehicletype());

		this.weighBridgeVehicleMst = weighBridgeVehicleMst.getBody();

	}

	public void putbackEnrichedData() {

		eventBus.post(this);

	}

	public Map<String, String> getObserversList() {
		return observersList;
	}

	public void setObserversList(Map<String, String> observersList) {
		this.observersList = observersList;
	}

}
