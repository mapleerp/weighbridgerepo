package com.maple.weighbridge.dataenrich;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.jfree.util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.weighbridge.EventBusFactory;
import com.maple.weighbridge.service.WeighBridgeEventInitiator;

 
public class RateByWheel {
	EventBus eventBus = EventBusFactory.getEventBus();

	Map<String, String> observersList = new HashMap<String, String>();
	SavedTareWeight savedTareWeight =null;
	 

	private static RateByWheel instance = new RateByWheel();
	WeighBridgeEventInitiator weighBridgeEventInitiator = WeighBridgeEventInitiator.getInstance();

	private RateByWheel() {
		eventBus.register(this);
		 
	}

	public static RateByWheel getInstance() {

		return instance;
	}

	@Subscribe
	public void weighBridgeEventListner(SavedTareWeight savedTareWeight) {
		this.savedTareWeight = savedTareWeight;

  for (Entry<String, String> entry : savedTareWeight.getObserversList().entrySet()) {
			 
			 
			 observersList.put(entry.getKey() , entry.getValue());
			 
				System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
			 

				 
			 }
  observersList.put( "RateByWheel", "DONE");
	 

		/*
		 * Call API to fetch fetch rate by whhel and set it to the event
		 */
		

		putbackEnrichedData();

	}

	public void putbackEnrichedData() {

 
		eventBus.post(this);
		
		 
	 
	}

	public Map<String, String> getObserversList() {
		return observersList;
	}

	public void setObserversList(Map<String, String> observersList) {
		this.observersList = observersList;
	}

}
