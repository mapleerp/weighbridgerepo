package com.maple.weighbridge.dataenrich;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.jfree.util.Log;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.weighbridge.EventBusFactory;
import com.maple.weighbridge.event.WeighBridgeEventOne;
import com.maple.weighbridge.service.WeighBridgeEventInitiator;

 
public class SavedTareWeight {
	EventBus eventBus = EventBusFactory.getEventBus();
	Map<String, String> observersList = new HashMap<String, String>();

 
	WeighBridgeEventOne weighBridgeEvent = null;
	private static SavedTareWeight instance = new SavedTareWeight();
 
	private SavedTareWeight() {
		eventBus.register(this);
		 
	}

	public static SavedTareWeight getInstance() {

		return instance;
	}

//	@Subscribe
//	public void weighBridgeEventListner(WeighBridgeEventOne weighBridgeEvent) {
//		this.weighBridgeEvent = weighBridgeEvent;
//
//	 
//
//		 for (Entry<String, String> entry : weighBridgeEvent.getObserversList().entrySet()) {
//			 
//			 
//			 observersList.put(entry.getKey() , entry.getValue());
//			 
//				System.out.println("Key = " + entry.getKey() + ", Value = " + entry.getValue());
//				
//
//				 
//			 }
//		 observersList.put( "SavedTareWeight", "DONE");
//
//		putbackEnrichedData();
//
//	}

	public void putbackEnrichedData() {

  
		eventBus.post(this);
		
		 
		
	}

	public Map<String, String> getObserversList() {
		return observersList;
	}

	public void setObserversList(Map<String, String> observersList) {
		this.observersList = observersList;
	}

}
