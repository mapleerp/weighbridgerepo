package com.maple.weighbridge.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
 
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.weighbridge.EventBusFactory;

 
import com.maple.weighbridge.entity.WeighBridgeWeights;
import com.maple.weighbridge.event.WeighingMachineFirstWtEvent;
  
public class WeighingMachineFirstWtPopUp {
	
	String taskid;
	String processInstanceId;
	
	boolean initializedCalled = false;

	WeighingMachineFirstWtEvent weighingMachineFirstWtEvent;
	private EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<WeighBridgeWeights> popUpItemList = FXCollections.observableArrayList();
	StringProperty SearchString = new SimpleStringProperty();

    @FXML
    private TableView<WeighBridgeWeights> tbFirstWt;

    @FXML
    private TableColumn<WeighBridgeWeights, String> clDate;

    @FXML
    private TableColumn<WeighBridgeWeights, String> clVehicleNo;

    @FXML
    private TableColumn<WeighBridgeWeights, Number> clFirstWt;

    @FXML
    private TextField txtVehicleNo;

    @FXML
    private Button btnOk;

    @FXML
    private Button btnCancel;
    @FXML
	private void initialize() {
		if (initializedCalled)
			return;

		initializedCalled = true;
		System.out.println("initializedCalledinitializedCalled");
	weighingMachineFirstWtEvent = new WeighingMachineFirstWtEvent();
		eventBus.register(this);
		btnOk.setDefaultButton(true);

		btnCancel.setCache(true);
		
		txtVehicleNo.textProperty().bindBidirectional(SearchString);
		LoadItemPopupBySearch("");
		tbFirstWt.setItems(popUpItemList);
		clDate.setCellValueFactory(cellData -> cellData.getValue().getvoucherDateProperty());
		clFirstWt.setCellValueFactory(cellData -> cellData.getValue().getMachineWeightProperty());
		clVehicleNo.setCellValueFactory(cellData -> cellData.getValue().getvehiclenoProperty());
		tbFirstWt.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {

		if (newSelection != null) {
			weighingMachineFirstWtEvent = new WeighingMachineFirstWtEvent();
			weighingMachineFirstWtEvent.setFirstWeight(newSelection.getMachineweight());
			weighingMachineFirstWtEvent.setVehicleNo(newSelection.getVehicleno());
			weighingMachineFirstWtEvent.setVoucherDate(newSelection.getVoucherDate());
		}
		});
		SearchString.addListener(new ChangeListener() {

			@Override
			public void changed(ObservableValue observable, Object oldValue, Object newValue) {

				LoadItemPopupBySearch((String) newValue);
			}
		});
    }
    @FXML
    void actionCancel(ActionEvent event) {
    	Stage stage = (Stage) btnCancel.getScene().getWindow();
    	weighingMachineFirstWtEvent.setFirstWeight(null);
		eventBus.post(weighingMachineFirstWtEvent);
		stage.close();
    }

    @FXML
    void actionOk(ActionEvent event) {
    	Stage stage = (Stage) btnCancel.getScene().getWindow();
		eventBus.post(weighingMachineFirstWtEvent);
		stage.close();

    }
    @FXML
    void onKeyPress(KeyEvent event) {
    	if (event.getCode() == KeyCode.ENTER) {
			eventBus.post(weighingMachineFirstWtEvent);
			Stage stage = (Stage) btnCancel.getScene().getWindow();
			stage.close();
		} else if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN
				|| event.getCode() == KeyCode.TAB || event.getCode() == KeyCode.UP || event.getCode() == KeyCode.KP_UP) {

		} else {
			txtVehicleNo.requestFocus();
		}

		initializedCalled = false;

    }

    @FXML
    void onKeyPressText(KeyEvent event) {
    	if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
    		tbFirstWt.requestFocus();
			tbFirstWt.getSelectionModel().selectFirst();
		}
		if (event.getCode() == KeyCode.ESCAPE) {
			
			weighingMachineFirstWtEvent.setFirstWeight(null);
			eventBus.post(weighingMachineFirstWtEvent);
			Stage stage = (Stage) btnCancel.getScene().getWindow();
			stage.close();
		}
		if(event.getCode() == KeyCode.BACK_SPACE && txtVehicleNo.getText().length()==0)
		{
			weighingMachineFirstWtEvent.setFirstWeight(null);
			eventBus.post(weighingMachineFirstWtEvent);
			Stage stage = (Stage) btnCancel.getScene().getWindow();
			stage.close();
		}
    }

    private void LoadItemPopupBySearch(String searchData)
    {
    	ResponseEntity<List<WeighBridgeWeights>> getAllWeights = RestCaller.searchWeightByVehicleNo(searchData);
    	popUpItemList = FXCollections.observableArrayList(getAllWeights.getBody());
    	tbFirstWt.setItems(popUpItemList);
    }
 


   	private void PageReload(String hdrId) {

   	}
    

}
