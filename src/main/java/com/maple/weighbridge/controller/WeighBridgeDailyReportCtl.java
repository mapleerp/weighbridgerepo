package com.maple.weighbridge.controller;

import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import com.google.common.eventbus.Subscribe;
import com.maple.jasper.JasperPdfReportService;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.weighbridge.entity.BranchMst;
import com.maple.weighbridge.entity.CompanyMst;
import com.maple.weighbridge.entity.WeighBridgeWeights;
import com.maple.weighbridge.utils.SystemSetting;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;
public class WeighBridgeDailyReportCtl {
	
	String taskid;
	String processInstanceId;

	private ObservableList<WeighBridgeWeights> weighBridgeReportList = FXCollections.observableArrayList();

	WeighBridgeWeights weighBridgeWeights; 
    @FXML
    private DatePicker dpStartDate;

    @FXML
    private DatePicker dpToDate;

    @FXML
    private TextField txtGrandTotal;
    
 
    @FXML
    private Button btnGenerateReport;

    @FXML
    private TableView<WeighBridgeWeights> tblReport;

    @FXML
    private TableColumn<WeighBridgeWeights, String> clVehicleNumber;

    @FXML
    private TableColumn<WeighBridgeWeights, String> clfirstWeightDate;

    @FXML
    private TableColumn<WeighBridgeWeights, String> clMaterial;

    @FXML
    private TableColumn<WeighBridgeWeights, Number> clRate;
    

    

    @FXML
    private Button btnPrintReport;

   

   

    @FXML
   	private void initialize() {
    	dpStartDate = SystemSetting.datePickerFormat(dpStartDate, "dd/MMM/yyyy");
    	dpToDate = SystemSetting.datePickerFormat(dpToDate, "dd/MMM/yyyy");
    	tblReport.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					WeighBridgeWeights  weighBridgeWeights= new WeighBridgeWeights();
					weighBridgeWeights.setId(newSelection.getId());
					
				}
			}
    	});
       }
 

    @FXML
    void GenerateReport(ActionEvent event) {
    	if (null == dpStartDate.getValue()) {

//			notifyMessage(3, "please select From date", false);
			dpStartDate.requestFocus();
			return;
		}
		if (null == dpToDate.getValue()) {

//			notifyMessage(3, "please select Todate", false);
			dpToDate.requestFocus();
			return;
		}

		java.util.Date uDate = Date.valueOf(dpStartDate.getValue());
		String fromdate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date u1Date = Date.valueOf(dpToDate.getValue());
		String todate = SystemSetting.UtilDateToString(u1Date, "yyy-MM-dd");


		ResponseEntity<List<WeighBridgeWeights>> weighBridgeReportResp = RestCaller.getWeighBridgeReport(fromdate, todate);

		weighBridgeReportList = FXCollections.observableArrayList(weighBridgeReportResp.getBody());

		fillTable();
		
		   Integer grandTotal=0;
		   for(int i=0;i<weighBridgeReportList.size();i++) {
		    		
		    		
		    		Integer total=weighBridgeReportList.get(i).getFirstPaidAmount();
		    		
		    		grandTotal=total+grandTotal;
		    	}
		  
		   Double grndTotal=Double.valueOf(grandTotal);
		   txtGrandTotal.setText(String.valueOf(grndTotal));

	}
    
    private void fillTable() {
    	tblReport.setItems(weighBridgeReportList);

		clVehicleNumber.setCellValueFactory(cellData -> cellData.getValue().getvehiclenoProperty());
		clfirstWeightDate.setCellValueFactory(cellData -> cellData.getValue().getFirstwtDateProperty());
		clMaterial.setCellValueFactory(cellData -> cellData.getValue().getMaterialtypeidProperty());
		clRate.setCellValueFactory(cellData -> cellData.getValue().getFirstPaidAmountProperty());
		
		
	}
    
    
    @FXML
    void PrintReport(ActionEvent event) {
    	java.util.Date uDate = Date.valueOf(dpStartDate.getValue());
		String fromdate = SystemSetting.UtilDateToString(uDate, "yyy-MM-dd");
		java.util.Date u1Date = Date.valueOf(dpToDate.getValue());
		String todate = SystemSetting.UtilDateToString(u1Date, "yyy-MM-dd");

		try {
			JasperPdfReportService.WeighBridgeReport(fromdate, todate);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    }

    
    

	/*
	 * public void notifyMessage(int duration, String msg, boolean success) {
	 * 
	 * Image img; if (success) { img = new Image("done.png");
	 * 
	 * } else { img = new Image("failed.png"); }
	 * 
	 * Notifications notificationBuilder =
	 * Notifications.create().text(msg).graphic(new ImageView(img))
	 * .hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
	 * .onAction(new EventHandler<ActionEvent>() {
	 * 
	 * @Override public void handle(ActionEvent event) {
	 * System.out.println("clicked on notification"); } });
	 * notificationBuilder.darkStyle(); notificationBuilder.show();
	 * 
	 * }
	 */
	

}
