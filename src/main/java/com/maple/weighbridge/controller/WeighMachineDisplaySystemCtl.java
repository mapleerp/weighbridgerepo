package com.maple.weighbridge.controller;

import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.function.UnaryOperator;
import java.util.regex.Pattern;

import org.apache.poi.ss.formula.ptg.TblPtg;
import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.lowagie.text.pdf.CMapAwareDocumentFont;
import com.maple.jasper.JasperPdfReportService;

import com.maple.mapleclient.restService.RestCaller;
import com.maple.weighbridge.EventBusFactory;
import com.maple.weighbridge.WeighingMachine;
import com.maple.weighbridge.WeighingMachinePrint;
import com.maple.weighbridge.entity.BranchMst;
import com.maple.weighbridge.entity.CompanyMst;
import com.maple.weighbridge.entity.WeighBridgeMaterialMst;
import com.maple.weighbridge.entity.WeighBridgeVehicleMst;
import com.maple.weighbridge.entity.WeighBridgeVehicleTypeMst;
import com.maple.weighbridge.entity.WeighBridgeWeights;
import com.maple.weighbridge.event.WeighBridgeEventFive;
import com.maple.weighbridge.event.WeighBridgeEventOne;
import com.maple.weighbridge.event.WeighBridgeEventThree;
import com.maple.weighbridge.event.WeighingMachineDataEvent;
import com.maple.weighbridge.event.WeighingMachineFirstWtEvent;
import com.maple.weighbridge.service.WeighBridgeEventInitiator;
import com.maple.weighbridge.utils.SystemSetting;

import javafx.application.Platform;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.Duration;
import net.sf.jasperreports.engine.JRException;

public class WeighMachineDisplaySystemCtl {
	private LocalDate firstWtDate = null;

	private String firstWeight = "0";
	private EventBus eventBus = EventBusFactory.getEventBus();
	WeighBridgeEventOne weighBridgeEvent = null;

	WeighBridgeWeights weighBridgeWeightsSelected = null;

	StringProperty weightfromMachine = new SimpleStringProperty("");

	ObservableList<WeighBridgeWeights> weighBridgeWeightsList = FXCollections.observableArrayList();

	@FXML
	private TableView<WeighBridgeWeights> tbWeighingMachine;

	@FXML
	private TableColumn<WeighBridgeWeights, String> cldate;

	@FXML
	private TableColumn<WeighBridgeWeights, String> clVehicleNumber;

	@FXML
	private TableColumn<WeighBridgeWeights, Number> clFirstWt;

	@FXML
	private TableColumn<WeighBridgeWeights, Number> clSecondWt;

	@FXML
	private TextField txtVehicleNumber;

	@FXML
	private TextField txtFirstWt;

	@FXML
	private TextField txtwtFromMachine;

	@FXML
	private TextField txtNumberOfCopies;

	@FXML
	private ComboBox<String> cmbVehicleType;

	@FXML
	private ComboBox<String> cmbMaterial;

	@FXML
	private TextField txtRate;

	@FXML
	private Button btnAdd;

	@FXML
	private Button btnPrint;

	@FXML
	private Button btnclear;

	@FXML
	private DatePicker dpDate;

	@FXML
	private Button btnDelete;

	@FXML
	private TextField txtCashToPay;

	@FXML
	private TextField txtEmptyAmount;

	@FXML
	private TextField txtLoadAmount;

	@FXML
	private TextField txtPaidAmount;

	@FXML
	private TextField txtBalanceAmount;

	@FXML
	private TextField txtPreviousRate;

	@FXML
	private Button btnOk;

	@FXML
	private Button btnReady;

	@FXML
	private void initialize() {

		dpDate.setValue(LocalDate.now());
		eventBus.register(this);

		txtwtFromMachine.textProperty().bindBidirectional(weightfromMachine);
		weightfromMachine.set("0");

		alphaNumericFormatter(txtVehicleNumber);

		cmbVehicleType.getItems().add("4");
		cmbVehicleType.getItems().add("6");
		cmbVehicleType.getItems().add("8");
		cmbVehicleType.getItems().add("10");
		cmbVehicleType.getItems().add("12");
		cmbVehicleType.getItems().add("14");
		cmbVehicleType.getItems().add("16");

		/*
		 * Thread t = new Thread(new WeighingMachine(SystemSetting.WEIGHBRIDGEPORT,
		 * SystemSetting.WEIGHBRIDGEBAUDRATE, SystemSetting.WEIGHBRIDGEDATABITS,
		 * SystemSetting.WEIGHBRIDGESTOPBIT, SystemSetting.WEIGHBRIDGEPARITY));
		 * t.start();
		 */

		getMaterialDetails();

		tbWeighingMachine.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				weighBridgeWeightsSelected = newSelection;
			}
		});
	}

	@FXML
	void btnAction(ActionEvent event) {



		String vhType = getVehicleType();

		ResponseEntity<WeighBridgeVehicleMst> weighBridgeVehicleMst = RestCaller
				.getWeighBridgeVehicleMstByVehicleType(vhType);

		WeighBridgeVehicleMst wbmst = weighBridgeVehicleMst.getBody();

		double emptyAmount = wbmst.getEmptyRate();

		double loadAmount = wbmst.getLoadRate();

		double cashToPay = emptyAmount + loadAmount;
		String vehicleLoadStatus = "EMPTY";

		if (Double.parseDouble(firstWeight) > 0) {
			cashToPay = 0;
			vehicleLoadStatus = "LOAD";
		}

		if (null != wbmst) {
			cmbVehicleType.getSelectionModel().select(wbmst.getVehicletype());
		}

		if (null != wbmst) {
			txtPreviousRate.setText(wbmst.getEmptyRate().toString());
		}

		txtRate.setText(cashToPay + "");

		txtCashToPay.setText(cashToPay + "");

		txtFirstWt.setText(firstWeight);
		
		/*
		 * Thread t = new Thread(new WeighingMachine(SystemSetting.WEIGHBRIDGEPORT,
		 * SystemSetting.WEIGHBRIDGEBAUDRATE, SystemSetting.WEIGHBRIDGEDATABITS,
		 * SystemSetting.WEIGHBRIDGESTOPBIT, SystemSetting.WEIGHBRIDGEPARITY));
		 * t.start();
		 */

		WeighingMachine weighingMachine = new WeighingMachine(SystemSetting.WEIGHBRIDGEPORT,
				SystemSetting.WEIGHBRIDGEBAUDRATE, SystemSetting.WEIGHBRIDGEDATABITS, SystemSetting.WEIGHBRIDGESTOPBIT,
				SystemSetting.WEIGHBRIDGEPARITY);

		String wtFromMachine = weighingMachine.getWeightFromMachine(SystemSetting.WEIGHBRIDGEPORT,
				SystemSetting.WEIGHBRIDGEBAUDRATE, SystemSetting.WEIGHBRIDGEDATABITS, SystemSetting.WEIGHBRIDGESTOPBIT,
				SystemSetting.WEIGHBRIDGEPARITY);

		String wData = wtFromMachine;

		if (wData.toUpperCase().contains("KG")) {

			Platform.runLater(() -> {
				weightfromMachine.set(wtFromMachine);
			});

		}
		

	}

	private String getVehicleType() {
		ResponseEntity<WeighBridgeVehicleTypeMst> weighBridgeVehicleTypeMstResp = RestCaller
				.getWeighBridgeVehicleTypeMstVehicleNumber(txtVehicleNumber.getText().trim());

		// firstWeight

		WeighBridgeVehicleTypeMst weighBridgeVehicleTypeMst = weighBridgeVehicleTypeMstResp.getBody();

		String vhType = cmbVehicleType.getSelectionModel().getSelectedItem();

		if (null != weighBridgeVehicleTypeMst) {
			vhType = weighBridgeVehicleTypeMst.getVehicletype();

		}
		return vhType;
	}

	@FXML
	void MaterialOnAction(ActionEvent event) {

	}

	@FXML
	void onActionVehicle(ActionEvent event) {

	}

	@FXML
	void actionAdd(ActionEvent event) {

		if (txtVehicleNumber.getText().trim().isEmpty()) {
			txtVehicleNumber.requestFocus();
			return;
		}

		if (txtCashToPay.getText().trim().isEmpty()) {
			txtCashToPay.requestFocus();
			return;
		}

		if (!txtCashToPay.getText().equals("0")) {
			if (txtPaidAmount.getText().trim().isEmpty()) {
				txtPaidAmount.requestFocus();
				return;
			}
		}

		if (txtwtFromMachine.getText().trim().isEmpty()) {
			txtwtFromMachine.requestFocus();
			return;
		}

		String vhType = getVehicleType();

		// ResponseEntity<WeighBridgeVehicleTypeMst> weighBridgeVehicleTypeMstResp =
		// RestCaller
		// .getWeighBridgeVehicleTypeMstVehicleNumber(txtVehicleNumber.getText().trim());

		// firstWeight

		// WeighBridgeVehicleTypeMst weighBridgeVehicleTypeMst =
		// weighBridgeVehicleTypeMstResp.getBody();

		ResponseEntity<WeighBridgeVehicleMst> weighBridgeVehicleMst = RestCaller
				.getWeighBridgeVehicleMstByVehicleType(vhType);

		WeighBridgeVehicleMst wbmst = weighBridgeVehicleMst.getBody();

		double emptyAmount = wbmst.getEmptyRate();

		double loadAmount = wbmst.getLoadRate();

		double cashToPay = emptyAmount + loadAmount;
		String vehicleLoadStatus = "EMPTY";
		if (Double.parseDouble(firstWeight) > 0) {
			cashToPay = 0;
			vehicleLoadStatus = "LOAD";
		}

		// Integer emptyAmount = Integer.parseInt(txtEmptyAmount.getText());
		// Integer loadAmount = Integer.parseInt(txtLoadAmount.getText());

		BranchMst branchMst = RestCaller.getBranchDtls("WB");

		ResponseEntity<CompanyMst> companyMstResp = RestCaller.getCompanyMst("WB");

		ResponseEntity<WeighBridgeMaterialMst> weighBridgeMaterialMstResp = RestCaller
				.getWeighBridgeMaterialMstByMaterialName(cmbMaterial.getSelectionModel().getSelectedItem());
		if (null == weighBridgeMaterialMstResp.getBody()) {

			WeighBridgeMaterialMst weighBridgeMaterialMst = new WeighBridgeMaterialMst();
			weighBridgeMaterialMst.setMaterial(cmbMaterial.getSelectionModel().getSelectedItem());
			if (null != branchMst) {
				weighBridgeMaterialMst.setBranchMst(branchMst);
			}
			if (null != companyMstResp.getBody()) {
				weighBridgeMaterialMst.setCompanyMst(companyMstResp.getBody());
			}
			ResponseEntity<WeighBridgeMaterialMst> weighBridgeMaterialMstSaved = RestCaller
					.saveWeighBridgeMaterialMst(weighBridgeMaterialMst);

		}

		ResponseEntity<WeighBridgeWeights> weighBridgeWeightsResp = RestCaller
				.getFirstWeightByVehicleNo(txtVehicleNumber.getText(), vehicleLoadStatus);

		WeighBridgeWeights weighBridgeWeights = weighBridgeWeightsResp.getBody();

		String voucherNum = RestCaller.getVoucherNumber("WB");

		LocalDate vDate = dpDate.getValue();

		LocalDateTime vDateTime = vDate.atStartOfDay();

		Date date = SystemSetting.localToUtilDate(dpDate.getValue());
		String dateStr = SystemSetting.UtilDateToString(date, "yyyy-MM-dd");

		if (null == weighBridgeWeights) {

			weighBridgeWeights = new WeighBridgeWeights();

			weighBridgeWeights.setBranchMst(branchMst);
			weighBridgeWeights.setFirstCashToPay((int) cashToPay);
			weighBridgeWeights.setCompanyMst(companyMstResp.getBody());
			weighBridgeWeights.setFirstweightdate(dateStr);

			weighBridgeWeights.setMachineweight(weightAsInt());
			weighBridgeWeights.setPreviousweight(weightAsInt());

			weighBridgeWeights.setFirstPaidAmount((int) cashToPay);

			if (vehicleLoadStatus.equals("EMPTY")) {
				weighBridgeWeights.setRate((int) emptyAmount);

			}
			if (vehicleLoadStatus.equals("LOAD")) {
				weighBridgeWeights.setRate((int) loadAmount);

			}
			weighBridgeWeights.setVehicleno(txtVehicleNumber.getText());
			weighBridgeWeights.setVehicletypeid(vhType);

			weighBridgeWeights.setFirstVoucherNumber(voucherNum);
			weighBridgeWeights.setVoucherDate(vDateTime);
			weighBridgeWeights.setStatus("INCOMPLETE");
			weighBridgeWeights.setFtVehicleloadStatus(vehicleLoadStatus);

			weighBridgeWeights.setMaterialtypeid(cmbMaterial.getSelectionModel().getSelectedItem());

			ResponseEntity<WeighBridgeWeights> weighBridgeWeightsSaved = RestCaller
					.SaveWeighBridgeWeights(weighBridgeWeights);
			weighBridgeWeights = weighBridgeWeightsSaved.getBody();

		} else {

			weighBridgeWeights.setSecondVoucherNumber(voucherNum);
			weighBridgeWeights.setBranchMst(branchMst);
			weighBridgeWeights.setCompanyMst(companyMstResp.getBody());
			weighBridgeWeights.setNextweight(weightAsInt());
			weighBridgeWeights.setMachineweight(weightAsInt());
			weighBridgeWeights.setSecondCashToPay((int) cashToPay);
			weighBridgeWeights.setSecondPaidAmount((int) cashToPay);

			if (vehicleLoadStatus.equals("EMPTY")) {
				weighBridgeWeights.setSecondRate((int) emptyAmount);

			}
			if (vehicleLoadStatus.equals("LOAD")) {
				weighBridgeWeights.setSecondRate((int) loadAmount);

			}

			weighBridgeWeights.setSecondWeightdate(dateStr);
			weighBridgeWeights.setVehicleno(txtVehicleNumber.getText());
			weighBridgeWeights.setStatus("COMPLETE");
			weighBridgeWeights.setSndVehicleloadStatus(vehicleLoadStatus);
			weighBridgeWeights.setSndVoucherDate(vDateTime);

			weighBridgeWeights.setMaterialtypeid(cmbMaterial.getSelectionModel().getSelectedItem());

			RestCaller.updateWeighBridgeWeights(weighBridgeWeights);

		}
//--------------------------------billing-----------------------------

		try {
			JasperPdfReportService.WeighBridgeInvoice(weighBridgeWeights);
		} catch (JRException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		printAction(weighBridgeWeights);
		clearAll();

		weighBridgeWeights = null;
		getMaterialDetails();
		txtVehicleNumber.setEditable(true);

	}

	public void printAction(WeighBridgeWeights weighBridgeWeights) {
		WeighingMachinePrint wprint = new WeighingMachinePrint();

		if (null == weighBridgeWeights) {
			return;
		}

		try {

			Integer noOfCopies = 1;
			if (!txtNumberOfCopies.getText().trim().isEmpty()) {
				noOfCopies = Integer.parseInt(txtNumberOfCopies.getText());
			}

			if (null == weighBridgeWeights.getSecondVoucherNumber()
					|| weighBridgeWeights.getSecondVoucherNumber().length() == 0) {
				while (noOfCopies > 0) {
					wprint.PrintInvoiceThermalPrinter(weighBridgeWeights.getPreviousweight() + "",
							weighBridgeWeights.getMachineweight() + "", weighBridgeWeights.getPreviousweight() + "",
							weighBridgeWeights.getVehicleno(), weighBridgeWeights.getFirstPaidAmount() + "",
							weighBridgeWeights.getFirstweightdate(), weighBridgeWeights.getFirstVoucherNumber(),
							weighBridgeWeights.getFirstweightdate() + "");
					noOfCopies = noOfCopies - 1;
				}
			} else {
				int netValue = weighBridgeWeights.getPreviousweight() - weighBridgeWeights.getNextweight();
				if (netValue < 0) {
					netValue = netValue * -1;
				}
				while (noOfCopies > 0) {
					wprint.PrintInvoiceThermalPrinter(weighBridgeWeights.getNextweight() + "",
							weighBridgeWeights.getPreviousweight() + "", netValue + "",
							weighBridgeWeights.getVehicleno(), weighBridgeWeights.getSecondPaidAmount() + "",
							weighBridgeWeights.getSecondWeightdate(), weighBridgeWeights.getSecondVoucherNumber(),
							weighBridgeWeights.getFirstweightdate());
					noOfCopies = noOfCopies - 1;
					while (noOfCopies > 0) {
						wprint.PrintInvoiceThermalPrinter(weighBridgeWeights.getNextweight() + "",
								weighBridgeWeights.getPreviousweight() + "", netValue + "",
								weighBridgeWeights.getVehicleno(), weighBridgeWeights.getSecondPaidAmount() + "",
								weighBridgeWeights.getSecondWeightdate(), weighBridgeWeights.getSecondVoucherNumber(),
								weighBridgeWeights.getFirstweightdate() + "");
						noOfCopies = noOfCopies - 1;
					}
				}

			}

			/*
			 * String firstWeight, String secondWt, String netWeight, String vehicleNO,
			 * String rateAmount , String dateInvoice)
			 */
		} catch (SQLException e) {

			e.printStackTrace();
		}

	}

	private void clearAll() {

		txtVehicleNumber.clear();
		txtBalanceAmount.clear();
		txtCashToPay.clear();
		txtEmptyAmount.clear();
		txtFirstWt.setText("0");
		txtLoadAmount.clear();
		txtNumberOfCopies.setText("1");
		txtPaidAmount.clear();
		txtPreviousRate.clear();
		txtRate.clear();
		txtVehicleNumber.clear();
		txtwtFromMachine.setText("0");
		cmbVehicleType.getSelectionModel().clearSelection();
		cmbMaterial.getSelectionModel().clearSelection();

		firstWeight = "0";
		txtVehicleNumber.setEditable(true);

	}

	@FXML
	void actionClear(ActionEvent event) {
		firstWeight = "0";
		txtVehicleNumber.clear();
		txtBalanceAmount.clear();
		txtCashToPay.clear();
		txtEmptyAmount.clear();
		txtFirstWt.setText("0");
		txtLoadAmount.clear();
		txtNumberOfCopies.setText("1");
		txtPaidAmount.clear();
		txtPreviousRate.clear();
		txtRate.clear();
		txtVehicleNumber.clear();
		txtwtFromMachine.setText("0");
		cmbVehicleType.getSelectionModel().clearSelection();
		txtVehicleNumber.setEditable(true);

	}

	@FXML
	void actionPrint(ActionEvent event) {
		if (null != weighBridgeWeightsSelected) {
			printAction(weighBridgeWeightsSelected);
		}

	}

	@FXML
	void firstWtOnEnter(KeyEvent event) {

	}

	@FXML
	void onactionvehicle(ActionEvent event) {

	}

	@FXML
	void VehicleNumberOnKeyPress(KeyEvent event) {

		if (event.getCode() == KeyCode.ENTER) {
			loadFirstWeight();
		}

	}

	@FXML
	void actionShowAll(ActionEvent event) {

		Date vDate = SystemSetting.localToUtilDate(dpDate.getValue());
		String vStartDate = SystemSetting.UtilDateToString(vDate, "yyyy-MM-dd");

		if (null == vDate) {
			notifyMessage(3, "Please select the date", false);
		}

		ResponseEntity<List<WeighBridgeWeights>> weighBridgeWeightsResp = RestCaller.getWeighBridgeDetails(vStartDate);
		if (null != weighBridgeWeightsResp.getBody()) {
			weighBridgeWeightsList = FXCollections.observableArrayList(weighBridgeWeightsResp.getBody());
		}
		filltable();
	}

	private void filltable() {
		tbWeighingMachine.setItems(weighBridgeWeightsList);
		cldate.setCellValueFactory(cellData -> cellData.getValue().getvoucherDateProperty());
		clVehicleNumber.setCellValueFactory(cellData -> cellData.getValue().getvehiclenoProperty());
		clFirstWt.setCellValueFactory(cellData -> cellData.getValue().getpreviousWtPropertyProperty());
		clSecondWt.setCellValueFactory(cellData -> cellData.getValue().getNextwtproperty());
	}

	private void getMaterialDetails() {
		cmbMaterial.getItems().clear();
		ResponseEntity<List<WeighBridgeMaterialMst>> weighBridgeMaterialMstList = RestCaller
				.getAllWeighBridgeMaterialMst();
		if (null != weighBridgeMaterialMstList.getBody()) {
			for (WeighBridgeMaterialMst weighBridgeMaterial : weighBridgeMaterialMstList.getBody()) {
				cmbMaterial.getItems().add(weighBridgeMaterial.getMaterial());
			}
		}
	}

	/*
	 * @Subscribe synchronized public void popuplistner(WeighingMachineDataEvent
	 * voucherNoEvent) {
	 * 
	 * String wData = voucherNoEvent.getWeightData().trim();
	 * 
	 * if (wData.toUpperCase().contains("KG")) {
	 * 
	 * Platform.runLater(() -> {
	 * weightfromMachine.set(voucherNoEvent.getWeightData().trim()); });
	 * 
	 * }
	 * 
	 * }
	 */
	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

	private void loadFirstWeight() {
		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/WeighingMachineFirstWtPopUp.fxml"));
			Parent root = loader.load();
			// PopupCtl popupctl = loader.getController();
			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();
//					dpSupplierInvDate.requestFocus();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// getReady()

	@Subscribe
	public void popuplistner(WeighingMachineFirstWtEvent weighingMachineFirstWtEvent) {

		Stage stage = (Stage) btnAdd.getScene().getWindow();
		if (stage.isShowing()) {

			firstWeight = weighingMachineFirstWtEvent.getFirstWeight().toString();
			txtFirstWt.setText(firstWeight);
			txtVehicleNumber.setText(weighingMachineFirstWtEvent.getVehicleNo());
			txtVehicleNumber.setEditable(false);

			if (null != weighingMachineFirstWtEvent.getVoucherDate()) {
				java.util.Date udate = SystemSetting
						.StringToUtilDate(weighingMachineFirstWtEvent.getVoucherDate().toString(), "yyyy-MM-dd");
				// dpDate.setValue(SystemSetting.utilToLocaDate(udate));
				firstWtDate = SystemSetting.utilToLocaDate(udate);

			}
		}
		// getReady();
		
		 
	}

	private boolean firstWeightStatus() {

		return !(null == txtFirstWt.getText() || (txtFirstWt.getText()).trim().length() < 2);
	}

	private void alphaNumericFormatter(TextField txtField) {
		Pattern pattern = Pattern.compile("[A-Z0-9]*");
		UnaryOperator<TextFormatter.Change> filter = c -> {
			if (pattern.matcher(c.getControlNewText()).matches()) {
				return c;
			} else {
				return null;
			}
		};
		TextFormatter<String> formatter = new TextFormatter<>(filter);
		txtField.setTextFormatter(formatter);
	}

	private int weightAsInt() {
		if (null == txtwtFromMachine.getText())
			return 0;
		if (txtwtFromMachine.getText().length() <= 2)
			return 0;

		String wtString = txtwtFromMachine.getText();
		wtString = wtString.replace("KG", "");
		Integer wtAsint = Integer.parseInt(wtString);

		return wtAsint;

	}

	/*
	 * on focus loose of Vehicle number text box
	 */
	private void fetchDetails(String vehicleNo) {

		/*
		 * step 1 get vehicletypeid from weigh_bridge_weights where vehicleno is equal
		 * to current veh.no and drop down of veh. number is disabled. if not vehicle
		 * found , the drop down is enabled.
		 * 
		 * step 2 fetch load rate and empty rate , sum it up and display.
		 * 
		 * 
		 */

		/*
		 * if previoud wt is present then find previous paid amount
		 */

	}
}
