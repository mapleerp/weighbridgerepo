package com.maple.weighbridge.controller;

import java.util.List;
import java.util.function.UnaryOperator;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.weighbridge.EventBusFactory;
import com.maple.weighbridge.entity.WeighBridgeTareWeight;
import com.maple.weighbridge.event.WeighBridgeTareWeightEvent;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextFormatter;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;

public class WeighBridgeTareWeightPopCtl {
	
	String taskid;
	String processInstanceId;
	
	boolean initializedCalled = false;

	WeighBridgeTareWeightEvent weighBridgeTareWeightEvent;
	private EventBus eventBus = EventBusFactory.getEventBus();
	private ObservableList<WeighBridgeTareWeight> popUpItemList = FXCollections.observableArrayList();
	StringProperty SearchString = new SimpleStringProperty();

    @FXML
    private TableView<WeighBridgeTareWeight> tbTareWeight;

    @FXML
    private TableColumn<WeighBridgeTareWeight, String> clVehicleNumber;

    @FXML
    private TableColumn<WeighBridgeTareWeight, Number> clTareWeight;

    @FXML
    private TextField txtVehicleNo;

    @FXML
    private Button btnOk;

    @FXML
    private Button btnCancel;
    @FXML
  	private void initialize() {
  		if (initializedCalled)
  			return;

  		initializedCalled = true;
  		
  		alphaNumericFormatter(txtVehicleNo);
  		System.out.println("initializedCalledinitializedCalled");
  		weighBridgeTareWeightEvent = new WeighBridgeTareWeightEvent();
  		eventBus.register(this);
  		btnOk.setDefaultButton(true);

  		btnCancel.setCache(true);
  		
  		txtVehicleNo.textProperty().bindBidirectional(SearchString);
  		LoadItemPopupBySearch("");
  		tbTareWeight.setItems(popUpItemList);
  		clTareWeight.setCellValueFactory(cellData -> cellData.getValue().gettareweightProperty());
  		clVehicleNumber.setCellValueFactory(cellData -> cellData.getValue().getvehiclenoProperty());
  		tbTareWeight.getSelectionModel().selectedItemProperty().addListener((obs, oldSelectionss, newSelection) -> {

  		if (newSelection != null) {
  			weighBridgeTareWeightEvent = new WeighBridgeTareWeightEvent();
  			weighBridgeTareWeightEvent.setTareweight(newSelection.getTareweight());
  			weighBridgeTareWeightEvent.setVehicleno(newSelection.getVehicleno());
  		}
  		});
  		SearchString.addListener(new ChangeListener() {

  			@Override
  			public void changed(ObservableValue observable, Object oldValue, Object newValue) {

  				LoadItemPopupBySearch((String) newValue);
  			}
  		});
      }
    @FXML
    void actionCancel(ActionEvent event) {

    	Stage stage = (Stage) btnCancel.getScene().getWindow();
    	weighBridgeTareWeightEvent.setTareweight(null);
		eventBus.post(weighBridgeTareWeightEvent);
		stage.close();
    
    }

    @FXML
    void actionOk(ActionEvent event) {

    	Stage stage = (Stage) btnCancel.getScene().getWindow();
		eventBus.post(weighBridgeTareWeightEvent);
		stage.close();

    
    }

    @FXML
    void onKeyPress(KeyEvent event) {

    	if (event.getCode() == KeyCode.ENTER) {
			eventBus.post(weighBridgeTareWeightEvent);
			Stage stage = (Stage) btnCancel.getScene().getWindow();
			stage.close();
		} else if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN
				|| event.getCode() == KeyCode.TAB || event.getCode() == KeyCode.UP || event.getCode() == KeyCode.KP_UP) {

		} else {
			txtVehicleNo.requestFocus();
		}

		initializedCalled = false;

    

    }

    @FXML
    void onKeyPressText(KeyEvent event) {

    	if (event.getCode() == KeyCode.DOWN || event.getCode() == KeyCode.KP_DOWN) {
    		tbTareWeight.requestFocus();
			tbTareWeight.getSelectionModel().selectFirst();
		}
		if (event.getCode() == KeyCode.ESCAPE) {
			
			weighBridgeTareWeightEvent.setTareweight(null);
			eventBus.post(weighBridgeTareWeightEvent);
			Stage stage = (Stage) btnCancel.getScene().getWindow();
			stage.close();
		}
		if(event.getCode() == KeyCode.BACK_SPACE && txtVehicleNo.getText().length()==0)
		{
			weighBridgeTareWeightEvent.setTareweight(null);
			eventBus.post(weighBridgeTareWeightEvent);
			Stage stage = (Stage) btnCancel.getScene().getWindow();
			stage.close();
		}
    
    }
    private void LoadItemPopupBySearch(String searchData)
    {
    	
    	
//    	List<WeighBridgeTareWeight> weighBridgeTareWeightList = weighBridgeTareWeightRepository.searchByVehicleNo("%"+searchData+"%");
    	
    	ResponseEntity<List<WeighBridgeTareWeight>> weighBridgeTareWeightListResp = 
    			RestCaller.WeighBridgeTareWeightSearchByVehicleNo(searchData);
    	
    	List<WeighBridgeTareWeight> weighBridgeTareWeightList = weighBridgeTareWeightListResp.getBody();
    	
    	popUpItemList = FXCollections.observableArrayList(weighBridgeTareWeightList);
    	tbTareWeight.setItems(popUpItemList);
    }
    
    private void alphaNumericFormatter(TextField txtField) {
    	Pattern pattern = Pattern.compile("[a-zA-Z]*");
    	UnaryOperator<TextFormatter.Change> filter = c -> {
    	    if (pattern.matcher(c.getControlNewText()).matches()) {
    	        return c ;
    	    } else {
    	        return null ;
    	    }
    	};
    	TextFormatter<String> formatter = new TextFormatter<>(filter);
    	txtField.setTextFormatter(formatter);
    }
   
  
}
