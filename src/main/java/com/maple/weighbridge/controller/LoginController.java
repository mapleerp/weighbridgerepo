
package com.maple.weighbridge.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import com.google.common.eventbus.Subscribe;
import com.maple.mapleclient.restService.RestCaller;
import com.maple.weighbridge.entity.BranchMst;
import com.maple.weighbridge.entity.CompanyMst;
import com.maple.weighbridge.entity.WeighBridgeVehicleMst;
import com.maple.weighbridge.utils.SystemSetting;

import org.controlsfx.control.Notifications;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

@Component
public class LoginController {
	String taskid;
	String processInstanceId;
	private static final Logger logger = LoggerFactory.getLogger(LoginController.class);

	@Value("${mybranch}")
	private String mybranch;

	@Value("${reportpath}")
	private String reportpath;
	
	@Value("${mycompany}")
	private String mycompany;
	

	@Value("${WEIGHBRIDGEPORT}")
	private int WEIGHBRIDGEPORT;

	@Value("${WEIGHBRIDGEPAPERSIZE}")
	private int WEIGHBRIDGEPAPERSIZE;

	@Value("${WEIGHCOMPANYTITLE}")
	private String WEIGHCOMPANYTITLE;

	@Value("${WEIGHCOMPANYTITLEFONTSIZE}")
	private int WEIGHCOMPANYTITLEFONTSIZE;

	@Value("${WEIGHCOMPANYTITLEX}")
	private int WEIGHCOMPANYTITLEX;

	@Value("${WEIGHCOMPANYTITLEY}")
	private int WEIGHCOMPANYTITLEY;

	@Value("${WEIGHCOMPANYADDRESS1}")
	private String WEIGHCOMPANYADDRESS1;

	@Value("${WEIGHCOMPANYADDRESS1FONTSIZE}")
	private int WEIGHCOMPANYADDRESS1FONTSIZE;

	@Value("${WEIGHCOMPANYADDRESS1X}")
	private int WEIGHCOMPANYADDRESS1X;

	@Value("${WEIGHCOMPANYADDRESS1Y}")
	private int WEIGHCOMPANYADDRESS1Y;

	@Value("${WEIGHCOMPANYADDRESS2}")
	private String WEIGHCOMPANYADDRESS2;

	@Value("${WEIGHCOMPANYADDRESS2FONTSIZE}")
	private int WEIGHCOMPANYADDRESS2FONTSIZE;

	@Value("${WEIGHCOMPANYADDRESS2X}")
	private int WEIGHCOMPANYADDRESS2X;

	@Value("${WEIGHCOMPANYADDRESS2Y}")
	private int WEIGHCOMPANYADDRESS2Y;

	@Value("${WEIGHCOMPANYADDRESS3}")
	private String WEIGHCOMPANYADDRESS3;

	@Value("${WEIGHCOMPANYADDRESS3FONTSIZE}")
	private int WEIGHCOMPANYADDRESS3FONTSIZE;

	@Value("${WEIGHCOMPANYADDRESS3X}")
	private int WEIGHCOMPANYADDRESS3X;

	@Value("${WEIGHCOMPANYADDRESS3Y}")
	private int WEIGHCOMPANYADDRESS3Y;

	@Value("${WEIGHCOMPANYVEHICKENOX}")
	private int WEIGHCOMPANYVEHICKENOX;

	@Value("${WEIGHCOMPANYVEHICKENOY}")
	private int WEIGHCOMPANYVEHICKENOY;

	@Value("${WEIGHCOMPANYVEHICKENOFONTSIZE}")
	private int WEIGHCOMPANYVEHICKENOFONTSIZE;

	@Value("${WEIGHCOMPANYRATEX}")
	private int WEIGHCOMPANYRATEX;

	@Value("${WEIGHCOMPANYRATEY}")
	private int WEIGHCOMPANYRATEY;

	@Value("${WEIGHCOMPANYRATEFONTSIZE}")
	private int WEIGHCOMPANYRATEFONTSIZE;

	@Value("${WEIGHCOMPANYDATEX}")
	private int WEIGHCOMPANYDATEX;

	@Value("${WEIGHCOMPANYDATEY}")
	private int WEIGHCOMPANYDATEY;

	@Value("${WEIGHCOMPANYDATEFONTSIZE}")
	private int WEIGHCOMPANYDATEFONTSIZE;

	@Value("${WEIGHCOMPANYFIRSTWEIGHTX}")
	private int WEIGHCOMPANYFIRSTWEIGHTX;

	@Value("${WEIGHCOMPANYFIRSTWEIGHTY}")
	private int WEIGHCOMPANYFIRSTWEIGHTY;

	@Value("${WEIGHCOMPANYFIRSTWEIGHTFONTSIZE}")
	private int WEIGHCOMPANYFIRSTWEIGHTFONTSIZE;

	@Value("${WEIGHCOMPANYSECONDWEIGHTX}")
	private int WEIGHCOMPANYSECONDWEIGHTX;

	@Value("${WEIGHCOMPANYSECONDWEIGHTY}")
	private int WEIGHCOMPANYSECONDWEIGHTY;

	@Value("${WEIGHCOMPANYSECONDWEIGHTFONTSIZE}")
	private int WEIGHCOMPANYSECONDWEIGHTFONTSIZE;

	@Value("${WEIGHCOMPANYNETWEIGHTX}")
	private int WEIGHCOMPANYNETWEIGHTX;

	@Value("${WEIGHCOMPANYNETWEIGHTY}")
	private int WEIGHCOMPANYNETWEIGHTY;

	@Value("${WEIGHCOMPANYNETWEIGHTFONTSIZE}")
	private int WEIGHCOMPANYNETWEIGHTFONTSIZE;



	@Value("${WEIGHCOMPANYVOUCHERNUMBERY}")
	private int WEIGHCOMPANYVOUCHERNUMBERY;

	@Value("${WEIGHCOMPANYVOUCHERNUMBERX}")
	private int WEIGHCOMPANYVOUCHERNUMBERX;

	@Value("${WEIGHCOMPANYVOUCHERNUMBERFONTSIZE}")
	private int WEIGHCOMPANYVOUCHERNUMBERFONTSIZE;




	@Value("${WEIGHBRIDGEBAUDRATE}")
	private int WEIGHBRIDGEBAUDRATE;

	@Value("${WEIGHBRIDGEDATABITS}")
	private int WEIGHBRIDGEDATABITS;

	@Value("${WEIGHBRIDGESTOPBIT}")
	private int WEIGHBRIDGESTOPBIT;

	@Value("${WEIGHBRIDGEPARITY}")
	private int WEIGHBRIDGEPARITY;
	
	
	@Value("${WEIGHCOMPANYFIRSTDATEX}")
	private int WEIGHCOMPANYFIRSTDATEX;
	
	@Value("${WEIGHCOMPANYFIRSTDATEY}")
	private int WEIGHCOMPANYFIRSTDATEY;
	



	String user = "maple";
	String password = "maple";
	String checkuser, checkpassword;

	Boolean emptyDatabase = false;

	@FXML
	private TextField loguser;

	@FXML
	private PasswordField logpass;

	@FXML
	private DatePicker logDate;

	@FXML
	private Button logButton;

	@FXML
	private Button exit;

	@FXML
	private ComboBox<String> cmbBranchCode;

	String serverVersion = null;

	@FXML
	void exitbtn(ActionEvent event) {

		Stage stage = (Stage) exit.getScene().getWindow();
		stage.close();
	}

	@FXML
	void loginWindow(ActionEvent event) throws IOException {

		SystemSetting.myCompany = mycompany;
		SystemSetting.systemBranch = mybranch;
		SystemSetting.reportpath=reportpath;
		
		SystemSetting.systemBranch = mybranch;
		SystemSetting.myCompany = mycompany;

		SystemSetting.WEIGHBRIDGEPORT = WEIGHBRIDGEPORT;

		SystemSetting.WEIGHBRIDGEPAPERSIZE = WEIGHBRIDGEPAPERSIZE;

		SystemSetting.WEIGHCOMPANYTITLE = WEIGHCOMPANYTITLE;

		SystemSetting.WEIGHCOMPANYTITLEFONTSIZE = WEIGHCOMPANYTITLEFONTSIZE;

		SystemSetting.WEIGHCOMPANYTITLEX = WEIGHCOMPANYTITLEX;

		SystemSetting.WEIGHCOMPANYTITLEY = WEIGHCOMPANYTITLEY;

		SystemSetting.WEIGHCOMPANYADDRESS1 = WEIGHCOMPANYADDRESS1;

		SystemSetting.WEIGHCOMPANYADDRESS1FONTSIZE = WEIGHCOMPANYADDRESS1FONTSIZE;

		SystemSetting.WEIGHCOMPANYADDRESS1X = WEIGHCOMPANYADDRESS1X;

		SystemSetting.WEIGHCOMPANYADDRESS1Y = WEIGHCOMPANYADDRESS1Y;

		SystemSetting.WEIGHCOMPANYADDRESS2 = WEIGHCOMPANYADDRESS2;

		SystemSetting.WEIGHCOMPANYADDRESS2FONTSIZE = WEIGHCOMPANYADDRESS2FONTSIZE;

		SystemSetting.WEIGHCOMPANYADDRESS2X = WEIGHCOMPANYADDRESS2X;

		SystemSetting.WEIGHCOMPANYADDRESS2Y = WEIGHCOMPANYADDRESS2Y;

		SystemSetting.WEIGHCOMPANYADDRESS3 = WEIGHCOMPANYADDRESS3;

		SystemSetting.WEIGHCOMPANYADDRESS3FONTSIZE = WEIGHCOMPANYADDRESS3FONTSIZE;

		SystemSetting.WEIGHCOMPANYADDRESS3X = WEIGHCOMPANYADDRESS3X;

		SystemSetting.WEIGHCOMPANYADDRESS3Y = WEIGHCOMPANYADDRESS3Y;

		SystemSetting.WEIGHCOMPANYVEHICKENOX = WEIGHCOMPANYVEHICKENOX;

		SystemSetting.WEIGHCOMPANYVEHICKENOY = WEIGHCOMPANYVEHICKENOY;

		SystemSetting.WEIGHCOMPANYVEHICKENOFONTSIZE = WEIGHCOMPANYVEHICKENOFONTSIZE;

		SystemSetting.WEIGHCOMPANYRATEX = WEIGHCOMPANYRATEX;

		SystemSetting.WEIGHCOMPANYRATEY = WEIGHCOMPANYRATEY;

		SystemSetting.WEIGHCOMPANYRATEFONTSIZE = WEIGHCOMPANYRATEFONTSIZE;

		SystemSetting.WEIGHCOMPANYDATEX = WEIGHCOMPANYDATEX;

		SystemSetting.WEIGHCOMPANYDATEY = WEIGHCOMPANYDATEY;

		SystemSetting.WEIGHCOMPANYDATEFONTSIZE = WEIGHCOMPANYDATEFONTSIZE;

		SystemSetting.WEIGHCOMPANYFIRSTWEIGHTX = WEIGHCOMPANYFIRSTWEIGHTX;

		SystemSetting.WEIGHCOMPANYFIRSTWEIGHTY = WEIGHCOMPANYFIRSTWEIGHTY;

		SystemSetting.WEIGHCOMPANYFIRSTWEIGHTFONTSIZE = WEIGHCOMPANYFIRSTWEIGHTFONTSIZE;

		SystemSetting.WEIGHCOMPANYSECONDWEIGHTX = WEIGHCOMPANYSECONDWEIGHTX;

		SystemSetting.WEIGHCOMPANYSECONDWEIGHTY = WEIGHCOMPANYSECONDWEIGHTY;

		SystemSetting.WEIGHCOMPANYSECONDWEIGHTFONTSIZE = WEIGHCOMPANYSECONDWEIGHTFONTSIZE;

		SystemSetting.WEIGHCOMPANYNETWEIGHTX = WEIGHCOMPANYNETWEIGHTX;

		SystemSetting.WEIGHCOMPANYNETWEIGHTY = WEIGHCOMPANYNETWEIGHTY;

		SystemSetting.WEIGHCOMPANYNETWEIGHTFONTSIZE = WEIGHCOMPANYNETWEIGHTFONTSIZE;

		SystemSetting.WEIGHCOMPANYVOUCHERNUMBERY = WEIGHCOMPANYVOUCHERNUMBERY;
		SystemSetting.WEIGHCOMPANYVOUCHERNUMBERX = WEIGHCOMPANYVOUCHERNUMBERX;
		SystemSetting.WEIGHCOMPANYVOUCHERNUMBERFONTSIZE = WEIGHCOMPANYVOUCHERNUMBERFONTSIZE;


		SystemSetting.WEIGHBRIDGEBAUDRATE = WEIGHBRIDGEBAUDRATE;

		SystemSetting.WEIGHBRIDGEDATABITS = WEIGHBRIDGEDATABITS;

		SystemSetting.WEIGHBRIDGESTOPBIT = WEIGHBRIDGESTOPBIT;

		SystemSetting.WEIGHBRIDGEPARITY = WEIGHBRIDGEPARITY;
		
		SystemSetting.WEIGHCOMPANYFIRSTDATEX = WEIGHCOMPANYFIRSTDATEX;

		SystemSetting.WEIGHCOMPANYFIRSTDATEY = WEIGHCOMPANYFIRSTDATEY;


		


		companyCreation();
		branchCreation();

		saveWeighBridgeVehicleMst();

		Stage primaryStage = (Stage) logButton.getScene().getWindow();
		primaryStage.hide();

		ShowMainframe();

	}

	private void saveWeighBridgeVehicleMst() {

		try {

			ResponseEntity<WeighBridgeVehicleMst> weighBridgeVehicleMstList = RestCaller
					.getWeighBridgeVehicleMstByVehicleType("8");
			WeighBridgeVehicleMst weighBridgeVehicleMst = weighBridgeVehicleMstList.getBody();

			ResponseEntity<CompanyMst> companyMstResp = RestCaller.getCompanyMst(SystemSetting.myCompany);
			CompanyMst companyMst = companyMstResp.getBody();

			BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);

			if (null == weighBridgeVehicleMst) {

				weighBridgeVehicleMst = new WeighBridgeVehicleMst();

				weighBridgeVehicleMst.setBranchMst(branchMst);
				weighBridgeVehicleMst.setCompanyMst(companyMst);
				weighBridgeVehicleMst.setId("1");
				weighBridgeVehicleMst.setEmptyRate(50);
				weighBridgeVehicleMst.setLoadRate(100);
				weighBridgeVehicleMst.setVehicletype("8");

				ResponseEntity<WeighBridgeVehicleMst> weighBridgeVehicleMstResp = RestCaller
						.SaveWeighBridgeVehicleMst(weighBridgeVehicleMst);
			}

			weighBridgeVehicleMstList = RestCaller.getWeighBridgeVehicleMstByVehicleType("4");
			weighBridgeVehicleMst = weighBridgeVehicleMstList.getBody();
			if (null == weighBridgeVehicleMst) {
				weighBridgeVehicleMst = new WeighBridgeVehicleMst();

				weighBridgeVehicleMst.setBranchMst(branchMst);
				weighBridgeVehicleMst.setCompanyMst(companyMst);
				weighBridgeVehicleMst.setId("2");
				weighBridgeVehicleMst.setEmptyRate(50);
				weighBridgeVehicleMst.setLoadRate(60);				
				weighBridgeVehicleMst.setVehicletype("4");

				ResponseEntity<WeighBridgeVehicleMst> weighBridgeVehicleMstResp1 = RestCaller
						.SaveWeighBridgeVehicleMst(weighBridgeVehicleMst);

			}
			weighBridgeVehicleMstList = RestCaller.getWeighBridgeVehicleMstByVehicleType("14");
			weighBridgeVehicleMst = weighBridgeVehicleMstList.getBody();
			if (null == weighBridgeVehicleMst) {
				weighBridgeVehicleMst = new WeighBridgeVehicleMst();

				weighBridgeVehicleMst.setBranchMst(branchMst);
				weighBridgeVehicleMst.setCompanyMst(companyMst);
				weighBridgeVehicleMst.setId("3");
				weighBridgeVehicleMst.setEmptyRate(50);
				weighBridgeVehicleMst.setLoadRate(170);
				weighBridgeVehicleMst.setVehicletype("14");

				ResponseEntity<WeighBridgeVehicleMst> weighBridgeVehicleMstResp2 = RestCaller
						.SaveWeighBridgeVehicleMst(weighBridgeVehicleMst);

			}
			
			weighBridgeVehicleMstList = RestCaller.getWeighBridgeVehicleMstByVehicleType("6");
			weighBridgeVehicleMst = weighBridgeVehicleMstList.getBody();
			if (null == weighBridgeVehicleMst) {
				weighBridgeVehicleMst = new WeighBridgeVehicleMst();

				weighBridgeVehicleMst.setBranchMst(branchMst);
				weighBridgeVehicleMst.setCompanyMst(companyMst);
				weighBridgeVehicleMst.setId("4");
				weighBridgeVehicleMst.setEmptyRate(50);
				weighBridgeVehicleMst.setLoadRate(80);
				weighBridgeVehicleMst.setVehicletype("6");

				ResponseEntity<WeighBridgeVehicleMst> weighBridgeVehicleMstResp2 = RestCaller
						.SaveWeighBridgeVehicleMst(weighBridgeVehicleMst);

			}
			
			weighBridgeVehicleMstList = RestCaller.getWeighBridgeVehicleMstByVehicleType("10");
			weighBridgeVehicleMst = weighBridgeVehicleMstList.getBody();
			if (null == weighBridgeVehicleMst) {
				weighBridgeVehicleMst = new WeighBridgeVehicleMst();

				weighBridgeVehicleMst.setBranchMst(branchMst);
				weighBridgeVehicleMst.setCompanyMst(companyMst);
				weighBridgeVehicleMst.setId("5");
				weighBridgeVehicleMst.setEmptyRate(50);
				weighBridgeVehicleMst.setLoadRate(150);
				weighBridgeVehicleMst.setVehicletype("10");

				ResponseEntity<WeighBridgeVehicleMst> weighBridgeVehicleMstResp2 = RestCaller
						.SaveWeighBridgeVehicleMst(weighBridgeVehicleMst);

			}
			
			
			weighBridgeVehicleMstList = RestCaller.getWeighBridgeVehicleMstByVehicleType("12");
			weighBridgeVehicleMst = weighBridgeVehicleMstList.getBody();
			if (null == weighBridgeVehicleMst) {
				weighBridgeVehicleMst = new WeighBridgeVehicleMst();

				weighBridgeVehicleMst.setBranchMst(branchMst);
				weighBridgeVehicleMst.setCompanyMst(companyMst);
				weighBridgeVehicleMst.setId("6");
				weighBridgeVehicleMst.setEmptyRate(50);
				weighBridgeVehicleMst.setLoadRate(170);
				weighBridgeVehicleMst.setVehicletype("12");

				ResponseEntity<WeighBridgeVehicleMst> weighBridgeVehicleMstResp2 = RestCaller
						.SaveWeighBridgeVehicleMst(weighBridgeVehicleMst);

			}
			
			weighBridgeVehicleMstList = RestCaller.getWeighBridgeVehicleMstByVehicleType("16");
			weighBridgeVehicleMst = weighBridgeVehicleMstList.getBody();
			if (null == weighBridgeVehicleMst) {
				weighBridgeVehicleMst = new WeighBridgeVehicleMst();

				weighBridgeVehicleMst.setBranchMst(branchMst);
				weighBridgeVehicleMst.setCompanyMst(companyMst);
				weighBridgeVehicleMst.setId("7");
				weighBridgeVehicleMst.setEmptyRate(50);
				weighBridgeVehicleMst.setLoadRate(170);
				weighBridgeVehicleMst.setVehicletype("16");

				ResponseEntity<WeighBridgeVehicleMst> weighBridgeVehicleMstResp2 = RestCaller
						.SaveWeighBridgeVehicleMst(weighBridgeVehicleMst);

			}
			
			weighBridgeVehicleMstList = RestCaller.getWeighBridgeVehicleMstByVehicleType("TRAILER");
			weighBridgeVehicleMst = weighBridgeVehicleMstList.getBody();
			if (null == weighBridgeVehicleMst) {
				weighBridgeVehicleMst = new WeighBridgeVehicleMst();

				weighBridgeVehicleMst.setBranchMst(branchMst);
				weighBridgeVehicleMst.setCompanyMst(companyMst);
				weighBridgeVehicleMst.setId("7");
				weighBridgeVehicleMst.setEmptyRate(100);
				weighBridgeVehicleMst.setLoadRate(150);
				weighBridgeVehicleMst.setVehicletype("TRAILER");

				ResponseEntity<WeighBridgeVehicleMst> weighBridgeVehicleMstResp2 = RestCaller
						.SaveWeighBridgeVehicleMst(weighBridgeVehicleMst);

			}
			
			
			weighBridgeVehicleMstList = RestCaller.getWeighBridgeVehicleMstByVehicleType("NEW REGISTRATION");
			weighBridgeVehicleMst = weighBridgeVehicleMstList.getBody();
			if (null == weighBridgeVehicleMst) {
				weighBridgeVehicleMst = new WeighBridgeVehicleMst();

				weighBridgeVehicleMst.setBranchMst(branchMst);
				weighBridgeVehicleMst.setCompanyMst(companyMst);
				weighBridgeVehicleMst.setId("7");
				weighBridgeVehicleMst.setEmptyRate(250);
				weighBridgeVehicleMst.setLoadRate(250);
				weighBridgeVehicleMst.setVehicletype("NEW REGISTRATION");

				ResponseEntity<WeighBridgeVehicleMst> weighBridgeVehicleMstResp2 = RestCaller
						.SaveWeighBridgeVehicleMst(weighBridgeVehicleMst);

			}
		

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	private void branchCreation() {
		ResponseEntity<CompanyMst> companyMstResp = RestCaller.getCompanyMst(SystemSetting.myCompany);
		CompanyMst companyMst = companyMstResp.getBody();

		BranchMst branchMst = RestCaller.getBranchDtls(SystemSetting.systemBranch);
		if (null == branchMst) {
			branchMst = new BranchMst();
			branchMst.setCompanyMst(companyMst);
			branchMst.setBranchName("WB");
			branchMst.setBranchCode("WB");
			branchMst.setMyBranch("WB");
			branchMst.setId("WB");

			RestCaller.SavebranchCreation(branchMst);
		}

	}

	private void companyCreation() {

		ResponseEntity<CompanyMst> companyMstResp = RestCaller.getCompanyMst(SystemSetting.myCompany);
		CompanyMst companyMst = companyMstResp.getBody();

		if (null == companyMst) {
			companyMst = new CompanyMst();
			companyMst.setCompanyName("WB");
			companyMst.setState("KERALA");
			companyMst.setId("WB");

			RestCaller.saveCompanyMst(companyMst);
		}

	}

	private void ShowMainframe() {

		try {

			Parent root;
			root = FXMLLoader.load(getClass().getResource("/fxml/MainFrame.fxml"));
			Scene scene = new Scene(root);
			Stage stage = new Stage();
			stage.setScene(scene);

			stage.show();
			return;

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@FXML
	private void initialize() {

	}

	@FXML
	void userNameOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			logpass.requestFocus();
		}

	}

	@FXML
	void passwordOnEnter(KeyEvent event) {
		if (event.getCode() == KeyCode.ENTER) {
			logButton.requestFocus();
		}
	}

	public void notifyMessage(int duration, String msg) {

		Image img = new Image("done.png");
		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();
	}

	public void notifyMessage(int duration, String msg, boolean success) {

		Image img;
		if (success) {
			img = new Image("done.png");

		} else {
			img = new Image("failed.png");
		}

		Notifications notificationBuilder = Notifications.create().text(msg).graphic(new ImageView(img))
				.hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
				.onAction(new EventHandler<ActionEvent>() {
					@Override
					public void handle(ActionEvent event) {
						System.out.println("clicked on notification");
					}
				});
		notificationBuilder.darkStyle();
		notificationBuilder.show();

	}

	private void PageReload() {

	}
}
