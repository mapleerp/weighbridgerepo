package com.maple.weighbridge.controller;

import java.awt.event.FocusEvent;
import java.io.IOException;
import java.net.URL;
import java.net.UnknownHostException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Properties;
import java.util.ResourceBundle;

import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Component;

import com.google.common.eventbus.EventBus;
import com.google.common.eventbus.Subscribe;
import com.maple.weighbridge.EventBusFactory;
 
 
import com.maple.weighbridge.WeighbridgeApplication;
import com.maple.weighbridge.event.WeighBridgeEventOne;
import com.maple.weighbridge.service.WeighBridgeEventInitiator;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Screen;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;

@Component
public class WBMainFrameController implements Initializable {
	 
	 
	WeighBridgeEventInitiator weighBridgeEventInitiator = WeighBridgeEventInitiator.getInstance();
	
	String taskid;
	String processInstanceId;

	public HashMap menuWindowQueue = new HashMap();

	EventBus eventBus = EventBusFactory.getEventBus();

	// version4.4

	Node userreg;
	Node reportSqlCommand;
	Node AccountHeads;
	Node tyreSaving;


	// --------------------versio 6.11 surya end



	@FXML
	Button HomeBtn;

	@FXML
	private Pane functionSideBar;

	@FXML
	VBox sideVBOX;

	@FXML
	private AnchorPane mainWorkArea; // Main Pane where the sub window is loaded

	@FXML
	private void handleHomeAction(ActionEvent event) {

		try {
			System.out.println("inside the popup");
			FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxml/tasklist-1000x600.fxml"));
			Parent root = loader.load();

			Stage stage = new Stage();
			stage.setScene(new Scene(root));
			stage.initModality(Modality.APPLICATION_MODAL);
			stage.show();

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@FXML
	void anchorarea1(MouseEvent event) {

	}

	@FXML
	void anchorarea2(MouseEvent event) {

	}

	@Override
	public void initialize(URL url, ResourceBundle rb) {

		eventBus.register(this);
		WeighbridgeApplication.mainWorkArea = mainWorkArea;

		WeighbridgeApplication.mainFrameController = this;

		sideVBOX.getChildren().clear();

		final Button actionBtnPos = new Button("HOME");
		actionBtnPos.setMinWidth(HomeBtn.getMinWidth());
		actionBtnPos.setMaxWidth(HomeBtn.getMaxWidth());
		actionBtnPos.setPrefWidth(HomeBtn.getPrefWidth());
		actionBtnPos.setMinHeight(HomeBtn.getMinHeight());
		actionBtnPos.setMaxHeight(HomeBtn.getMaxHeight());
		actionBtnPos.setPrefHeight(HomeBtn.getPrefHeight());

		actionBtnPos.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {


			 
				WeighBridgeEventInitiator weighBridgeEvent = WeighBridgeEventInitiator.getInstance();
				
				weighBridgeEvent.EventInitiator("000","EMPTY",null);
			
				
				Node dynamicWindow = (Node) menuWindowQueue.get("WEIGHTS");
				
				
				try {
//					dynamicWindow = FXMLLoader.load(getClass().getResource("/fxml/WeighingMachineDisplayNew.fxml"));
					dynamicWindow = FXMLLoader.load(getClass().getResource("/fxml/WeighingMachineDisplaySystem.fxml"));
					

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

				try {

					// Clear screen before loading the Page.
					if (!WeighbridgeApplication.mainWorkArea.getChildren().isEmpty())
						WeighbridgeApplication.mainWorkArea.getChildren().clear();

					WeighbridgeApplication.mainWorkArea.getChildren().add(dynamicWindow);

					WeighbridgeApplication.mainWorkArea.setTopAnchor(dynamicWindow, 0.0);
					WeighbridgeApplication.mainWorkArea.setRightAnchor(dynamicWindow, 0.0);
					WeighbridgeApplication.mainWorkArea.setLeftAnchor(dynamicWindow, 0.0);
					WeighbridgeApplication.mainWorkArea.setBottomAnchor(dynamicWindow, 0.0);
					dynamicWindow.requestFocus();

				} catch (Exception e) {
					e.printStackTrace();
				}

			}
		});

		sideVBOX.getChildren().add(actionBtnPos);

		try {

			AccountHeads = FXMLLoader.load(getClass().getResource("/fxml/AccountHead.fxml"));

		}

		catch (Exception e) {
			System.out.println(e.toString());
		}

		try {

			userreg = FXMLLoader.load(getClass().getResource("/fxml/userregistration.fxml"));

		} catch (Exception e) {
			System.out.println(e.toString());
		}

		final Button actionBtnserviceTree = new Button("MASTER REPORTS");
		actionBtnserviceTree.setMinWidth(HomeBtn.getMinWidth());
		actionBtnserviceTree.setMaxWidth(HomeBtn.getMaxWidth());
		actionBtnserviceTree.setPrefWidth(HomeBtn.getPrefWidth());
		actionBtnserviceTree.setMinHeight(HomeBtn.getMinHeight());
		actionBtnserviceTree.setMaxHeight(HomeBtn.getMaxHeight());
		actionBtnserviceTree.setPrefHeight(HomeBtn.getPrefHeight());
		actionBtnserviceTree.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				
				try {
					reportSqlCommand = FXMLLoader.load(getClass().getResource("/fxml/weighBridgeDailyReport.fxml"));
					

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {

					if (!mainWorkArea.getChildren().isEmpty())
						mainWorkArea.getChildren().clear();

					mainWorkArea.getChildren().add(reportSqlCommand);

					mainWorkArea.setTopAnchor(reportSqlCommand, 0.0);
					mainWorkArea.setRightAnchor(reportSqlCommand, 0.0);
					mainWorkArea.setLeftAnchor(reportSqlCommand, 0.0);
					mainWorkArea.setBottomAnchor(reportSqlCommand, 0.0);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		sideVBOX.getChildren().add(actionBtnserviceTree);
		
		
		
		final Button actionBtntyreWeightsave = new Button("TYRE SAVING");
		actionBtntyreWeightsave.setMinWidth(HomeBtn.getMinWidth());
		actionBtntyreWeightsave.setMaxWidth(HomeBtn.getMaxWidth());
		actionBtntyreWeightsave.setPrefWidth(HomeBtn.getPrefWidth());
		actionBtntyreWeightsave.setMinHeight(HomeBtn.getMinHeight());
		actionBtntyreWeightsave.setMaxHeight(HomeBtn.getMaxHeight());
		actionBtntyreWeightsave.setPrefHeight(HomeBtn.getPrefHeight());
		actionBtntyreWeightsave.setOnAction(new EventHandler<ActionEvent>() {
			
			
			@Override
			public void handle(ActionEvent event) {
				
				try {
					tyreSaving = FXMLLoader.load(getClass().getResource("/fxml/TyreWeightSavingWindow.fxml"));
					

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {

					if (!mainWorkArea.getChildren().isEmpty())
						mainWorkArea.getChildren().clear();

					mainWorkArea.getChildren().add(tyreSaving);

					mainWorkArea.setTopAnchor(tyreSaving, 0.0);
					mainWorkArea.setRightAnchor(tyreSaving, 0.0);
					mainWorkArea.setLeftAnchor(tyreSaving, 0.0);
					mainWorkArea.setBottomAnchor(tyreSaving, 0.0);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		sideVBOX.getChildren().add(actionBtntyreWeightsave);
		}
	}


