//package com.maple.weighbridge.controller;
//
//import java.sql.SQLException;
//import java.text.SimpleDateFormat;
//import java.util.Date;
//import java.util.List;
//
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.ResponseEntity;
//
//import com.google.common.eventbus.EventBus;
//
//import javafx.beans.property.SimpleStringProperty;
//import javafx.beans.property.StringProperty;
//import javafx.beans.value.ChangeListener;
//import javafx.beans.value.ObservableValue;
//import javafx.collections.FXCollections;
//import javafx.collections.ObservableList;
//import javafx.event.ActionEvent;
//import javafx.fxml.FXML;
//import javafx.scene.control.Button;
//import javafx.scene.control.TableColumn;
//import javafx.scene.control.TableView;
//import javafx.scene.control.TextField;
//import javafx.stage.Stage;
//import com.google.common.eventbus.Subscribe;
//import com.maple.mapleclient.restService.RestCaller;
//import com.maple.weighbridge.EventBusFactory;
//import com.maple.weighbridge.WeighingMachinePrint;
//import com.maple.weighbridge.entity.WeighBridgeWeights;
//
//public class WeighingMachineReprintPopup {
//
//	String taskid;
//	String processInstanceId;
//
//	boolean initializedCalled = false;
//	private EventBus eventBus = EventBusFactory.getEventBus();
//	StringProperty SearchString = new SimpleStringProperty();
//	WeighBridgeWeights weighBridgeWeights = null;
//	private ObservableList<WeighBridgeWeights> popUpItemList1 = FXCollections.observableArrayList();
//
//	@FXML
//	private TableView<WeighBridgeWeights> tbVehicleWeighs;
//
//	@FXML
//	private TableColumn<WeighBridgeWeights, String> clDate;
//
//	@FXML
//	private TableColumn<WeighBridgeWeights, String> clVehicleNo;
//
//	@FXML
//	private TableColumn<WeighBridgeWeights, String> clWeight;
//
//	@FXML
//	private TextField txtVehicleNo;
//
//	@FXML
//	private Button btnPrint;
//
//	@FXML
//	private Button btnCance;
//
//	@FXML
//	void actionCancel(ActionEvent event) {
//		Stage stage = (Stage) btnCance.getScene().getWindow();
//		stage.close();
//	}
//
//	@FXML
//	private void initialize() {
//		if (initializedCalled)
//			return;
//
//		initializedCalled = true;
//		System.out.println("initializedCalledinitializedCalled");
//		eventBus.register(this);
//		txtVehicleNo.textProperty().bindBidirectional(SearchString);
//		LoadItemPopupBySearch("");
//		tbVehicleWeighs.setItems(popUpItemList1);
//		clDate.setCellValueFactory(cellData -> cellData.getValue().getvoucherDateProperty());
//		clVehicleNo.setCellValueFactory(cellData -> cellData.getValue().getvehiclenoProperty());
//		clWeight.setCellValueFactory(cellData -> cellData.getValue().getnetweightProperty());
//		tbVehicleWeighs.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
//			if (newSelection != null) {
//				System.out.println("getSelectionModel");
//				if (null != newSelection.getId()) {
//
//					weighBridgeWeights = new WeighBridgeWeights();
//					weighBridgeWeights.setId(newSelection.getId());
//				}
//			}
//		});
//
//		SearchString.addListener(new ChangeListener() {
//
//			@Override
//			public void changed(ObservableValue observable, Object oldValue, Object newValue) {
//
//				LoadItemPopupBySearch((String) newValue);
//			}
//		});
//	}
//
//	@FXML
//	void actionPrint(ActionEvent event) {
//		if (null == weighBridgeWeights) {
//			return;
//		}
//		if (null == weighBridgeWeights.getId()) {
//			return;
//		}
//		WeighingMachinePrint wprint = new WeighingMachinePrint();
//
//		ResponseEntity<WeighBridgeWeights> getWeightResp = RestCaller.getWeighBridgeWeightById(weighBridgeWeights.getId());
//
//		WeighBridgeWeights getWeight = getWeightResp.getBody();
//		if (null != getWeight)
//			try {
//				wprint.PrintInvoiceThermalPrinter(getWeight.getNetweight() + "", getWeight.getPreviousweight() + "",
//						getWeight.getNetweight() + "", getWeight.getVehicleno(), Integer.toString(getWeight.getRate()),
//						getCurrentTimeStamp(), getWeight.getFirstVoucherNumber());
//			} catch (SQLException e) {
//				// TODO Auto-generated catch block
//				e.printStackTrace();
//			}
//
//		Stage stage = (Stage) btnCance.getScene().getWindow();
//		stage.close();
//	}
//
//	private void LoadItemPopupBySearch(String searchData) {
//		ResponseEntity<List<WeighBridgeWeights>> getAllWeight = RestCaller.searchWeightByVehicleNo(searchData);
//
//		popUpItemList1 = FXCollections.observableArrayList(getAllWeight.getBody());
//		tbVehicleWeighs.setItems(popUpItemList1);
//		clDate.setCellValueFactory(cellData -> cellData.getValue().getvoucherDateProperty());
//		clVehicleNo.setCellValueFactory(cellData -> cellData.getValue().getvehiclenoProperty());
//		clWeight.setCellValueFactory(cellData -> cellData.getValue().getnetweightProperty());
//
//	}
//
//	public static String getCurrentTimeStamp() {
//		SimpleDateFormat sdfDate = new SimpleDateFormat("dd-MM-yyyy HH:mm");// dd/MM/yyyy
//		Date now = new Date();
//		String strDate = sdfDate.format(now);
//		return strDate;
//	}
//
//	private void PageReload(String hdrId) {
//
//	}
//}
