package com.maple.weighbridge.controller;
import java.awt.event.KeyEvent;
import java.util.List;

import org.controlsfx.control.Notifications;
import org.springframework.http.ResponseEntity;

import com.maple.mapleclient.restService.RestCaller;
import com.maple.weighbridge.entity.BranchMst;
import com.maple.weighbridge.entity.CompanyMst;
import com.maple.weighbridge.entity.TyreWeightSavingMst;
import com.maple.weighbridge.entity.WeighBridgeVehicleMst;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.ComboBox;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.MouseEvent;
import javafx.util.Duration;


public class TyreWeightSavingWindowCtl {
	
	
	private ObservableList<TyreWeightSavingMst> tyreWeightSavingMstList = FXCollections.observableArrayList();
	private ObservableList<WeighBridgeVehicleMst> weighBridgeVehicleMstList = FXCollections.observableArrayList(); 
	
	
	TyreWeightSavingMst tyreWeightSavingMst;

	     @FXML
	    private TextField txtVehicleNumber;

	    @FXML
	    private ComboBox<String> cmbVehicleType;

	    @FXML
	    private TextField txtWeight;

	    @FXML
	    private Button btnAddItem;

	    @FXML
	    private Button btnShow;

	    @FXML
	    private Button btnDeleteItem;

	    @FXML
	    private Button btnClear;


	@FXML
	private TableView<TyreWeightSavingMst> tblvehicleDtls;

	@FXML
	private TableColumn<TyreWeightSavingMst, String> clvehicleNumber;

	@FXML
	private TableColumn<TyreWeightSavingMst, String> clvehicleType;

	@FXML
	private TableColumn<TyreWeightSavingMst, String> clweight;

	@FXML
	private void initialize() {
		
		ResponseEntity<List<WeighBridgeVehicleMst>> vehicleListResp = RestCaller.getAllWeighBridgeVehicleMst();
		weighBridgeVehicleMstList=FXCollections.observableArrayList(vehicleListResp.getBody());
		for (WeighBridgeVehicleMst vehicle : weighBridgeVehicleMstList) {
			cmbVehicleType.getItems().add(vehicle.getVehicletype());
		}

		
		
		tblvehicleDtls.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
			if (newSelection != null) {
				if (null != newSelection.getId()) {

					tyreWeightSavingMst = new TyreWeightSavingMst();
					tyreWeightSavingMst.setId(newSelection.getId());
				}
			}
		});

	}

	@FXML
    void AddItem(ActionEvent event) {
		save();
    }

	
	private void save() {
		TyreWeightSavingMst tyreWeightSavingMsts = new TyreWeightSavingMst();
		if (null == txtVehicleNumber.getText()) {
//			notifyMessage(5, "Please enter Vehicle Number", false);
			return;
		}
		if (null == cmbVehicleType.getValue())

		{

//			notifyMessage(5, "Please select Vehicle tyre", false);
			cmbVehicleType.requestFocus();
			return;
		}
		if (null == txtWeight.getText()) {
//			notifyMessage(5, "Please enter Vehicle Weight", false);
			return;
		}

		tyreWeightSavingMsts.setVehicleNumber(txtVehicleNumber.getText());

		tyreWeightSavingMsts.setVehicleType(cmbVehicleType.getSelectionModel().getSelectedItem());

		tyreWeightSavingMsts.setVehicleWeight(txtWeight.getText());
		
		BranchMst branchMst = RestCaller.getBranchDtls("WB");

		ResponseEntity<CompanyMst> companyMstResp = RestCaller.getCompanyMst("WB");
		
		tyreWeightSavingMsts.setCompanyMst(companyMstResp.getBody());
		tyreWeightSavingMsts.setBranchMst(branchMst);

		ResponseEntity<TyreWeightSavingMst> respEntity = RestCaller.saveVehicleTyreWeight(tyreWeightSavingMsts);

		{
//			notifyMessage(5, "Saved ", true);
			txtVehicleNumber.clear();
			cmbVehicleType.setValue(null);
			txtWeight.clear();

			showAll();
			return;
		}

	}

    @FXML
    void DeleteItem(ActionEvent event) {
    	if (null != tyreWeightSavingMst) {

			if (null != tyreWeightSavingMst.getId()) {
				RestCaller.deleteTyreWeightSaving(tyreWeightSavingMst.getId());

				showAll();
//				notifyMessage(5, " deleted .....", false);
				return;
			}
		}
    }


    @FXML
    void ShowAll(ActionEvent event) {
    	showAll();
    }
    private void showAll() {
		ResponseEntity<List<TyreWeightSavingMst>> respentity = RestCaller.getVehicleTyreWeight();
		tyreWeightSavingMstList = FXCollections.observableArrayList(respentity.getBody());

		System.out.print(tyreWeightSavingMstList.size() + "observable list size is");
		fillTable();
	}
    
    private void fillTable() {

		tblvehicleDtls.setItems(tyreWeightSavingMstList);

		clvehicleNumber.setCellValueFactory(cellData -> cellData.getValue().getVehicleNumberProperty());

		clvehicleType.setCellValueFactory(cellData -> cellData.getValue().getVehicleTypeProperty());

		clweight.setCellValueFactory(cellData -> cellData.getValue().getVehicleWeightProperty());

	}
    @FXML
    void addItem(KeyEvent event) {

    }

    @FXML
    void clearItems(ActionEvent event) {
    	txtVehicleNumber.clear();
		cmbVehicleType.getItems().clear();
		txtWeight.clear();
    }


    @FXML
    void OnActionVehicleType(ActionEvent event) {

    }
	

	

	/*
	 * public void notifyMessage(int duration, String msg, boolean success) {
	 * 
	 * Image img; if (success) { img = new Image("done.png");
	 * 
	 * } else { img = new Image("failed.png"); }
	 * 
	 * Notifications notificationBuilder =
	 * Notifications.create().text(msg).graphic(new ImageView(img))
	 * .hideAfter(Duration.seconds(duration)).position(Pos.BOTTOM_RIGHT)
	 * .onAction(new EventHandler<ActionEvent>() {
	 * 
	 * @Override public void handle(ActionEvent event) {
	 * System.out.println("clicked on notification"); } });
	 * notificationBuilder.darkStyle(); notificationBuilder.show();
	 * 
	 * }
	 */

}
