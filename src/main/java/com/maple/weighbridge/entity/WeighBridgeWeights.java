package com.maple.weighbridge.entity;

import java.io.Serializable;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.hibernate.annotations.UpdateTimestamp;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

@Entity
public class WeighBridgeWeights implements Serializable {
	private static final long serialVersionUID = 1L;

	@Column(length = 50)
	@Id
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;

	@JsonProperty("machineweight")
	Integer machineweight;
	
	@Column(length = 50)
	@JsonProperty("vehicleno")
	private String vehicleno;

	public WeighBridgeWeights() {

		this.firstwtDateProperty = new SimpleStringProperty("");
		this.rateProperty = new SimpleIntegerProperty();
		this.vehiclenoProperty = new SimpleStringProperty("");
		this.netweightProperty = new SimpleStringProperty("");
		this.nextwtproperty = new SimpleIntegerProperty();
		this.previousWtProperty = new SimpleIntegerProperty();
		this.voucherDateProperty = new SimpleStringProperty("");
		this.machineWeightProperty = new SimpleIntegerProperty();
		this.materialtypeidProperty= new SimpleStringProperty("");
		this.firstPaidAmountProperty =  new SimpleIntegerProperty();
	}

	@Transient
	private StringProperty firstwtDateProperty;
	
	@Transient
	private StringProperty materialtypeidProperty;

	@Transient
	private SimpleIntegerProperty rateProperty;

	
	@Transient
	private SimpleIntegerProperty firstPaidAmountProperty;

	
	@Transient
	private SimpleStringProperty vehiclenoProperty;

	@Transient
	private SimpleStringProperty netweightProperty;

	@Transient
	private SimpleIntegerProperty nextwtproperty;

	@Transient
	private SimpleIntegerProperty previousWtProperty;

	@Transient
	private SimpleStringProperty voucherDateProperty;

	@Transient
	private SimpleIntegerProperty machineWeightProperty;

	@JsonProperty("previousweight")
	private Integer previousweight;

	@JsonProperty("previousweightid")
	private String previousweightid;

	@JsonProperty("nextweight")
	private Integer nextweight;
	
	@Column(length = 50)
	@JsonProperty("nextweightid")
	private String nextweightid;

	@Column(length = 50)
	@JsonProperty("netweight")
	private String netweight;
	
	@Column(length = 50)
	@JsonProperty("vehicletypeid")
	private String vehicletypeid;
	
	@Column(length = 50)
	@JsonProperty("materialtypeid")
	private String materialtypeid;

	@Column(length = 50)
	@JsonProperty("firstweightdate")
	private String firstweightdate;

	@Column(length = 50)
	@JsonProperty("secondWeightdate")
	private String secondWeightdate;

	@JsonProperty("rate")
	private Integer rate;

	@JsonProperty("secondRate")
	private Integer secondRate;

	@JsonProperty("voucherDate")
	private LocalDateTime voucherDate;

	
	@JsonProperty("sndVoucherDate")
	private LocalDateTime sndVoucherDate;
	
	@UpdateTimestamp
	LocalDateTime updatedTime;

	@Column(length = 50)
	@JsonProperty("firstVoucherNumber")
	private String firstVoucherNumber;

	@Column(length = 50)
	@JsonProperty("secondVoucherNumber")
	private String secondVoucherNumber;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "branchMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private BranchMst branchMst;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;
	
	@Column(length = 50)
	private String processInstanceId;
	
	@Column(length = 50)
	private String taskId;

	@JsonProperty("firstPaidAmount")
	private Integer firstPaidAmount;

	@JsonProperty("secondPaidAmount")
	private Integer secondPaidAmount;

	@JsonProperty("secondCashToPay")
	private Integer secondCashToPay;

	@JsonProperty("firstCashToPay")
	private Integer firstCashToPay;

	@Column(length = 50)
	private String status;
	
	@Column(length = 50)
	private String ftVehicleloadStatus;
	
	@Column(length = 50)
	private String sndVehicleloadStatus;
	
	
	public LocalDateTime getVoucherDate() {
		return voucherDate;
	}

	public void setVoucherDate(LocalDateTime voucherDate) {
		this.voucherDate = voucherDate;
	}

	public LocalDateTime getUpdatedTime() {
		return updatedTime;
	}

	public String getFirstweightdate() {
		return firstweightdate;
	}

	public void setFirstweightdate(String firstweightdate) {
		this.firstweightdate = firstweightdate;
	}

	public void setUpdatedTime(LocalDateTime updatedTime) {
		this.updatedTime = updatedTime;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Integer getMachineweight() {
		return machineweight;
	}

	public void setMachineweight(Integer machineweight) {
		this.machineweight = machineweight;
	}

	public String getNetweight() {
		return netweight;
	}

	public void setNetweight(String netweight) {
		this.netweight = netweight;
	}

	public String getVehicletypeid() {
		return vehicletypeid;
	}

	public void setVehicletypeid(String vehicletypeid) {
		this.vehicletypeid = vehicletypeid;
	}

	public String getMaterialtypeid() {
		return materialtypeid;
	}

	public void setMaterialtypeid(String materialtypeid) {
		this.materialtypeid = materialtypeid;
	}

	public Integer getRate() {
		return rate;
	}

	public void setRate(Integer rate) {
		this.rate = rate;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	public BranchMst getBranchMst() {
		return branchMst;
	}

	public void setBranchMst(BranchMst branchMst) {
		this.branchMst = branchMst;
	}

	public String getVehicleno() {
		return vehicleno;
	}

	public void setVehicleno(String vehicleno) {
		this.vehicleno = vehicleno;
	}

	public Integer getPreviousweight() {
		return previousweight;
	}

	public void setPreviousweight(Integer previousweight) {
		this.previousweight = previousweight;
	}

	public String getPreviousweightid() {
		return previousweightid;
	}

	public void setPreviousweightid(String previousweightid) {
		this.previousweightid = previousweightid;
	}

	public Integer getNextweight() {
		return nextweight;
	}

	public void setNextweight(Integer nextweight) {
		this.nextweight = nextweight;
	}

	public String getNextweightid() {
		return nextweightid;
	}

	public void setNextweightid(String nextweightid) {
		this.nextweightid = nextweightid;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	public SimpleIntegerProperty getNextwtproperty() {
		if (null != nextweight) {
			nextwtproperty.set(nextweight);
		}
		return nextwtproperty;
	}

	public void setNextwtproperty(Integer nextweight) {
		this.nextweight = nextweight;
	}

	public StringProperty getFirstwtDateProperty() {
		firstwtDateProperty.set(firstweightdate);
		return firstwtDateProperty;
	}

	public void setFirstwtDateProperty(String firstweightdate) {
		this.firstweightdate = firstweightdate;
	}

	public StringProperty getvehiclenoProperty() {
		vehiclenoProperty.set(vehicleno);
		return vehiclenoProperty;
	}

	public void setvehiclenoProperty(String vehicleno) {
		this.vehicleno = vehicleno;
	}

	public StringProperty getnetweightProperty() {
		netweightProperty.set(netweight);
		return netweightProperty;
	}

	public void setMachineWeightProperty(Integer machineweight) {
		this.machineweight = machineweight;
	}

	public StringProperty getvoucherDateProperty() {
		voucherDateProperty.set(voucherDate.toString());
		return voucherDateProperty;
	}

	public void setvoucherDateProperty(LocalDateTime voucherDate) {
		this.voucherDate = voucherDate;
	}

	@JsonIgnore
	public SimpleIntegerProperty getRateProperty() {

		rateProperty.set(rate);
		return rateProperty;
	}

	@JsonIgnore
	public IntegerProperty getpreviousWtPropertyProperty() {
		if (null != previousweight) {
			previousWtProperty.set(previousweight);
		}
		return previousWtProperty;
	}

	public IntegerProperty getMachineWeightProperty() {
		machineWeightProperty.set(machineweight);
		return machineWeightProperty;
	}

	public String getFirstVoucherNumber() {
		return firstVoucherNumber;
	}

	public void setFirstVoucherNumber(String firstVoucherNumber) {
		this.firstVoucherNumber = firstVoucherNumber;
	}

	public String getSecondVoucherNumber() {
		return secondVoucherNumber;
	}

	public void setSecondVoucherNumber(String secondVoucherNumber) {
		this.secondVoucherNumber = secondVoucherNumber;
	}

	public String getSecondWeightdate() {
		return secondWeightdate;
	}

	public void setSecondWeightdate(String secondWeightdate) {
		this.secondWeightdate = secondWeightdate;
	}

	public Integer getSecondRate() {
		return secondRate;
	}

	public void setSecondRate(Integer secondRate) {
		this.secondRate = secondRate;
	}

	public Integer getFirstPaidAmount() {
		return firstPaidAmount;
	}

	public void setFirstPaidAmount(Integer firstPaidAmount) {
		this.firstPaidAmount = firstPaidAmount;
	}

	public Integer getSecondPaidAmount() {
		return secondPaidAmount;
	}

	public void setSecondPaidAmount(Integer secondPaidAmount) {
		this.secondPaidAmount = secondPaidAmount;
	}

	public Integer getSecondCashToPay() {
		return secondCashToPay;
	}

	public void setSecondCashToPay(Integer secondCashToPay) {
		this.secondCashToPay = secondCashToPay;
	}

	public Integer getFirstCashToPay() {
		return firstCashToPay;
	}

	public void setFirstCashToPay(Integer firstCashToPay) {
		this.firstCashToPay = firstCashToPay;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getFtVehicleloadStatus() {
		return ftVehicleloadStatus;
	}

	public void setFtVehicleloadStatus(String ftVehicleloadStatus) {
		this.ftVehicleloadStatus = ftVehicleloadStatus;
	}

	public String getSndVehicleloadStatus() {
		return sndVehicleloadStatus;
	}

	public void setSndVehicleloadStatus(String sndVehicleloadStatus) {
		this.sndVehicleloadStatus = sndVehicleloadStatus;
	}

	public LocalDateTime getSndVoucherDate() {
		return sndVoucherDate;
	}

	public void setSndVoucherDate(LocalDateTime sndVoucherDate) {
		this.sndVoucherDate = sndVoucherDate;
	}

	public StringProperty getMaterialtypeidProperty() {
		if (null != materialtypeid) {
			materialtypeidProperty.set(materialtypeid);
		}
		
		return materialtypeidProperty;
	}

	public void setMaterialtypeidProperty(StringProperty materialtypeidProperty) {
		this.materialtypeidProperty = materialtypeidProperty;
	}

	@JsonIgnore
	public SimpleIntegerProperty getFirstPaidAmountProperty() {
		
		firstPaidAmountProperty.set(firstPaidAmount);
		 
		return firstPaidAmountProperty;
	}

	@JsonIgnore
	public void setFirstPaidAmountProperty(SimpleIntegerProperty firstPaidAmountProperty) {
		this.firstPaidAmountProperty = firstPaidAmountProperty;
	}

	
	
}
