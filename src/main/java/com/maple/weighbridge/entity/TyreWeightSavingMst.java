package com.maple.weighbridge.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

 
public class TyreWeightSavingMst implements Serializable {
	private static final long serialVersionUID = 1L; 
	
	 
	private String id;
	
 
	private CompanyMst companyMst;
	
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	
	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "branchMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private BranchMst branchMst;
	@Column(length = 50)
	private String vehicleNumber;
	@Column(length = 50)
	private String vehicleType;
	@Column(length = 50)
	private String vehicleWeight;
	
	
	public TyreWeightSavingMst() {


		
		this.vehicleNumberProperty = new SimpleStringProperty();
		this.vehicleTypeProperty=new SimpleStringProperty();
		this.vehicleWeightProperty=new SimpleStringProperty();
	}
	
	@JsonIgnore
	StringProperty vehicleNumberProperty;
	@JsonIgnore 
	StringProperty vehicleTypeProperty;
	@JsonIgnore
	StringProperty vehicleWeightProperty;


	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public CompanyMst getCompanyMst() {
		return companyMst;
	}
	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}
	public String getProcessInstanceId() {
		return processInstanceId;
	}
	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}
	public String getTaskId() {
		return taskId;
	}
	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}
	public BranchMst getBranchMst() {
		return branchMst;
	}
	public void setBranchMst(BranchMst branchMst) {
		this.branchMst = branchMst;
	}
	public String getVehicleNumber() {
		return vehicleNumber;
	}
	public void setVehicleNumber(String vehicleNumber) {
		this.vehicleNumber = vehicleNumber;
	}
	public String getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}
	public String getVehicleWeight() {
		return vehicleWeight;
	}
	public void setVehicleWeight(String vehicleWeight) {
		this.vehicleWeight = vehicleWeight;
	}
	public StringProperty getVehicleNumberProperty() {
		vehicleNumberProperty.set(vehicleNumber);
		return vehicleNumberProperty;
	}
	public void setVehicleNumberProperty(StringProperty vehicleNumberProperty) {
		this.vehicleNumberProperty = vehicleNumberProperty;
	}
	public StringProperty getVehicleTypeProperty() {
		vehicleTypeProperty.set(vehicleType);
		return vehicleTypeProperty;
	}
	public void setVehicleTypeProperty(StringProperty vehicleTypeProperty) {
		this.vehicleTypeProperty = vehicleTypeProperty;
	}
	public StringProperty getVehicleWeightProperty() {
		vehicleWeightProperty.set(vehicleWeight);
		return vehicleWeightProperty;
	}
	public void setVehicleWeightProperty(StringProperty vehicleWeightProperty) {
		this.vehicleWeightProperty = vehicleWeightProperty;
	}
	@Override
	public String toString() {
		return "TyreWeightSavingMst [id=" + id + ", companyMst=" + companyMst + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + ", branchMst=" + branchMst + ", vehicleNumber="
				+ vehicleNumber + ", vehicleType=" + vehicleType + ", vehicleWeight=" + vehicleWeight
				+ ", vehicleNumberProperty=" + vehicleNumberProperty + ", vehicleTypeProperty=" + vehicleTypeProperty
				+ ", vehicleWeightProperty=" + vehicleWeightProperty + "]";
	}
	
	
	
	

}
