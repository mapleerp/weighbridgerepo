package com.maple.weighbridge.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

@Entity
public class BranchMst implements Serializable {

	private static final long serialVersionUID = 1L;
	@Column(length = 50)
	@Id
	String id;

	@Column(unique = true)
	String branchName;

	
	@Column(unique = true)
	String branchCode;
	@Column(length = 50)
	String branchGst;
	@Column(length = 50)
	String branchState;
	@Column(length = 50)
	String myBranch;
	@Column(length = 50)
	String branchAddress1;
	@Column(length = 50)
	String branchAddress2;
	@Column(length = 50)
	String branchTelNo;
	@Column(length = 50)
	String branchEmail;
	@Column(length = 50)
	String bankName;
	@Column(length = 50)
	String accountNumber;
	@Column(length = 50)
	String bankBranch;
	@Column(length = 50)
	String branchPlace;
	@Column(length = 50)
	String ifsc;
	@Column(length = 50)
	
	String oldId;

	@Column(length = 50)
	String branchWebsite;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	CompanyMst companyMst;
	@Column(length = 50)
	private String  processInstanceId;
	@Column(length = 50)
	private String taskId;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBranchName() {
		return branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getBranchCode() {
		return branchCode;
	}

	public void setBranchCode(String branchCode) {
		this.branchCode = branchCode;
	}

	public String getBranchState() {
		return branchState;
	}

	public void setBranchState(String branchState) {
		this.branchState = branchState;
	}

	public String getMyBranch() {
		return myBranch;
	}

	public void setMyBranch(String myBranch) {
		this.myBranch = myBranch;
	}

	public String getBranchAddress1() {
		return branchAddress1;
	}

	public void setBranchAddress1(String branchAddress1) {
		this.branchAddress1 = branchAddress1;
	}

	public String getBranchAddress2() {
		return branchAddress2;
	}

	public void setBranchAddress2(String branchAddress2) {
		this.branchAddress2 = branchAddress2;
	}

	public String getBranchTelNo() {
		return branchTelNo;
	}

	public void setBranchTelNo(String branchTelNo) {
		this.branchTelNo = branchTelNo;
	}

	public String getBranchEmail() {
		return branchEmail;
	}

	public void setBranchEmail(String branchEmail) {
		this.branchEmail = branchEmail;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getBankBranch() {
		return bankBranch;
	}

	public void setBankBranch(String bankBranch) {
		this.bankBranch = bankBranch;
	}

	public String getBranchPlace() {
		return branchPlace;
	}

	public void setBranchPlace(String branchPlace) {
		this.branchPlace = branchPlace;
	}

	public String getIfsc() {
		return ifsc;
	}

	public void setIfsc(String ifsc) {
		this.ifsc = ifsc;
	}

	public String getBranchGst() {
		return branchGst;
	}

	public void setBranchGst(String branchGst) {
		this.branchGst = branchGst;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	public String getBranchWebsite() {
		return branchWebsite;
	}

	public void setBranchWebsite(String branchWebsite) {
		this.branchWebsite = branchWebsite;
	}
	
	

	public String getOldId() {
		return oldId;
	}

	public void setOldId(String oldId) {
		this.oldId = oldId;
	}



	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "BranchMst [id=" + id + ", branchName=" + branchName + ", branchCode=" + branchCode + ", branchGst="
				+ branchGst + ", branchState=" + branchState + ", myBranch=" + myBranch + ", branchAddress1="
				+ branchAddress1 + ", branchAddress2=" + branchAddress2 + ", branchTelNo=" + branchTelNo
				+ ", branchEmail=" + branchEmail + ", bankName=" + bankName + ", accountNumber=" + accountNumber
				+ ", bankBranch=" + bankBranch + ", branchPlace=" + branchPlace + ", ifsc=" + ifsc + ", oldId=" + oldId
				+ ", branchWebsite=" + branchWebsite + ", companyMst=" + companyMst + ", processInstanceId="
				+ processInstanceId + ", taskId=" + taskId + "]";
	}



}
