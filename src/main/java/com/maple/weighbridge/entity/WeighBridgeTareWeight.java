package com.maple.weighbridge.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

@Entity
public class WeighBridgeTareWeight implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(length = 50)
	@GeneratedValue(generator = "uuid")
	@GenericGenerator(name = "uuid", strategy = "uuid2")
	private String id;
	@Column(length = 50)
	@JsonProperty("vehicleno")
	String vehicleno;

	@JsonProperty("tareweight")
	private Integer tareweight;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "companyMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private CompanyMst companyMst;

	@ManyToOne(fetch = FetchType.EAGER, optional = false)
	@JoinColumn(name = "branchMst", nullable = false)
	@OnDelete(action = OnDeleteAction.CASCADE)
	private BranchMst branchMst;
	@Column(length = 50)
	private String processInstanceId;
	@Column(length = 50)
	private String taskId;

	public WeighBridgeTareWeight() {

		this.vehiclenoProperty = new SimpleStringProperty();
		this.tareweightProperty = new SimpleIntegerProperty();
	}

	@Transient
	private StringProperty vehiclenoProperty;

	@Transient
	private IntegerProperty tareweightProperty;

	public StringProperty getvehiclenoProperty() {
		vehiclenoProperty.set(vehicleno);
		return vehiclenoProperty;
	}

	public void setvehiclenoProperty(String vehicleno) {
		this.vehicleno = vehicleno;
	}

	@Transient
	public IntegerProperty gettareweightProperty() {
		tareweightProperty.set(tareweight);
		return tareweightProperty;
	}

	public void settareweightProperty(Integer tareweight) {
		this.tareweight = tareweight;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getVehicleno() {
		return vehicleno;
	}

	public void setVehicleno(String vehicleno) {
		this.vehicleno = vehicleno;
	}

	public Integer getTareweight() {
		return tareweight;
	}

	public void setTareweight(Integer tareweight) {
		this.tareweight = tareweight;
	}

	public CompanyMst getCompanyMst() {
		return companyMst;
	}

	public void setCompanyMst(CompanyMst companyMst) {
		this.companyMst = companyMst;
	}

	public BranchMst getBranchMst() {
		return branchMst;
	}

	public void setBranchMst(BranchMst branchMst) {
		this.branchMst = branchMst;
	}

	public String getProcessInstanceId() {
		return processInstanceId;
	}

	public void setProcessInstanceId(String processInstanceId) {
		this.processInstanceId = processInstanceId;
	}

	public String getTaskId() {
		return taskId;
	}

	public void setTaskId(String taskId) {
		this.taskId = taskId;
	}

	@Override
	public String toString() {
		return "WeighBridgeTareWeight [id=" + id + ", vehicleno=" + vehicleno + ", tareweight=" + tareweight
				+ ", companyMst=" + companyMst + ", branchMst=" + branchMst + ", processInstanceId=" + processInstanceId
				+ ", taskId=" + taskId + "]";
	}

}
