package com.maple.weighbridge.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.Socket;
import java.net.UnknownHostException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.jfree.util.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
 
import org.springframework.stereotype.Component;
 

 

import javafx.scene.control.DatePicker;

@Component

public class SystemSetting {
	// @Autowired
	// private static Environment env;

	public enum MapleDomains {
		PHARMACY, BAKERY, SUPERMARKET, HARDWARE, WHITEGOODS, RESTAURANT, ORGANIZATION
	}
	public static String STOCKTRANSFER_FORMAT;
	public static String STORE;;

 

	public static String timeverificationbeforeorderedit;
	public static String customer_site_selection;
	public static String wholesale_invoice_format;

	private static String logo_name;
	private static String posFormat;
	public static String reportpath;

	private static String cashDrawerPresent;
	public static String day_end_format;

	
 

	public static ArrayList<String> user_roles = new ArrayList();
	public static Date systemDate;

	public static String systemBranch;
	public static String myCompany;

	public static String HOST;
	public static String userId;
 
	public static boolean debugUser = false; // if debugUser then all menu permission is enabled

	public static String printer_name;

	public static String nutrition_facts_printer;
	public static int INVOICELINE_Y;

	public static int logox;
	public static int logoy;
	public static int logow;
	public static int logoh;

	public static String posinvoicetitle1;
	public static String posinvoicetitle2;
	public static String posinvoicetitle3;
	public static String posinvoicetitle4;
	public static String posinvoicetitle5;

	public static int title1x;
	public static int title1y;
	public static int title2x;
	public static int title2y;
	public static int title3x;
	public static int title3y;
	public static int title4x;
	public static int title4y;
	public static int title5x;
	public static int title5y;

	public static String BARCODE_SIZE = "SIZE 3,1.5";
	public static String BARCODE_FORMAT = "FOMAT0";
	public static String BARCODETITLE = "";
	public static Date applicationDate;
	public static String BARCODE_STARTX = "300";

	public static String NUTRITION_SIZE = "SIZE 3,1";
	public static String NUTRITION_FORMAT = "FORMAT0";
	public static String NUTRITIONTITLE = "";
	public static String SALE_ORDER_PRINT_FORMAT;

	public static String MENUC1;
	public static String MENUR1;
	public static String SMSENABLE;
	public static String PHONENUMBER;

	public static String DLLPATH = "E:\\TSCLIB.dll";

	// barcode&nutrition value combained property

	public static String version = "2.0";
	public static String combinedbarcodex;
	public static String combinedbarcodey;
	public static String combineditemnamex;
	public static String combineditemnamey;
	public static String combinedmrpx;
	public static String combinedmrpy;
	public static String combinedpkdx;
	public static String combinedpkdy;
	public static String combinedexpx;
	public static String combinedexpy;
	public static String combinedingline1x;
	public static String DISCOUNT_ENABLE;

	public static int WEIGHBRIDGEPORT;
	public static int WEIGHBRIDGEBAUDRATE;

	public static int WEIGHBRIDGEDATABITS;
	public static int WEIGHBRIDGESTOPBIT;
	public static int WEIGHBRIDGEPARITY;

	public static int WEIGHBRIDGEPAPERSIZE;

	public static String WEIGHCOMPANYTITLE;
	public static int WEIGHCOMPANYTITLEFONTSIZE;
	public static int WEIGHCOMPANYTITLEX;
	public static int WEIGHCOMPANYTITLEY;
	public static String WEIGHCOMPANYADDRESS1;
	public static int WEIGHCOMPANYADDRESS1FONTSIZE;
	public static int WEIGHCOMPANYADDRESS1X;
	public static int WEIGHCOMPANYADDRESS1Y;
	public static String WEIGHCOMPANYADDRESS2;
	public static int WEIGHCOMPANYADDRESS2FONTSIZE;
	public static int WEIGHCOMPANYADDRESS2X;
	public static int WEIGHCOMPANYADDRESS2Y;
	public static String WEIGHCOMPANYADDRESS3;
	public static int WEIGHCOMPANYADDRESS3FONTSIZE;
	public static int WEIGHCOMPANYADDRESS3X;
	public static int WEIGHCOMPANYADDRESS3Y;
	public static int WEIGHCOMPANYVEHICKENOX;
	public static int WEIGHCOMPANYVEHICKENOY;
	public static int WEIGHCOMPANYVEHICKENOFONTSIZE;
	public static int WEIGHCOMPANYRATEX;
	public static int WEIGHCOMPANYRATEY;
	public static int WEIGHCOMPANYRATEFONTSIZE;
	public static int WEIGHCOMPANYDATEX;
	public static int WEIGHCOMPANYDATEY;
	public static int WEIGHCOMPANYDATEFONTSIZE;
	public static int WEIGHCOMPANYFIRSTWEIGHTX;
	public static int WEIGHCOMPANYFIRSTWEIGHTY;
	
	
	public static int WEIGHCOMPANYFIRSTDATEX;
	public static int WEIGHCOMPANYFIRSTDATEY;

	
	

	
	
	public static int WEIGHCOMPANYFIRSTWEIGHTFONTSIZE;
	public static int WEIGHCOMPANYSECONDWEIGHTX;
	public static int WEIGHCOMPANYSECONDWEIGHTY;
	public static int WEIGHCOMPANYSECONDWEIGHTFONTSIZE;
	public static int WEIGHCOMPANYNETWEIGHTX;
	public static int WEIGHCOMPANYNETWEIGHTY;
	public static int WEIGHCOMPANYNETWEIGHTFONTSIZE;

	public static int WEIGHCOMPANYVOUCHERNUMBERY;
	public static int WEIGHCOMPANYVOUCHERNUMBERX;
	public static int WEIGHCOMPANYVOUCHERNUMBERFONTSIZE;

	public static int CHEQUECROSSLINE1X1;
	public static int CHEQUECROSSLINE1Y1;
	public static int CHEQUECROSSLINE1X2;
	public static int CHEQUECROSSLINE1Y2;
	public static int CHEQUECROSSLINE2X1;
	public static int CHEQUECROSSLINE2Y1;
	public static int CHEQUECROSSLINE2X2;
	public static int CHEQUECROSSLINE2Y2;
	public static int CHEQUEDATEX;
	public static int CHEQUEDATEY;
	public static int CHEQUEACCOUNTNAMEX;
	public static int CHEQUEACCOUNTNAMEY;
	public static int CHEQUEAMOUNTINWORDSX;
	public static int CHEQUEAMOUNTINWORDSY;
	public static int CHEQUEAMOUNTX;
	public static int CHEQUEAMOUNTY;
	public static int CHEQUEPRINTFONTSIZE;
	public static String CAMUNDAHOST;
	public static String IMPORT_PURCHASE_COSTING_METHOD;

	public static String CUSTOMER_CDREDIT_PERIOD;
	
	public static String SHOWSTOCKPOPUP;
	
	public static String POSPREFIX;
	public static String KOTPREFIX;
	
	public static String PACKAGINGLICENCE;
	public static String FSSAI;


	public static String getCombineditemnamex() {
		return combineditemnamex;
	}

	public static void setCombineditemnamex(String combineditemnamex) {
		SystemSetting.combineditemnamex = combineditemnamex;
	}

	public static String getSMSENABLE() {
		return SMSENABLE;
	}

	public static void setSMSENABLE(String sMSENABLE) {
		SMSENABLE = sMSENABLE;
	}

	public static String getPHONENUMBER() {
		return PHONENUMBER;
	}

	public static void setPHONENUMBER(String pHONENUMBER) {
		PHONENUMBER = pHONENUMBER;
	}

	public static String getCombineditemnamey() {
		return combineditemnamey;
	}

	public static void setCombineditemnamey(String combineditemnamey) {
		SystemSetting.combineditemnamey = combineditemnamey;
	}

	public static String getCombinedmrpx() {
		return combinedmrpx;
	}

	public static String getIMPORT_PURCHASE_COSTING_METHOD() {
		return IMPORT_PURCHASE_COSTING_METHOD;
	}

	public static void setIMPORT_PURCHASE_COSTING_METHOD(String iMPORT_PURCHASE_COSTING_METHOD) {
		IMPORT_PURCHASE_COSTING_METHOD = iMPORT_PURCHASE_COSTING_METHOD;
	}

	public static void setCombinedmrpx(String combinedmrpx) {
		SystemSetting.combinedmrpx = combinedmrpx;
	}

	public static String getCombinedmrpy() {
		return combinedmrpy;
	}

	public static void setCombinedmrpy(String combinedmrpy) {
		SystemSetting.combinedmrpy = combinedmrpy;
	}

	public static String getCombinedpkdx() {
		return combinedpkdx;
	}

	public static Date getApplicationDate() {
		return applicationDate;
	}

	public static void setApplicationDate(Date applicationDate) {
		SystemSetting.applicationDate = applicationDate;
	}

	public static void setCombinedpkdx(String combinedpkdx) {
		SystemSetting.combinedpkdx = combinedpkdx;
	}

	public static String getCombinedpkdy() {
		return combinedpkdy;
	}

	public static void setCombinedpkdy(String combinedpkdy) {
		SystemSetting.combinedpkdy = combinedpkdy;
	}

	public static String getCombinedingline1x() {
		return combinedingline1x;
	}

	public static void setCombinedingline1x(String combinedingline1x) {
		SystemSetting.combinedingline1x = combinedingline1x;
	}

	public static String getCombinedingline1y() {
		return combinedingline1y;
	}

	public static void setCombinedingline1y(String combinedingline1y) {
		SystemSetting.combinedingline1y = combinedingline1y;
	}

	public static String getCombinedingline2x() {
		return combinedingline2x;
	}

	public static void setCombinedingline2x(String combinedingline2x) {
		SystemSetting.combinedingline2x = combinedingline2x;
	}

	public static String getCombinedingline2y() {
		return combinedingline2y;
	}

	public static void setCombinedingline2y(String combinedingline2y) {
		SystemSetting.combinedingline2y = combinedingline2y;
	}

	public static String getCombinednutritioninformx() {
		return combinednutritioninformx;
	}

	public static String getDISCOUNT_ENABLE() {
		return DISCOUNT_ENABLE;
	}

	public static void setDISCOUNT_ENABLE(String dISCOUNT_ENABLE) {
		DISCOUNT_ENABLE = dISCOUNT_ENABLE;
	}

	public static void setCombinednutritioninformx(String combinednutritioninformx) {
		SystemSetting.combinednutritioninformx = combinednutritioninformx;
	}

	public static String getCombinednutritioninformy() {
		return combinednutritioninformy;
	}

	public static void setCombinednutritioninformy(String combinednutritioninformy) {
		SystemSetting.combinednutritioninformy = combinednutritioninformy;
	}

	public static String getCombinedservingsizex() {
		return combinedservingsizex;
	}

	public static void setCombinedservingsizex(String combinedservingsizex) {
		SystemSetting.combinedservingsizex = combinedservingsizex;
	}

	public static String getCombinedservingsizey() {
		return combinedservingsizey;
	}

	public static void setCombinedservingsizey(String combinedservingsizey) {
		SystemSetting.combinedservingsizey = combinedservingsizey;
	}

	public static String getCombinednutritionfactsydelta() {
		return combinednutritionfactsydelta;
	}

	public static void setCombinednutritionfactsydelta(String combinednutritionfactsydelta) {
		SystemSetting.combinednutritionfactsydelta = combinednutritionfactsydelta;
	}

	public static String combinedingline1y;
	public static String combinedingline2x;
	public static String combinedingline2y;
	public static String combinednutritioninformx;
	public static String combinednutritioninformy;
	public static String combinedservingsizex;
	public static String combinedservingsizey;
	public static String combinedfatx;
	public static String combinedfaty;
	public static String combinebatchx;
	public static String combinedbatchy;
	public static String combinedcaloriex;
	public static String combinedcaloriey;
	public static String nutritionfact;

	public static String combinedfontsizemrp;
	public static String combinedfontsizeitem;
	public static String combinedfontsizedates;
	public static String combinedfontsizenutritionfacts;
	public static String combinedfontsizenutritioninformation;
	public static String combinednutritionfactsydelta;

	public static String barcodex;
	public static String barcodey;
	public static String barcodeManufactureDatex;
	public static String barcodeManufactureDatey;
	public static String barcodeExpiryDatex;
	public static String barcodeExpiryDatey;
	public static String barcodeItemNamex;
	public static String barcodeItemNamey;
	public static String barcodeIngLine1x;
	public static String barcodeIngLine1y;
	public static String barcodeIngLine2x;
	public static String barcodeIngLine2y;
	public static String barcodeMrpx;
	public static String barcodeMrpy;
	public static String barcodeNetWtx;
	public static String barcodeNetWty;
	public static String batchLinex;
	public static String batchLiney;
	public static String nutritionTitleX;
	public static String nutritionTitleY;
	public static String nutritionFactX;
	public static String nutritionFactY;
	public static String barcodeTitleX;
	public static String barcodeTitleY;
	public static boolean HOLDPROPERTY;
	public static String ONLINEDAYENDREPORT;
	public static String SHOW_PREVIOUS_INVOICEAMOUNT_IN_POS;
	public static boolean NEGATIVEBILLING;
	
	/*TallyRetry*/
	
	public static String tallyServer;

	public static boolean isNEGATIVEBILLING() {
		return NEGATIVEBILLING;
	}

	public static void setNEGATIVEBILLING(boolean nEGATIVEBILLING) {
		NEGATIVEBILLING = nEGATIVEBILLING;
	}

	public static String posInvoiceBottomLine;

	public static String posInvoiceBottomLine2;
	
	public static String posInvoiceBottomLine3;
	
	/*
	 * /* public static int barcodetitle5y; public static int barcodetitle5y; public
	 * static int barcodetitle5y; public static int barcodetitle5y;
	 */

	public static String getPosInvoiceBottomLine3() {
		return posInvoiceBottomLine3;
	}

	public static void setPosInvoiceBottomLine3(String posInvoiceBottomLine3) {
		SystemSetting.posInvoiceBottomLine3 = posInvoiceBottomLine3;
	}

	public static String getPosInvoiceBottomLine2() {
		return posInvoiceBottomLine2;
	}

	public static void setPosInvoiceBottomLine2(String posInvoiceBottomLine2) {
		SystemSetting.posInvoiceBottomLine2 = posInvoiceBottomLine2;
	}

	public static String ONLINE_SALES_PREFIX = "POS";
	public static String POS_SALES_PREFIX = "POS";
	public static String WHOLE_SALES_PREFIX = "WS";
	public static String GST_INVOCE_PREFIX = "WS";
	public static String VAN_SALES_PREFIX = "VS";
	public static String KOT_SALES_PREFIX;
	public static String SALE_ORDER_PREFIX;
	public static int STOCK_VERIFICATION_COUNT;
	public static String STOCK_TRANSFER_PREFIX;
	private static String financialYear = "2019-2020";

	public static String ITEM_CODE_GENERATOR_ID = "ICD";
	
	

	

	public SystemSetting() {
		systemDate = new java.util.Date();
	}

	public static String getFinancialYear() {

		return financialYear;
	}

	public static String getSTOCK_TRANSFER_PREFIX() {
		return STOCK_TRANSFER_PREFIX;
	}

	public static void setSTOCK_TRANSFER_PREFIX(String sTOCK_TRANSFER_PREFIX) {
		STOCK_TRANSFER_PREFIX = sTOCK_TRANSFER_PREFIX;
	}

	public static String getBarcodeTitleX() {
		return barcodeTitleX;
	}

	public static void setBarcodeTitleX(String barcodeTitleX) {
		SystemSetting.barcodeTitleX = barcodeTitleX;
	}

	public static String getBarcodeTitleY() {
		return barcodeTitleY;
	}

	public static void setBarcodeTitleY(String barcodeTitleY) {
		SystemSetting.barcodeTitleY = barcodeTitleY;
	}

	public static String getBatchLinex() {
		return batchLinex;
	}

	public static void setBatchLinex(String batchLinex) {
		SystemSetting.batchLinex = batchLinex;
	}

	public static String getNutritionTitleX() {
		return nutritionTitleX;
	}

	public static void setNutritionTitleX(String nutritionTitleX) {
		SystemSetting.nutritionTitleX = nutritionTitleX;
	}



	public static String getNutritionFactX() {
		return nutritionFactX;
	}

	public static void setNutritionFactX(String nutritionFactX) {
		SystemSetting.nutritionFactX = nutritionFactX;
	}

	public static String getNutritionFactY() {
		return nutritionFactY;
	}

	public static void setNutritionFactY(String nutritionFactY) {
		SystemSetting.nutritionFactY = nutritionFactY;
	}

	public static String getNutritionTitleY() {
		return nutritionTitleY;
	}

	public static void setNutritionTitleY(String nutritionTitleY) {
		SystemSetting.nutritionTitleY = nutritionTitleY;
	}

	public static String getBatchLiney() {
		return batchLiney;
	}

	public static void setBatchLiney(String batchLiney) {
		SystemSetting.batchLiney = batchLiney;
	}

	public static Date getSystemDate() {
		systemDate = new java.util.Date();
		return systemDate;
	}

	public static String getCombinedcaloriex() {
		return combinedcaloriex;
	}

	public static void setCombinedcaloriex(String combinedcaloriex) {
		SystemSetting.combinedcaloriex = combinedcaloriex;
	}

	public static String getCombinedcaloriey() {
		return combinedcaloriey;
	}

	public static void setCombinedcaloriey(String combinedcaloriey) {
		SystemSetting.combinedcaloriey = combinedcaloriey;
	}

	public static String getNutritionfact() {
		return nutritionfact;
	}

	public static void setNutritionfact(String nutritionfact) {
		SystemSetting.nutritionfact = nutritionfact;
	}

	public static int getSTOCK_VERIFICATION_COUNT() {
		return STOCK_VERIFICATION_COUNT;
	}

	public static void setSTOCK_VERIFICATION_COUNT(int sTOCK_VERIFICATION_COUNT) {
		STOCK_VERIFICATION_COUNT = sTOCK_VERIFICATION_COUNT;
	}

	public static String getBarcodeNetWtx() {
		return barcodeNetWtx;
	}

	public static void setBarcodeNetWtx(String barcodeNetWtx) {
		SystemSetting.barcodeNetWtx = barcodeNetWtx;
	}

	public static String getBarcodeNetWty() {
		return barcodeNetWty;
	}

	public static void setBarcodeNetWty(String barcodeNetWty) {
		SystemSetting.barcodeNetWty = barcodeNetWty;
	}

	public static String getPREVIOUS_INVOICEAMOUNT_IN_POS() {
		return SHOW_PREVIOUS_INVOICEAMOUNT_IN_POS;
	}

	public static void setPREVIOUS_INVOICEAMOUNT_IN_POS(String pREVIOUS_INVOICEAMOUNT_IN_POS) {
		SHOW_PREVIOUS_INVOICEAMOUNT_IN_POS = pREVIOUS_INVOICEAMOUNT_IN_POS;
	}

	public static String getONLINEDAYENDREPORT() {
		return ONLINEDAYENDREPORT;
	}

	public static void setONLINEDAYENDREPORT(String oNLINEDAYENDREPORT) {
		ONLINEDAYENDREPORT = oNLINEDAYENDREPORT;
	}

	public static java.util.Date localToUtilDate(LocalDate lDate) {

		Date date = Date.from(lDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
		return date;
	}

	public static LocalDate utilToLocaDate(java.util.Date uDate) {

		LocalDate date = uDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

		return date;
	}

	/*
	 * public Pair<Date, Date> getDateRange() { Date begining, end;
	 * 
	 * { Calendar calendar = getCalendarForNow();
	 * calendar.set(Calendar.DAY_OF_MONTH,
	 * calendar.getActualMinimum(Calendar.DAY_OF_MONTH));
	 * setTimeToBeginningOfDay(calendar); begining = calendar.getTime(); }
	 * 
	 * { Calendar calendar = getCalendarForNow();
	 * calendar.set(Calendar.DAY_OF_MONTH,
	 * calendar.getActualMaximum(Calendar.DAY_OF_MONTH));
	 * setTimeToEndofDay(calendar); end = calendar.getTime(); }
	 * 
	 * return Pair.of(begining, end); }
	 */

	private static Calendar getCalendarForNow() {
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setTime(new Date());
		return calendar;
	}

	private static void setTimeToBeginningOfDay(Calendar calendar) {
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
	}

	private static void setTimeToEndofDay(Calendar calendar) {
		calendar.set(Calendar.HOUR_OF_DAY, 23);
		calendar.set(Calendar.MINUTE, 59);
		calendar.set(Calendar.SECOND, 59);
		calendar.set(Calendar.MILLISECOND, 999);
	}

	public String getCurrentLocalDateTimeStampAsString() {
		return LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.SSS"));
	}

	public Timestamp getCurrentDatTime() {

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		Timestamp time = new Timestamp(cal.getTimeInMillis());

		return time;

	}

	public Date addDaysToDate(Date date, int days) {

		// First convert to local Date
		LocalDate lDate = utilToLocaDate(date);
		lDate = lDate.plusDays(days);

		// Now convert back to util date and return
		return localToUtilDate(lDate);

	}

	public boolean isFutureDate(Date fDate) {

		boolean isfuture = false;

		Date current = new Date();

		// compare both dates
		if (fDate.after(current)) {
			isfuture = true;
		} else {
			isfuture = false;
		}
		return isfuture;
	}

	public long DateLeft(Date fDate) {

		Date current = new Date();
		long diff = fDate.getTime() - current.getTime();

		long diffSeconds = diff / 1000;

		long diffMinutes = diffSeconds / 60;

		long diffHr = diffMinutes / 60;
		long diffDays = diffHr / 24;

		return diffDays;
	}

	public static boolean pingServer(String IP) {
		boolean pinged = false;
		try {
			Process p = Runtime.getRuntime().exec("ping " + IP);
			BufferedReader inputStream = new BufferedReader(new InputStreamReader(p.getInputStream()));

			String s = "";
			// reading output stream of the command
			while ((s = inputStream.readLine()) != null) {
				System.out.println(s);
				pinged = true;
			}

		} catch (Exception e) {
			e.printStackTrace();
			return pinged;
		}
		return true;
	}

 

	public boolean isValidEmailAddress(String email) {
		String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
		java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
		java.util.regex.Matcher m = p.matcher(email);
		return m.matches();
	}

	public static String padLeft(String s, int n) {
		// return String.format("%1$" + n + "s", s);
		return String.format("%1$" + n + "s", s);
	}

	public static double round(double value, int places) {

		if (places < 0) {
			throw new IllegalArgumentException();
		}

		BigDecimal bd = new BigDecimal(value);
		bd = bd.setScale(places, BigDecimal.ROUND_HALF_UP);
		return bd.doubleValue();
	}

	public static double round(java.math.BigDecimal value, int places) {

		if (places < 0) {
			throw new IllegalArgumentException();
		}

		value = value.setScale(places, BigDecimal.ROUND_HALF_UP);
		return value.doubleValue();
	}

	public static boolean IsValidNumber(String inString) {
		Pattern p = Pattern.compile("^[0-9]\\d*(\\.\\d+)?$");
		Matcher m = p.matcher(inString);
		boolean b = m.matches();
		return b;
	}

	public static java.util.Date StringToUtilDate(String strDate, String Format) {

		// String ReplaceStringDate = strDate.replace("-", "/");
		java.util.Date dt = StringToutilDateSlash(strDate, Format);
		return dt;
	}

	public static int getINVOICELINE_Y() {
		return INVOICELINE_Y;
	}

	public static void setINVOICELINE_Y(int iNVOICELINE_Y) {
		INVOICELINE_Y = iNVOICELINE_Y;
	}

	public java.sql.Date StringToSqlDate(String strDate, String Format) {

		String ReplaceStringDate = strDate.replace("-", "/");
		String ReplaceFormat = Format.replace("-", "/");
		java.sql.Date dt = StringToSqlDateSlash(ReplaceStringDate, ReplaceFormat);
		return dt;
	}

	private static java.util.Date StringToutilDateSlash(String strDate, String Format) {
		SimpleDateFormat formatter = new SimpleDateFormat(Format); // "dd/MM/yyyy"
		// String dateInString = "7-Jun-2013";
		Date date = null;
		try {

			date = formatter.parse(strDate);
			System.out.println(date);
			System.out.println(formatter.format(date));

		} catch (ParseException e) {
			 Log.info(e.toString());
		}
		return date;
	}

	public static java.sql.Date StringToSqlDateSlash(String strDate, String Format) {
		SimpleDateFormat formatter = new SimpleDateFormat(Format);// "dd/MM/yyyy"

		Date dt = StringToutilDateSlash(strDate, Format);
		java.sql.Date sqlDate = new java.sql.Date(dt.getTime());

		return sqlDate;
	}

	public String StringToStringFormatChange(String strDate, String inFormat, String outFormat) {

		String ddmmyyDate = "";
		SimpleDateFormat formatter = new SimpleDateFormat(inFormat);// "dd/MM/yyyy"

		Date dt = StringToutilDateSlash(strDate, inFormat);
		java.sql.Date sqlDate = new java.sql.Date(dt.getTime());

		if (outFormat.equals("dd/MM/yyyy")) {
			ddmmyyDate = SqlDateTostring(sqlDate);
		} else if (outFormat.equals("yyyy-MM-dd")) {
			ddmmyyDate = SqlDateTostringYYMMDD(sqlDate);
		} else {
			ddmmyyDate = "OutFormat not supported";
		}

		return ddmmyyDate;
	}

	public static String SqlDateTostring(Date date) {

		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String text = df.format(date);

		return text;
	}

	public java.util.Date SqlDateToUtilDate(java.sql.Date date) {

		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String text = df.format(date);

		java.util.Date uDate = StringToutilDateSlash(text, "dd/MM/yyyy");
		return uDate;
	}

	public String SqlDateTostringYYMMDD(Date date) {

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		String text = formatter.format(date);

		return text;
	}

	public static String UtilDateToString(java.util.Date dt) {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String reportDate = df.format(dt);
		Log.info(reportDate);
		 
		return reportDate;
	}

	public static String UtilDateToString(java.util.Date dt, String foamat) {
		DateFormat df = new SimpleDateFormat(foamat);
		String reportDate = df.format(dt);
	 
		return reportDate;
	}

	public java.sql.Date UtilDateToSQLDate(java.util.Date dt) {
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		String reportDate = df.format(dt);

		java.sql.Date sqlDate = StringToSqlDateSlash(reportDate, "dd/MM/yyyy");

		return sqlDate;
	}

	public static String getSALE_ORDER_PRINT_FORMAT() {
		return SALE_ORDER_PRINT_FORMAT;
	}

	public static void setSALE_ORDER_PRINT_FORMAT(String sALE_ORDER_PRINT_FORMAT) {
		SALE_ORDER_PRINT_FORMAT = sALE_ORDER_PRINT_FORMAT;
	}

	public boolean isValidDate(String dateString) {

		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date Systemdate = new Date();
		int Sysyear = Calendar.getInstance().get(Calendar.YEAR);

		dateString = dateString.replace("/", "");
		dateString = dateString.replace("-", "");
		if (dateString.length() == 7) {
			dateString = "0".concat(dateString);
		}

		if (dateString == null || (dateString.length() != "yyyyMMdd".length())) {
			return false;
		}
		// ddMMyyyy

		String strDD = dateString.substring(0, 2);
		String strMon = dateString.substring(2, 4);
		String strYy = dateString.substring(4, 8);
		dateString = strYy + strMon + strDD;

		int date;
		try {
			date = Integer.parseInt(dateString);
		} catch (NumberFormatException e) {
			return false;
		}

		int year = date / 10000;
		int month = (date % 10000) / 100;
		int day = date % 100;

		// leap years calculation not valid before 1581
		boolean yearOk = (year >= Sysyear - 1) && (year <= Sysyear + 1);
		boolean monthOk = (month >= 1) && (month <= 12);
		boolean dayOk = (day >= 1) && (day <= daysInMonth(year, month));

		return (yearOk && monthOk && dayOk);
	}

	private int daysInMonth(int year, int month) {
		int daysInMonth;
		switch (month) {
		case 1: // fall through
		case 3: // fall through
		case 5: // fall through
		case 7: // fall through
		case 8: // fall through
		case 10: // fall through
		case 12:
			daysInMonth = 31;
			break;
		case 2:
			if (((year % 4 == 0) && (year % 100 != 0)) || (year % 400 == 0)) {
				daysInMonth = 29;
			} else {
				daysInMonth = 28;
			}
			break;
		default:
			// returns 30 even for nonexistant months
			daysInMonth = 30;
		}
		return daysInMonth;
	}

	public static String AmountInWords(String StrAmt, String RupeName, String PaiseName) {

		String AmtWord = "";
		ToWordsCrore TwC = new ToWordsCrore();

		System.out.println("StrAmt = " + StrAmt);

		int pos = StrAmt.indexOf(".");
		String StrIPart = "";
		String StrDPart = "";
		if (pos > 0) {
			StrIPart = StrAmt.substring(0, pos);
			StrDPart = StrAmt.substring(pos + 1);

		} else {
			StrIPart = StrAmt;
		}

		System.out.println("Int Part =" + StrIPart);
		System.out.println("Deci Part =" + StrDPart);
		if (StrDPart.length() > 2) {
			StrDPart = StrDPart.substring(0, 2);
			System.out.println("Modi Deci Part =" + StrDPart);
		}

		if (StrDPart.length() == 0) {
			StrDPart = "0";
		}

		long iPart = Long.parseLong(StrIPart);

		long fPart = Long.parseLong(StrDPart);

		AmtWord = TwC.convertNumberToWords(iPart);
		System.out.println("in words ipart  =" + AmtWord);

		String Paise;
		if (fPart > 0) {
			String strfp = fPart + "";
			if (StrDPart.length() == 1) {
				fPart = fPart * 10;

			}
			Paise = TwC.convertNumberToWords(fPart);
			AmtWord = AmtWord + " " + RupeName + " And " + Paise + " " + PaiseName;
			System.out.println("in words paise  =" + Paise);

		} else {
			AmtWord = AmtWord + " " + RupeName;
		}
		// System.out.println("in words paise =" + AmtWord);

		return AmtWord;
	}

	public static String getSystemBranch() {
		return systemBranch;
	}

	public static void setSystemBranch(String systemBranch) {
		SystemSetting.systemBranch = systemBranch;
	}

	public static String getHOST() {
		return HOST;
	}

	public static void setHOST(String hOST) {
		HOST = hOST;
	}

	public static String getUserId() {
		return userId;
	}

	public static void setUserId(String userId) {
		SystemSetting.userId = userId;
	}

	public static boolean deleteFile(String filePath) {
		// initialize File object
		File file = new File(filePath);

		boolean result = false;
		try {
			// delete the file specified
			result = file.delete();
			// test if successfully deleted the file
			if (result) {
				System.out.println("Successfully deleted: " + file.getCanonicalPath());
			} else {
				System.out.println("Failed deleting " + file.getCanonicalPath());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return result;
	}

	public static boolean UserHasRole(String RoleName) {

		for (Iterator itr = user_roles.iterator(); itr.hasNext();) {
			String aRole = (String) itr.next();

			if (RoleName.equalsIgnoreCase(aRole)) {

				return true;
			}
		}

		if (SystemSetting.debugUser) {
			return true;
		}

		return false;

	}

	

	public static String getPosFormat() {

		return posFormat;
	}

	public static boolean hasCashDrawer() {

		if (cashDrawerPresent.equalsIgnoreCase("YES") || cashDrawerPresent.equalsIgnoreCase("Y")) {
			return true;
		} else {
			return false;
		}

	}

	public static String getCashDrawerPresent() {
		return cashDrawerPresent;
	}

	public static void setCashDrawerPresent(String cashDrawerPresent) {
		SystemSetting.cashDrawerPresent = cashDrawerPresent;
	}

	public static void setPosFormat(String posFormat) {
		SystemSetting.posFormat = posFormat;
	}

	public static String getLogo_name() {
		return logo_name;
	}

	public static void setLogo_name(String logo_name) {
		SystemSetting.logo_name = logo_name;
	}

	public static String getTimeverificationbeforeorderedit() {
		return timeverificationbeforeorderedit;
	}

	public static void setTimeverificationbeforeorderedit(String timeverificationbeforeorderedit) {
		SystemSetting.timeverificationbeforeorderedit = timeverificationbeforeorderedit;
	}

	public static String getWholesale_invoice_format() {
		return wholesale_invoice_format;
	}

	public static void setWholesale_invoice_format(String wholesale_invoice_format) {
		SystemSetting.wholesale_invoice_format = wholesale_invoice_format;
	}

	public static String getCustomer_site_selection() {
		return customer_site_selection;
	}

	public static void setCustomer_site_selection(String customer_site_selection) {
		SystemSetting.customer_site_selection = customer_site_selection;
	}

	public static String getPrinter_name() {
		return printer_name;
	}

	public static void setNutrition_facts_printer(String nutrition_facts_printer) {
		SystemSetting.nutrition_facts_printer = nutrition_facts_printer;
	}

	public static String getNutrition_facts_printer() {
		return nutrition_facts_printer;
	}

	public static void setPrinter_name(String printer_name) {
		SystemSetting.printer_name = printer_name;
	}

	public static String getReportpath() {
		return reportpath;
	}

	public static void setReportpath(String reportpath) {
		SystemSetting.reportpath = reportpath;
	}

 

	public static int getLogox() {
		return logox;
	}

	public static void setLogox(int logox) {
		SystemSetting.logox = logox;
	}

	public static int getLogoy() {
		return logoy;
	}

	public static void setLogoy(int logoy) {
		SystemSetting.logoy = logoy;
	}

	public static int getLogow() {
		return logow;
	}

	public static void setLogow(int logow) {
		SystemSetting.logow = logow;
	}

	public static int getLogoh() {
		return logoh;
	}

	public static void setLogoh(int logoh) {
		SystemSetting.logoh = logoh;
	}

	public static String getPosinvoicetitle1() {
		return posinvoicetitle1;
	}

	public static void setPosinvoicetitle1(String posinvoicetitle1) {
		SystemSetting.posinvoicetitle1 = posinvoicetitle1;
	}

	public static String getPosinvoicetitle2() {
		return posinvoicetitle2;
	}

	public static void setPosinvoicetitle2(String posinvoicetitle2) {
		SystemSetting.posinvoicetitle2 = posinvoicetitle2;
	}

	public static String getPosinvoicetitle3() {
		return posinvoicetitle3;
	}

	public static void setPosinvoicetitle3(String posinvoicetitle3) {
		SystemSetting.posinvoicetitle3 = posinvoicetitle3;
	}

	public static String getPosinvoicetitle4() {
		return posinvoicetitle4;
	}

	public static void setPosinvoicetitle4(String posinvoicetitle4) {
		SystemSetting.posinvoicetitle4 = posinvoicetitle4;
	}

	public static String getPosinvoicetitle5() {
		return posinvoicetitle5;
	}

	public static void setPosinvoicetitle5(String posinvoicetitle5) {
		SystemSetting.posinvoicetitle5 = posinvoicetitle5;
	}

	public static int getTitle1x() {
		return title1x;
	}

	public static void setTitle1x(int title1x) {
		SystemSetting.title1x = title1x;
	}

	public static int getTitle1y() {
		return title1y;
	}

	public static void setTitle1y(int title1y) {
		SystemSetting.title1y = title1y;
	}

	public static int getTitle2x() {
		return title2x;
	}

	public static void setTitle2x(int title2x) {
		SystemSetting.title2x = title2x;
	}

	public static int getTitle2y() {
		return title2y;
	}

	public static void setTitle2y(int title2y) {
		SystemSetting.title2y = title2y;
	}

	public static boolean isHOLDPROPERTY() {
		return HOLDPROPERTY;
	}

	public static void setHOLDPROPERTY(boolean hOLDPROPERTY) {
		HOLDPROPERTY = hOLDPROPERTY;
	}

	public static int getTitle3x() {
		return title3x;
	}

	public static void setTitle3x(int title3x) {
		SystemSetting.title3x = title3x;
	}

	public static int getTitle3y() {
		return title3y;
	}

	public static void setTitle3y(int title3y) {
		SystemSetting.title3y = title3y;
	}

	public static int getTitle4x() {
		return title4x;
	}

	public static void setTitle4x(int title4x) {
		SystemSetting.title4x = title4x;
	}

	public static int getTitle4y() {
		return title4y;
	}

	public static void setTitle4y(int title4y) {
		SystemSetting.title4y = title4y;
	}

	public static int getTitle5x() {
		return title5x;
	}

	public static void setTitle5x(int title5x) {
		SystemSetting.title5x = title5x;
	}

	public static int getTitle5y() {
		return title5y;
	}

	public static void setTitle5y(int title5y) {
		SystemSetting.title5y = title5y;
	}
	
	

 

	public static String getBARCODE_SIZE() {
		return BARCODE_SIZE;
	}

	public static void setBARCODE_SIZE(String bARCODE_SIZE) {
		BARCODE_SIZE = bARCODE_SIZE;
	}

	public static String getBARCODE_FORMAT() {
		return BARCODE_FORMAT;
	}

	public static void setBARCODE_FORMAT(String bARCODE_FORMAT) {
		BARCODE_FORMAT = bARCODE_FORMAT;
	}

	public static String getNUTRITION_SIZE() {
		return NUTRITION_SIZE;
	}

	public static void setNUTRITION_SIZE(String nUTRITION_SIZE) {
		NUTRITION_SIZE = nUTRITION_SIZE;
	}

	public static String getNUTRITION_FORMAT() {
		return NUTRITION_FORMAT;
	}

	public static void setNUTRITION_FORMAT(String nUTRITION_FORMAT) {
		NUTRITION_FORMAT = nUTRITION_FORMAT;
	}

	public static String getNUTRITIONTITLE() {
		return NUTRITIONTITLE;
	}

	public static void setNUTRITIONTITLE(String nUTRITIONTITLE) {
		NUTRITIONTITLE = nUTRITIONTITLE;
	}

	public static ArrayList<String> getUser_roles() {
		return user_roles;
	}

	public static void setUser_roles(ArrayList<String> user_roles) {
		SystemSetting.user_roles = user_roles;
	}

	public static String getReportPath() {
		return reportpath;
	}

	public static void setReportPath(String reportPath) {
		SystemSetting.reportpath = reportPath;
	}

	public static boolean isDebugUser() {
		return debugUser;
	}

	public static void setDebugUser(boolean debugUser) {
		SystemSetting.debugUser = debugUser;
	}

	public static String getBARCODETITLE() {
		return BARCODETITLE;
	}

	public static void setBARCODETITLE(String bARCODETITLE) {
		BARCODETITLE = bARCODETITLE;
	}

	public static String getBARCODE_STARTX() {
		return BARCODE_STARTX;
	}

	public static void setBARCODE_STARTX(String bARCODE_STARTX) {
		BARCODE_STARTX = bARCODE_STARTX;
	}

	public static String getDLLPATH() {
		return DLLPATH;
	}

	public static void setDLLPATH(String dLLPATH) {
		DLLPATH = dLLPATH;
	}

	public static String getBarcodex() {
		return barcodex;
	}

	public static void setBarcodex(String barcodex) {
		SystemSetting.barcodex = barcodex;
	}

	public static String getBarcodey() {
		return barcodey;
	}

	public static void setBarcodey(String barcodey) {
		SystemSetting.barcodey = barcodey;
	}

	public static String getBarcodeManufactureDatex() {
		return barcodeManufactureDatex;
	}

	public static void setBarcodeManufactureDatex(String barcodeManufactureDatex) {
		SystemSetting.barcodeManufactureDatex = barcodeManufactureDatex;
	}

	public static String getBarcodeManufactureDatey() {
		return barcodeManufactureDatey;
	}

	public static void setBarcodeManufactureDatey(String barcodeManufactureDatey) {
		SystemSetting.barcodeManufactureDatey = barcodeManufactureDatey;
	}

	public static String getBarcodeExpiryDatex() {
		return barcodeExpiryDatex;
	}

	public static void setBarcodeExpiryDatex(String barcodeExpiryDatex) {
		SystemSetting.barcodeExpiryDatex = barcodeExpiryDatex;
	}

	public static String getBarcodeExpiryDatey() {
		return barcodeExpiryDatey;
	}

	public static void setBarcodeExpiryDatey(String barcodeExpiryDatey) {
		SystemSetting.barcodeExpiryDatey = barcodeExpiryDatey;
	}

	public static String getBarcodeItemNamex() {
		return barcodeItemNamex;
	}

	public static void setBarcodeItemNamex(String barcodeItemNamex) {
		SystemSetting.barcodeItemNamex = barcodeItemNamex;
	}

	public static String getBarcodeItemNamey() {
		return barcodeItemNamey;
	}

	public static void setBarcodeItemNamey(String barcodeItemNamey) {
		SystemSetting.barcodeItemNamey = barcodeItemNamey;
	}

	public static String getBarcodeIngLine1x() {
		return barcodeIngLine1x;
	}

	public static void setBarcodeIngLine1x(String barcodeIngLine1x) {
		SystemSetting.barcodeIngLine1x = barcodeIngLine1x;
	}

	public static String getBarcodeIngLine1y() {
		return barcodeIngLine1y;
	}

	public static void setBarcodeIngLine1y(String barcodeIngLine1y) {
		SystemSetting.barcodeIngLine1y = barcodeIngLine1y;
	}

	public static String getBarcodeIngLine2x() {
		return barcodeIngLine2x;
	}

	public static void setBarcodeIngLine2x(String barcodeIngLine2x) {
		SystemSetting.barcodeIngLine2x = barcodeIngLine2x;
	}

	public static String getBarcodeIngLine2y() {
		return barcodeIngLine2y;
	}

	public static void setBarcodeIngLine2y(String barcodeIngLine2y) {
		SystemSetting.barcodeIngLine2y = barcodeIngLine2y;
	}

	public static String getBarcodeMrpx() {
		return barcodeMrpx;
	}

	public static void setBarcodeMrpx(String barcodeMrpx) {
		SystemSetting.barcodeMrpx = barcodeMrpx;
	}

	public static String getBarcodeMrpy() {
		return barcodeMrpy;
	}

	public static void setBarcodeMrpy(String barcodeMrpy) {
		SystemSetting.barcodeMrpy = barcodeMrpy;
	}

	public static String getSHOW_PREVIOUS_INVOICEAMOUNT_IN_POS() {
		return SHOW_PREVIOUS_INVOICEAMOUNT_IN_POS;
	}

	public static void setSHOW_PREVIOUS_INVOICEAMOUNT_IN_POS(String sHOW_PREVIOUS_INVOICEAMOUNT_IN_POS) {
		SHOW_PREVIOUS_INVOICEAMOUNT_IN_POS = sHOW_PREVIOUS_INVOICEAMOUNT_IN_POS;
	}

	public static String getPosInvoiceBottomLine() {
		return posInvoiceBottomLine;
	}

	
 
	public static void setPosInvoiceBottomLine(String posInvoiceBottomLine) {
		SystemSetting.posInvoiceBottomLine = posInvoiceBottomLine;
	}

	public static String getONLINE_SALES_PREFIX() {
		return ONLINE_SALES_PREFIX;
	}

	public static void setONLINE_SALES_PREFIX(String oNLINE_SALES_PREFIX) {
		ONLINE_SALES_PREFIX = oNLINE_SALES_PREFIX;
	}

	public static String getPOS_SALES_PREFIX() {
		return POS_SALES_PREFIX;
	}

	public static void setPOS_SALES_PREFIX(String pOS_SALES_PREFIX) {
		POS_SALES_PREFIX = pOS_SALES_PREFIX;
	}

	public static String getWHOLE_SALES_PREFIX() {
		return WHOLE_SALES_PREFIX;
	}

	public static void setWHOLE_SALES_PREFIX(String wHOLE_SALES_PREFIX) {
		WHOLE_SALES_PREFIX = wHOLE_SALES_PREFIX;
	}

	public static String getGST_INVOCE_PREFIX() {
		return GST_INVOCE_PREFIX;
	}

	public static void setGST_INVOCE_PREFIX(String gST_INVOCE_PREFIX) {
		GST_INVOCE_PREFIX = gST_INVOCE_PREFIX;
	}

	public static String getVAN_SALES_PREFIX() {
		return VAN_SALES_PREFIX;
	}

	public static void setVAN_SALES_PREFIX(String vAN_SALES_PREFIX) {
		VAN_SALES_PREFIX = vAN_SALES_PREFIX;
	}

	public static String getKOT_SALES_PREFIX() {
		return KOT_SALES_PREFIX;
	}

	public static void setKOT_SALES_PREFIX(String kOT_SALES_PREFIX) {
		KOT_SALES_PREFIX = kOT_SALES_PREFIX;
	}

	public static String getITEM_CODE_GENERATOR_ID() {
		return ITEM_CODE_GENERATOR_ID;
	}

	public static void setITEM_CODE_GENERATOR_ID(String iTEM_CODE_GENERATOR_ID) {
		ITEM_CODE_GENERATOR_ID = iTEM_CODE_GENERATOR_ID;
	}

 

	public static void setSystemDate(Date systemDate) {
		SystemSetting.systemDate = systemDate;
	}

	public static void setFinancialYear(String financialYear) {
		SystemSetting.financialYear = financialYear;
	}
	
	
	//==============
	public static DatePicker  datePickerFormat(DatePicker datePicker,String pattern ) {

		//String pattern = "yyyy-MM-dd";

		datePicker.setPromptText(pattern.toLowerCase());

		datePicker.setConverter(new javafx.util.StringConverter<LocalDate>() {
		     DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern(pattern);

		     @Override 
		     public String toString(LocalDate date) {
		         if (date != null) {
		             return dateFormatter.format(date);
		         } else {
		             return "";
		         }
		     }

		     @Override 
		     public LocalDate fromString(String string) {
		         if (string != null && !string.isEmpty()) {
		             return LocalDate.parse(string, dateFormatter);
		         } else {
		             return null;
		         }
		     }
		 });
		return datePicker;
	}

 
	
	
	
}
