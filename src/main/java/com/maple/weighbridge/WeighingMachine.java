package com.maple.weighbridge;

 
import com.google.common.eventbus.EventBus;
import com.maple.weighbridge.event.WeighingMachineDataEvent;
import com.fazecast.jSerialComm.SerialPort;

public class WeighingMachine implements Runnable {

	private int portNumber;
	private int baudRate;
	private int dataBit;
	
	private int stopBit;
	
	private int parityBit;
	
	
	private static EventBus eventBus = EventBusFactory.getEventBus();

	public WeighingMachine(int parameter, int baudRate, int dataBit, int stopBit, int parityBit) {
		this.portNumber = parameter;
		this.baudRate = baudRate;
		this.dataBit = dataBit;
		this.stopBit=stopBit;
		this.parityBit=parityBit;
		
		
	}

	@Override
	public void run() {
		 getWeightFromMachine(portNumber, baudRate,dataBit,stopBit,parityBit);

	}

	public static String getWeightFromMachine(int portno, int baudRate, int dataBit, int stopBit, int parityBit) {

		WeighingMachineDataEvent weighingMachineDataEvent = new WeighingMachineDataEvent();

		boolean found = false;
		String weightFromMachine = "";
		
		SerialPort[] comPorts = SerialPort.getCommPorts();

		SerialPort comPort = SerialPort.getCommPorts()[portno];

		comPort.openPort();
		comPort.setBaudRate(baudRate);
		//comPort.setComPortParameters(baudRate, 8, 1, 0);
		comPort.setComPortParameters(baudRate, dataBit, stopBit, parityBit);

		comPort.setComPortTimeouts(SerialPort.TIMEOUT_NONBLOCKING, 0, 0);
		try {
			//while (!found) {
				
				while (comPort.bytesAvailable() == 0)
					Thread.sleep(baudRate);

				try {
				byte[] readBuffer = new byte[comPort.bytesAvailable()];
				int numRead = comPort.readBytes(readBuffer, readBuffer.length);

				String text = new String(readBuffer, "UTF-8");
				char[] chars = text.toCharArray();
				String[] wt = text.split("\r\n");

				for (int i = 0; i < wt.length; i++) {
					if(null!=wt[i].trim()) {
					System.out.println("WEIGHT FROM MACHINE = " + wt[i].trim());
					
					weighingMachineDataEvent.setWeightData(wt[i].trim());
					eventBus.post(weighingMachineDataEvent);
					
					weightFromMachine = wt[i].trim();
				}

				}
				}catch(Exception e) {
					
				}
				/*
				 * if (wt.length > 5) { for (int i = 0; i < wt.length; i++) {
				 * 
				 * //System.out.println("lenght = " + wt[i].length());
				 * //System.out.println(wt[i]); if (wt[i].length() == 11) { weightFromMachine =
				 * wt[i]; found = true; break;
				 * 
				 * }
				 * 
				 * } }
				 */

			//}
		} catch (Exception e) {
			e.printStackTrace();
		}
		comPort.closePort();

		return weightFromMachine;

	}

}
